<?php

class AccountController extends Controller
{
    // public $layout = "account";
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionDirectSales()
    {
        $this->render('direct-sales');
    }

    public function actionFastrack()
    {
        $this->render('fastrack');
    }

    public function actionRipple()
    {
        $this->render('ripple');
    }

    public function actionRippleMatching()
    {
        $this->render('ripple-matching');
    }

    public function actionRippleRoyalty()
    {
        $this->render('ripple-royalty');
    }

    public function actionBinary()
    {
        $this->render('binary');
    }

    public function actionBonusPayoutTable(){
        $this->renderPartial('bonus-payout-table');
    }
}