<?php
/**
 * Created by PhpStorm.
 * User: Sagar
 * Date: 3/17/2017
 * Time: 6:03 PM
 */

class EmailController extends Controller
{
    // public $layout = "account";
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * This is the Compose mail
     */
    public function actionCompose()
    {
        $this->render('compose');
    }

    /**
     * List of Sent Mail
     */
    public function actionSent()
    {
        $this->render('sent');
    }

    /**
     * List of trash mail
     */
    public function actionTrash()
    {
        $this->render('trash');
    }

    /**
     * View mail in detail
     */
    public function actionView(){
        $this->render('view');
    }
}