<?php

class HomeController extends CController
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * @return array action filters
     */

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        return UserIdentity::accessRules();
    }

    /**
     * Allow only the owner to do the action
     * @return boolean whether or not the user is the owner
     */

    public function allowOnlyOwner()
    {
        if (Yii::app()->user->isAdmin) {
            return true;
        } else {
            $example = Example::model()->findByPk($_GET["id"]);
            return $example->uid === Yii::app()->user->id;
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()

    {

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $param = "";
        //$this->render('index');
        $navigation = NavCheck::navigationCheck();

        $this->render('index', array('navigation' => $navigation));

    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {

        $this->layout = 'login';
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $auth = Yii::app()->db->schema->getTable('address_mapping');
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $data = SysUsers::model()->findByAttributes(array('username' => $model->username));
                $_SESSION['user'] = $data->username;
                $this->redirect(['/admin/']);
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionEdit()
    {
        $this->pageTitle = 'Edit Profile';

        $data = SysUsers::model()->findByAttributes(array('username' => $_SESSION['user']));
        $this->render('edit', array('data' => $data));
    }

    public function actionUpdateEmail()
    {
        if (isset($_POST['email'])) {
            //echo "UPDATE sys_users SET email='".$_POST['email']."' WHERE username ='".$_SESSION['user']."';"; die;
            //Yii::$app->db->createCommand("UPDATE sys_users SET email='".$_POST['email']."' WHERE username ='".$_SESSION['user']."';")->execute();
            //if (Yii::app()->db->createCommand("UPDATE sys_users SET email='".$_POST['email']."' WHERE username ='".$_SESSION['user']."';")->execute()) {
            $user = SysUsers::model()->findByAttributes(['username' => $_SESSION['user']]);
            $user->email = $_POST['email'];
            $user->activekey = 1;
            $user->auth_level = 1;
            if ($user->save()) {
                echo json_encode([
                    'msg' => 'Success',
                ]);
            } else{
                print_r($user->Errors);
                echo json_encode([
                    'msg' => 'Fail',
                ]);
            }
        }
    }

    public function actionChangePass(){
        if (isset($_POST['current_pass']) && isset($_POST['New_Pass'])) {
            //print_r($_POST); die;
            $user = SysUsers::model()->findByAttributes(['username' => $_SESSION['user']]);
            if ($_POST['current_pass'] === $user->password){
                $user->password = $_POST['New_Pass'];
                if ($user->save()) {
                    echo json_encode([
                        'msg' => 'Success',
                    ]);
                } else{
                    echo json_encode([
                        'msg' => 'Fail',
                    ]);
                }
            }else{
                echo json_encode([
                    'token' => 1
                ]);
            }

        }
    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        unset($_SESSION['user']);
        $this->redirect(Yii::app()->homeUrl . "admin/home/login/");
    }

}