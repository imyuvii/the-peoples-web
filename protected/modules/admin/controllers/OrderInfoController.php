<?php

class OrderInfoController extends CController
{
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return UserIdentity::accessRules();
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'itemModel' => OrderLineItem::model()->findAllByAttributes(['order_info_id' => $id]),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new OrderInfo;
		$orderItem =new OrderLineItem();
		$orderPayment =new OrderPayment();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrderInfo']) && isset($_POST['OrderLineItem'])) {

			$invoiceNo = 1;
			$lastInvoiceNo = OrderInfo::model()->find(array('order'=>'order_info_id DESC'));
			$model->attributes = $_POST['OrderInfo'];
			if($lastInvoiceNo == ''){
				$model->invoice_number = $invoiceNo;
				$model->order_id = $invoiceNo;
			}else{
				$model->invoice_number = $lastInvoiceNo['invoice_number']+1;
				$model->order_id = $lastInvoiceNo['invoice_number']+1;
			}

			$model->created_date = date('Y-m-d H:i:s');
			$model->modified_date = date('Y-m-d H:i:s');
			$model->invoice_date = date('Y-m-d H:i:s');

			// Order All item price,discount,total,net total
			$totalArray = $this->getOrderAllTotal($_POST['OrderLineItem']);
			$model->orderTotal = $totalArray['orderTotal'];
			$model->discount = $totalArray['orderDiscount'];
			$model->netTotal = $totalArray['netTotal'];

			$orderPayment->attributes = $_POST['OrderPayment'];
			$orderPayment->created_at = date('Y-m-d H:i:s');
			$orderPayment->modified_at = date('Y-m-d H:i:s');
			$orderPayment->total = $model->netTotal;
			if($model->validate() && $orderPayment->validate()) {
				if ($model->save()){

					$orderPayment->order_info_id =  $model->order_info_id;
					$orderPayment->save();
					$this->saveOrderItem($_POST['OrderLineItem'],$model->order_info_id);

					// add user affiliate level amount
					foreach ($_POST['OrderLineItem']['product_id'] as $index => $orderProductId){
						$affiliateData = ProductAffiliate::model()->findAllByAttributes(['product_id' => $orderProductId]);
						$product_qty = $_POST['OrderLineItem']['item_qty'][$index];

						if(!empty($affiliateData)) {
							$affLevel = [];
							foreach ($affiliateData as $affKey => $affiliate){
								$affLevel[$affKey] = $affiliate->aff_level;
							}
							$maxValue = max($affLevel);
							$userParents = CJSON::decode(BinaryTreeHelper::GetParentTrace($model->user_id,$maxValue));
							foreach ($userParents as $parent){
								if(in_array($parent['level'],$affLevel)){

									// get transaction type
									$field = 'Credit';
									$fieldId = CylFields::model()->findByAttributes(['field_name' => 'transaction_type']);
									$fieldLabel = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id,'field_label' => $field]);

									//get Wallet type
									$walletTypeId = WalletTypeEntity::model()->findByAttributes(['wallet_type' => 'User']);

									// get reference id
									$reference = WalletMetaEntity::model()->findByAttributes(['reference_key' => 'AFFILIATE']);

									// wallet transaction comment
									$comment = "From UserId-".$model->user_id.", Level-".$parent['level'];

									// get denomination id
									$tableId = CylTables::model()->findByAttributes(['table_name' => 'denomination']);
									$denomFieldId = CylFields::model()->findByAttributes(['field_name' => 'denomination_type', 'table_id' => $tableId->table_id]);
									$paymentTableId = CylTables::model()->findByAttributes(['table_name' => 'order_payment']);//get Payment Mode
									$paymentFieldId = CylFields::model()->findByAttributes(['field_name' => 'payment_mode', 'table_id' => $paymentTableId->table_id]);
									$paymentMode = CylFieldValues::model()->findByAttributes(['field_id' => $paymentFieldId->field_id, 'predefined_value' => $orderPayment->payment_mode]);
									$denominationData = CylFieldValues::model()->findByAttributes(['field_id' => $denomFieldId->field_id, 'field_label' => $paymentMode->field_label]);
									$denominationId = Denomination::model()->findByAttributes(['denomination_type' => $denominationData->predefined_value, 'currency' => 'EUR']);

									// get transaction status
									$walletTable = CylTables::model()->findByAttributes(['table_name' => 'wallet']);
									$transactionField = CylFields::model()->findByAttributes(['field_name' => 'transaction_status', 'table_id' => $walletTable->table_id]);
									$transactionValue = CylFieldValues::model()->findByAttributes(['field_id' => $transactionField->field_id, 'field_label' => 'Approved']);

									//get Portal id
									$portal = Portals::model()->findByAttributes(['portal_name' => 'IrisCall']);

									// get affiliate
									$affiliateDetails = ProductAffiliate::model()->findByAttributes(['product_id' => $orderProductId, 'aff_level' => $parent['level']]);
									$affAmount = $affiliateDetails->amount * $product_qty;

									$wallet = new Wallet();
									$wallet->user_id = $parent['userId'];
									$wallet->wallet_type_id = $walletTypeId->wallet_type_id;
									$wallet->transaction_type = $fieldLabel->predefined_value;
									$wallet->reference_id = $reference->reference_id;
									$wallet->reference_num = $model->user_id;
									$wallet->transaction_comment = $comment;
									$wallet->denomination_id = $denominationId->denomination_id;
									$wallet->transaction_status = $transactionValue->predefined_value;
									$wallet->portal_id = $portal->portal_id;
									$wallet->amount = $affAmount;
									$wallet->created_at = date('Y-m-d H:i:s');
									$wallet->modified_at = date('Y-m-d H:i:s');

									if($wallet->validate()){
										$wallet->save();
									}
								}
							}
						}
					}

					//$this->saveUserLicenseCount($_POST['OrderLineItem'],$model->user_id);

					// add user product licenses in user-license-count table
					foreach ($_POST['OrderLineItem']['product_id'] as $key => $productId) {
						$product_ids = ProductLicenses::model()->findAllByAttributes(['purchase_product_id' => $productId]);
						$product_qty = $_POST['OrderLineItem']['item_qty'][$key];

						if(!empty($product_ids)) {
							foreach ($product_ids as $key1 => $licenseProductId) {
								// license multiply by quantity
								$totalLicense = $licenseProductId->license_no * $product_qty;

								//$userLicenseCount = UserLicenseCount::model()->findByAttributes(['product_id' => $licenseProductId->product_id, 'user_id' => $_POST['OrderLineItem']]);
								$userLicenseCount = new UserLicenseCount();
								$userLicenseCount->product_id = $licenseProductId->product_id;
								$userLicenseCount->total_licenses = $totalLicense;
								$userLicenseCount->available_licenses = $totalLicense;
								$userLicenseCount->user_id = $model->user_id;
								$userLicenseCount->modified_at = date('Y-m-d H:i:s');
								$userLicenseCount->created_at = date('Y-m-d H:i:s');
								if($userLicenseCount->validate()){
									$userLicenseCount->save();
								}
							}
						}
					}
					$this->redirect(array('view', 'id' => $model->order_info_id));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'orderItem' => $orderItem,
			'orderPayment' => $orderPayment,
		));

	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$orderItem = OrderLineItem::model()->findAllByAttributes(['order_info_id' => $model->order_info_id]);
		$orderPayment = OrderPayment::model()->find('order_info_id ='. $model->order_info_id);
		$userInfo = OrderInfo::model()->findByAttributes(['order_info_id' => $id]);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrderInfo']) && isset($_POST['OrderLineItem']) && isset($_POST['OrderPayment']))
		{
			$model->attributes = $_POST['OrderInfo'];
			$model->modified_date = date('Y-m-d H:i:s');
			if($model->validate()) {
				$model->attributes = $_POST['OrderInfo'];

				// Delete Order Item
				$oldItems = OrderLineItem::model()->findAllByAttributes(['order_info_id' => $model->order_info_id]);
				if(!empty($oldItems)){
					foreach ($oldItems as $key => $value) {
						if (!in_array($value['product_id'], $_POST['OrderLineItem']['product_id'])) {
							OrderLineItem::model()->deleteAllByAttributes(['order_info_id' => $model->order_info_id, 'product_id' => $value['product_id']]);
						}
					}
				}

				// Update or create Order Line Item
				$orderItemArray = $_POST['OrderLineItem'];
				$this->saveOrderItem($_POST['OrderLineItem'],$model->order_info_id);

				// Update Order All item price,discount,total,net total
				$totalArray = $this->getOrderAllTotal($_POST['OrderLineItem']);
				$model->orderTotal = $totalArray['orderTotal'];
				$model->discount = $totalArray['orderDiscount'];
				$model->netTotal = $totalArray['netTotal'];

				// Update Order Payment
				$orderPayment->attributes = $_POST['OrderPayment'];
				$orderPayment->modified_at = date('Y-m-d H:i:s');
				$orderPayment->total = $model->netTotal;
				if($orderPayment->validate()){
					$orderPayment->save();
				}

				if ($model->save()) {

					$this->redirect(array('view', 'id' => $model->order_info_id));
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'orderItem' => $orderItem,
			'orderPayment' => $orderPayment,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		$orders = OrderLineItem::model()->findByAttributes(['order_info_id' => $id]);
		if(!empty($orders)){
			OrderLineItem::model()->deleteAll("order_info_id ='" . $id . "'");
		}
		$orderPayment = OrderPayment::model()->findByAttributes(['order_info_id' => $id]);
		if(!empty($orderPayment)){
			OrderPayment::model()->deleteAll("order_info_id ='" . $id . "'");
		}
		$creditMemo = OrderCreditMemo::model()->findByAttributes(['order_info_id' => $id]);
		if(!empty($creditMemo)){
			OrderCreditMemo::model()->deleteAll("order_info_id ='" . $id . "'");
		}
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$this->redirect(['admin']);
		//		$dataProvider=new CActiveDataProvider('OrderInfo');
		//		$this->render('index',array(
		//			'dataProvider'=>$dataProvider,
		//		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new OrderInfo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OrderInfo']))
			$model->attributes=$_GET['OrderInfo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/*
	* Credit Memo
	*/
	public function actionCreditMemo($id){
        $model=$this->loadModel($id);
		$orderItem = OrderLineItem::model()->findAll('order_info_id =' . $model->order_info_id);
		$creditMemo = new OrderCreditMemo();


		//        foreach ($orderItem as $item){
		//            $data[] = $item->item_sku;
		//        }
		//        print_r($data);
		//        die;
		$this->render('creditMemo',[
			'model' => $model,
			'orderItem' => $orderItem,
			'creditMemo' => $creditMemo,
		]);
	}

	/**
	* Credit Memo Create using post
	*/
	public function actionCreditMemoCreate(){
		$creditMemo = new OrderCreditMemo();

		if(isset($_POST)){
			try {
				//get Product Name
				$productPrice = ProductInfo::model()->findByAttributes(['product_id' => $_POST['productId']]);
				$refundAmount = $productPrice->price * $_POST['qty'];

				// Add details in credit memo
				$creditMemo->product_id = $_POST['productId'];
				$creditMemo->order_info_id = $_POST['orderId'];
				$creditMemo->qty_refunded = $_POST['qty'];
				$creditMemo->invoice_number = $_POST['invoiceNo'];
				$creditMemo->memo_status = $_POST['status'];
				$creditMemo->amount_to_refund = $refundAmount;
				$creditMemo->created_at = date('Y-m-d H:i:s');
				$creditMemo->modified_at = date('Y-m-d H:i:s');
				// validate credit memo
				if ($creditMemo->validate()) {
					$creditMemo->save();
				}

				$orderItem = OrderLineItem::model()->findByAttributes(['order_info_id' => $_POST['orderId'],'product_id' => $_POST['productId']]);
				$oldQty = $orderItem->item_qty;
				// set new quantity
				$newQty = $oldQty - $_POST['qty'];

				$memoPrice = $_POST['qty'] * $productPrice->price;
				$newPrice = $orderItem->item_price - $memoPrice;
				$orderItem->item_price = $newPrice;

				$orderItem->item_qty = $newQty;
				$orderItem->modified_at = date('Y-m-d H:i:s');
				$orderItem->save();
				$orderItem = OrderLineItem::model()->findByAttributes(['order_info_id' => $_POST['orderId'],'product_id' => $_POST['productId']]);
				$orderQty = $orderItem->item_qty;
				if($orderQty <= 0 ){
					OrderLineItem::model()->deleteAllByAttributes(['order_info_id' => $_POST['orderId'],'product_id' => $_POST['productId']]);
				}

				$orderInfo = OrderInfo::model()->findByAttributes(['order_info_id' => $_POST['orderId']]);
				// set order total
				$oldTotal = $orderInfo->orderTotal;
				$newTotal = $oldTotal - $memoPrice;
				$orderInfo->orderTotal = $newTotal;

				// set order NetTotal
				$oldNetTotal = $orderInfo->netTotal;
				$newNetTotal = $oldNetTotal - $memoPrice;
				$orderInfo->netTotal = $newNetTotal;

				$orderInfo->modified_date = date('Y-m-d H:i:s');
				$orderInfo->save();
				$result = [
					'result' => true,
				];

			}catch (Exception $e){
				$result = [
					'result' => false,
					'error' => $newNetTotal,
				];
			}
		}
		echo json_encode($result);
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return OrderInfo the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
	$model=OrderInfo::model()->findByPk($id);
	if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param OrderInfo $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-info-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/*
	* Load address using user_id of user
	*/
	public function actionLoadAddresses()
	{
		$data=AddressMapping::model()->findAll('user_id=:user_id',
		array(':user_id'=>(int) $_POST['user_id']));


		$data=CHtml::listData($data,'address_mapping_id','address_id');
		echo "<option value=''>Select Address</option>";
		foreach($data as $value=>$addressMappId) {
			$addressMappData = Addresses::model()->find('address_id=:address_id',
			array(':address_id' => (int)$addressMappId));
			echo CHtml::tag('option', array('value' => $value), CHtml::encode($addressMappData->building_no . ', ' . $addressMappData->street . ', ' . $addressMappData->city . ', ' . $addressMappData->region), true);
		}
	}

	/*
	*
	*/
	public function actionLoadPrice(){

		if($_POST['qty'] > 0) {
			$productDetail = ProductInfo::model()->findByAttributes(['product_id' => $_POST['productId']]);
			$totalItemPrice = $productDetail->price * $_POST['qty'];
			$result = [
				'result' => true,
				'productPrice' => $totalItemPrice,
			];
		}else{
			$result = [
				'result' => false,
			];
		}
		echo json_encode($result);
	}

	/*
	* get Address and business address
	*/
	public function actionGetAddress(){
		if(isset($_POST['user_id'])){
		$users = UserInfo::model()->findByPk(['user_id' => $_POST['user_id']]);
		$result = [
			'result' => true,
			'userInfo' => $users->attributes,
		];
		}else{
			$result = [
				'result' => false,
			];
		}
		echo json_encode($result);
	}

	/**
	* save and update order item
	* @param $orderItemArray
	* @param $orderInfoId
	*/
	protected function saveOrderItem($orderItemArray,$orderInfoId){
		foreach ($orderItemArray['item_price'] as $key => $item){
			$orderItem = OrderLineItem::model()->findByAttributes(['order_info_id' => $orderInfoId, 'product_id' => $orderItemArray['product_id'][$key]]);
			if(!empty($orderItem)){
				$orderItem->item_qty = $orderItemArray['item_qty'][$key];
				$orderItem->item_disc = $orderItemArray['item_disc'][$key];
				$orderItem->item_price = $orderItemArray['item_price'][$key];
				$orderItem->modified_at = date('Y-m-d H:i:s');
				$orderItem->save();
			}else{
			// Item not exist then enter new data
				$orderItem =new OrderLineItem();
				$orderItem->order_info_id = $orderInfoId;
				$orderItem->product_id = $orderItemArray['product_id'][$key];
				$orderItem->item_qty = $orderItemArray['item_qty'][$key];
				$orderItem->item_disc = $orderItemArray['item_disc'][$key];
				$orderItem->item_price = $orderItemArray['item_price'][$key];
				$orderItem->created_at = date('Y-m-d H:i:s');
				$orderItem->modified_at = date('Y-m-d H:i:s');
				$orderItem->save();
			}
		}
	}

	/**
	* make total,net total and discount
	* @param $orderItemArray
	* @return array
	*/
	protected function getOrderAllTotal($orderItemArray){

	$itemPriceTotal = 0;
	$itemDiscTotal = 0;
	foreach ($orderItemArray['item_price'] as $key => $item){
		$itemPriceTotal += $orderItemArray['item_price'][$key];
		$itemDiscTotal += $orderItemArray['item_disc'][$key];
	}
	$result = [
		'orderTotal' => $itemPriceTotal,
		'orderDiscount' => $itemDiscTotal,
		'netTotal' => $itemPriceTotal - $itemDiscTotal
	];
	return $result;
	}

	/**
	* @param $userLicenseData
	* @param $purchaseProductId
	*/
	protected function saveUserLicenseCount($userLicenseData,$purchaseProductId){
		foreach ($userLicenseData['product_id'] as $key => $productId){
			$product_ids = ProductLicenses::model()->findAllByAttributes(['purchase_product_id' => $productId]);
			foreach ($product_ids as $key2 => $licenseProductId ){
				$userLicenses = UserLicenses::model()->findByAttributes(['product_id' => $licenseProductId->product_id,'user_id' => $purchaseProductId]);

				if(!empty($userLicenses)){
					$userLicenses->product_id = $licenseProductId->product_id;
					$userLicenses->license_no = $licenseProductId->license_no;
					$userLicenses->user_id = $purchaseProductId;
					$userLicenses->is_used = 1;
					$userLicenses->funded_on = date('Y-m-d H:i:s');
					$userLicenses->created_at = date('Y-m-d H:i:s');
					if($userLicenses->validate()){
						$userLicenses->save();
					}
				}else{
					$userLicenses = new UserLicenses();
					$userLicenses->product_id = $licenseProductId->product_id;
					$userLicenses->license_no = $licenseProductId->license_no;
					$userLicenses->user_id = $purchaseProductId;
					$userLicenses->is_used = 1;
					$userLicenses->funded_on = date('Y-m-d H:i:s');
					$userLicenses->created_at = date('Y-m-d H:i:s');
					if($userLicenses->validate()){
						$userLicenses->save();
					}
				}
			}
		}
	}
}
