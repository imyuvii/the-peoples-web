<?php

class UserInfoController extends CController
{
	/**
	* @return array action filters
	*/
		public function filters()
		{
			return array(
			'accessControl', // perform access control for CRUD operations
			);
		}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/

	public function accessRules()
	{
		return UserIdentity::accessRules();
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model = new UserInfo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['UserInfo'])) {
			if (isset($_POST['UserInfo']['checkbox'])) {
				if ($_POST['UserInfo']['checkbox']) {
					foreach ($_POST['UserInfo']['checkbox'] as $key => $val) {
					if ($val){
						$model->$key = implode(",", $val);
					}
				}
			}
		}

			$model->attributes = $_POST['UserInfo'];
			$model->created_at = date('Y-m-d H:i:s');
			$model->modified_at = date('Y-m-d H:i:s');
			$model->password = md5($_POST['UserInfo']['password']);
			if ($model->validate()) {
				if ($model->save()) {
					$this->redirect(array('view', 'id' => $model->user_id));
				}
			}
		}
		$this->render('create', array(
		'model' => $model,
		));
	}

	protected function validateUser($model, $address, $addressMap)
	{
		$user = UserInfo::model()->findAllByAttributes(['email' => $model->email]);
			if (count($user) > 0):
				$model->addError('email', 'Email Already Exist in System');
				$flag = 0;
				return $flag;
			endif;

			if ($model->validate() && $address->validate()) {
				$model->save(false);
				$address->save(false);
				$addressMap->address_id = $address->address_id;
				$addressMap->user_id = $model->user_id;
				$addressMap->created_at = date('Y-m-d H:i:s');
				$addressMap->modified_at = date('Y-m-d H:i:s');
				$model->created_at = date('Y-m-d H:i:s');
				$addressMap->save();
				$flag = 1;
			} else {
				$flag = 0;
			}
		return $flag;
	}

	protected function validateBusiness($model, $address, $addressMap, $business)
	{
		$user = UserInfo::model()->findAllByAttributes(['email' => $model->email]);
		if (count($user) > 0):
			$model->addError('email', 'Email Already Exist in System');
			$flag = 0;
			return $flag;
		endif;

		$business->user_id = rand(9, 9999);
			if ($model->validate() && $address->validate() && $business->validate()) {
				if ($model->save()) {
					$business->user_id = $model->user_id;
					$business->save();
					$address->save(false);
					$addressMap->user_id = $business->business_id;
					$addressMap->address_id = $address->address_id;
					$addressMap->user_id = $model->user_id;
					$addressMap->created_at = date('Y-m-d H:i:s');
					$addressMap->modified_at = date('Y-m-d H:i:s');
					$model->created_at = date('Y-m-d H:i:s');
					$addressMap->save();
					$flag = 1;
				}
			} else {
				$flag = 0;
			}
		return $flag;
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public
	function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['UserInfo'])) {
			if (isset($_POST['UserInfo']['checkbox'])) {
				if ($_POST['UserInfo']['checkbox']) {
					foreach ($_POST['UserInfo']['checkbox'] as $key => $val) {
						if ($val) {
							$model->$key = implode(",", $val);
						} else {
							$model->$key = "";
						}
					}
				}
			}
			$model->attributes = $_POST['UserInfo'];
			$model->modified_at = date('Y-m-d H:i:s');
			if ($model->save())
			$this->redirect(array('view', 'id' => $model->user_id));
		}
		foreach ($model->getMetadata()->columns as $temp) {
			$arr = json_decode($temp->comment);
			$fieldType[$arr->name] = $arr->field_input_type;
		}
		foreach ($fieldType as $key => $val) {
			if ($val == 'check') {
				$model->$key = explode(',', $model->$key);
			}
		}
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		$orderinfo = OrderInfo::model()->findByAttributes(['user_id' => $id]);
		$orderLineItem = OrderLineItem::model()->findByAttributes(['order_info_id' => $orderinfo->order_info_id]);
		$orderPayment = OrderPayment::model()->findByAttributes(['order_info_id' => $orderinfo->order_info_id]);
		$creditMemo = OrderCreditMemo::model()->findByAttributes(['order_info_id' => $orderinfo->order_info_id]);
		$wallet = Wallet::model()->findByAttributes(['user_id' => $id]);
		$userlicencecount = UserLicenseCount::model()->findByAttributes(['user_id' => $id]);

		if(!empty($orderLineItem)){
			OrderLineItem::model()->deleteAll("order_info_id ='" . $orderinfo->order_info_id . "'");
		}

		if(!empty($orderPayment)){
			OrderPayment::model()->deleteAll("order_info_id ='" . $orderinfo->order_info_id . "'");
		}

		if(!empty($orderinfo)){
			OrderInfo::model()->deleteAll("user_id ='" . $id . "'");
		}


		if(!empty($creditMemo)){
			OrderCreditMemo::model()->deleteAll("order_info_id ='" . $orderinfo->order_info_id . "'");
		}

		if(!empty($wallet)){
			Wallet::model()->deleteAll("user_id ='" . $id . "'");
		}

		if(!empty($userlicencecount)){
			UserLicenseCount::model()->deleteAll("user_id ='" . $id . "'");
		}
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('UserInfo');
			$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model = new UserInfo('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['UserInfo']))
		$model->attributes = $_GET['UserInfo'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return UserInfo the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model = UserInfo::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
			return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param UserInfo $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-info-form') {
		echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	* this action change user password
	* @param $id
	* @return json array
	*/
	public function actionChangePassword($id){
		$model = UserInfo::model()->findByPk(['user_id' => $id]);

		if(isset($_POST['UserInfo'])){
			try {
				$model->password = md5($_POST['UserInfo']['newPassword']);
				$model->modified_at = date('Y-m-d H:i:s');
				if ($model->validate()) {
					if($model->save())
						$result = [
							'result' => true
						];
				}
			}catch (Exception $e){
				$result = [
					'result' => false,
					'error' => $e
				];
			}
			echo CJSON::encode($result);
		}else {
			$this->render('changePassword', array(
				'model' => $model,
			));
		}
	}

	/**
	* this action active or inactive
	* @param $id
	*/
	public function actionUserActive($id){
		$model = UserInfo::model()->findByPk(['user_id' => $id]);

		if(isset($_GET['is_active'])){
			if($_GET['is_active'] == 0){
				$model->is_active = 1;
			}else{
				$model->is_active = 0;
			}
			$model->modified_at = date('Y-m-d H:i:s');
			if($model->save()) {
				$this->redirect(['view','id' => $model->user_id ]);
			}
		}
	}
}
