<?php /* @var $this Controller */
Yii::import('application.components.NavCheck');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <?php
    Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/../plugins/js/plugins/datatables/jquery.dataTables.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/../plugins/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/../plugins/css/bootstrap.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/../plugins/css/oneui.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/../plugins/js/plugins/select2/select2.min.css');
    // Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/../plugins/js/plugins/select2/select2-bootstrap.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/style.css');
    ?>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/../plugins/img/favicons/favicon.png">
    <title><?php echo ucfirst(Yii::app()->params['applicationName']); ?></title>
</head>
<body>
<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
    <!-- Side Overlay-->
    <aside id="side-overlay">
        <!-- Side Overlay Scroll Container -->
        <div id="side-overlay-scroll">
            <!-- Side Header -->
            <div class="side-header side-content">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-default pull-right" type="button" data-toggle="layout"
                        data-action="side_overlay_close">
                    <i class="fa fa-times"></i>
                </button>
                <span class="font-w600 push-10-l">Admin</span>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content remove-padding-t">
                <!-- Block -->
                <div class="block pull-r-l">
                    <div class="block-header bg-gray-lighter">
                        <ul class="block-options">
                            <li>
                                <button type="button" data-toggle="block-option" data-action="refresh_toggle"
                                        data-action-mode="demo"><i class="si si-refresh"></i></button>
                            </li>
                            <li>
                                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Block</h3>
                    </div>
                    <div class="block-content">
                        <p>...</p>
                    </div>
                </div>
                <!-- END Block -->
            </div>
            <!-- END Side Content -->
        </div>
        <!-- END Side Overlay Scroll Container -->
    </aside>
    <!-- END Side Overlay -->

    <!-- Sidebar -->
    <nav id="sidebar">
        <!-- Sidebar Scroll Container -->
        <div id="sidebar-scroll">
            <!-- Sidebar Content -->
            <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
            <div class="sidebar-content">
                <!-- Side Header -->
                <div class="side-header side-content bg-white-op">
                    <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                    <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                            data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times"></i>
                    </button>
                    <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                    <div class="btn-group pull-right">
                        <ul class="dropdown-menu dropdown-menu-right font-s13 sidebar-mini-hide">
                            <li>
                                <a data-toggle="theme" data-theme="default" tabindex="-1" href="javascript:void(0)">
                                    <i class="fa fa-circle text-default pull-right"></i> <span
                                        class="font-w600">Default</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="theme" data-theme="assets/css/themes/amethyst.min.css" tabindex="-1"
                                   href="javascript:void(0)">
                                    <i class="fa fa-circle text-amethyst pull-right"></i> <span class="font-w600">Amethyst</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="theme" data-theme="assets/css/themes/city.min.css" tabindex="-1"
                                   href="javascript:void(0)">
                                    <i class="fa fa-circle text-city pull-right"></i> <span
                                        class="font-w600">City</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="theme" data-theme="assets/css/themes/flat.min.css" tabindex="-1"
                                   href="javascript:void(0)">
                                    <i class="fa fa-circle text-flat pull-right"></i> <span
                                        class="font-w600">Flat</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="theme" data-theme="assets/css/themes/modern.min.css" tabindex="-1"
                                   href="javascript:void(0)">
                                    <i class="fa fa-circle text-modern pull-right"></i> <span
                                        class="font-w600">Modern</span>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="theme" data-theme="assets/css/themes/smooth.min.css" tabindex="-1"
                                   href="javascript:void(0)">
                                    <i class="fa fa-circle text-smooth pull-right"></i> <span
                                        class="font-w600">Smooth</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php $application = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", Yii::app()->params['applicationName']))?>
                    <a class="h5 text-white " href="<?php echo Yii::app()->createUrl('admin/home/index'); ?>">
                        <img src="<?php echo Yii::app()->baseUrl . '/../common/' . $application . '/Logo.png' ?>" style="max-width: 80px; max-height: 45px;">
                        <!--                        <i class="fa fa-circle-o-notch text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide">ne</span>-->
                    </a>
                </div>
                <!-- END Side Header -->
                <?php
                $nav = new NavCheck;
                $navigation = $nav->navigationCheck();
                ?>
                <!-- Side Content -->
                <div class="side-content" id="mainmenu">
                    <?php $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label' => '<i class="si si-home"></i> <span class="sidebar-mini-hide"> Home </span>', 'linkOptions' => ['class' => 'nav-menu'], 'url' => array('/admin/home/index')),

                            array('label' => '<i class="si si-user"></i> <span class=""> Users </span>', 'linkOptions' => ['class' => 'nav-submenu '.$navigation['user'] , 'data-toggle' => "nav-submenu"], 'url' => 'javascript::void(0);', 'items' => [
                                array('label' => '<i class="si si-user"></i> <span class="sidebar-mini-hide"> Users </span>', 'url' => array('/admin/userInfo/admin')),
                                array('label' => '<i class="si si-user"></i> <span class="sidebar-mini-hide">System Users</span>', 'url' => array('/admin/sysUsers/admin'))

                            ]),

                            array('label' => '<i class="fa fa-product-hunt"></i> <span class=""> Products </span>', 'linkOptions' => ['class' => 'nav-submenu '.$navigation['product'] , 'data-toggle' => "nav-submenu"], 'url' => 'javascript::void(0);', 'items' => [
                                array('label' => '<i class="fa fa-product-hunt"></i> <span class="sidebar-mini-hide"> Products </span>', 'url' => array('/admin/productInfo/admin')),
                                array('label' => '<i class="fa  fa-tags"></i> <span class="sidebar-mini-hide"> Products Categories</span>', 'url' => array('/admin/categories/admin'))

                            ]),
                            array('label' => '<i class="fa fa-file-text-o"></i> <span class=""> Orders </span>', 'linkOptions' => ['class' => 'nav-submenu '.$navigation['order'] , 'data-toggle' => "nav-submenu"], 'url' => 'javascript::void(0);', 'items' => [
                                array('label' => '<i class="fa fa-file-text-o"></i> <span class="sidebar-mini-hide"> Orders </span>', 'url' => array('/admin/orderInfo/admin')),
                                array('label' => '<i class="fa fa-book"></i> <span class="sidebar-mini-hide"> Orders Credit Memo </span>', 'url' => array('/admin/orderCreditMemo/admin'))

                            ]),
                            array('label' => '<i class="si si-wallet"></i> <span class=""> Wallet </span>', 'linkOptions' => ['class' => 'nav-submenu '.$navigation['wallet'] , 'data-toggle' => "nav-submenu"], 'url' => 'javascript::void(0);', 'items' => [
                                array('label' => '<i class="si si-wallet"></i> <span class="sidebar-mini-hide"> Wallet </span>', 'linkOptions' => ['class' => 'nav-menu '], 'url' => array('/admin/wallet/admin')),
                                array('label' => '<i class="si si-wallet"></i> <span class="sidebar-mini-hide"> Wallet Type </span>', 'linkOptions' => ['class' => 'nav-menu '], 'url' => array('/admin/walletTypeEntity/admin')),
                                array('label' => '<i class="si si-wallet"></i> <span class="sidebar-mini-hide"> Transaction Type </span>', 'linkOptions' => ['class' => 'nav-menu '], 'url' => array('/admin/walletMetaEntity/admin')),
                            ]),

                            array('label' => '<i class="fa fa-bar-chart"></i><span class="sidebar-mini-hide"> Report</span></i>', 'linkOptions' => ['class' => ' '.$navigation['wallet']], 'url' => array('/admin/Report/userReport')),
                            array('label' => '<i class="si si-doc"></i> <span class=""> Api Documentation </span>', 'linkOptions' => ['class' => 'nav-submenu', 'data-toggle' => "nav-submenu"], 'url' => 'javascript::void(0);', 'items' => [
                                array('label' => '<i class="si si-user '.$navigation['user'].'"></i><span class="sidebar-mini-hide '.$navigation['user'].'" > User Api </span></i> ', 'url' => array('/admin/Documentation/userApi')),
                                array('label' => '<i class="fa fa-gift '.$navigation['product'].'"></i> <span class="sidebar-mini-hide '.$navigation['product'].'"> Products Api</span></i>', 'url' => array('/admin/Documentation/productApi')),
                                array('label' => '<i class="fa fa-book '.$navigation['order'].'"></i> <span class="sidebar-mini-hide '.$navigation['order'].'"> Order Api</span></i>', 'url' => array('/admin/Documentation/orderApi')),
                                array('label' => '<i class="si si-wallet '.$navigation['wallet'].'"></i> <span class="sidebar-mini-hide '.$navigation['wallet'].'"> Wallet Api</span></i>', 'url' => array('/admin/Documentation/walletApi')),
                            ]),
                        ),
                        'encodeLabel' => false,
                        'htmlOptions' => array('class' => 'nav-main'),
                    )); ?>
                </div>
                <!-- mainmenu -->
            </div>
            <!-- Sidebar Content -->
        </div>
        <!-- END Sidebar Scroll Container -->
    </nav>
    <!-- END Sidebar -->

    <header id="header-navbar" class="content-mini content-mini-full">
        <!-- Header Navigation Right -->
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <h2 class="page-heading">
                        <?php if (isset($this->breadcrumbs)): ?>
                            <?php //echo ucfirst(Yii::app()->params['applicationName']); ?>
                        <?php endif; ?>
                    </h2>
                </div>
                <div class="col-sm-6">
                    <ul class="nav-header pull-right">
                        <li>
                            <div class="btn-group admin-btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                                    <i class="si si-user admin-icon user-admin-icon"></i>&nbsp;&nbsp;<?php echo isset($_SESSION['user'])?ucfirst($_SESSION['user']):''; ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a tabindex="-1" href="<?php echo Yii::app()->createUrl('admin/home/edit'); ?>">
                                            <i class="si si-settings pull-right"></i>Change Password
                                        </a>
                                    </li>
                                    <li>
                                        <a tabindex="-1"
                                           href="<?php echo Yii::app()->createUrl('admin/home/logout'); ?>">
                                            <i class="si si-logout pull-right"></i>Log out
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END Header Navigation Left -->
    </header>

    <main id="main-container" style="min-height: 342px; padding-top: 60px;">
        <div class="content bg-gray-lighter">
            <div class="row items-push">
                <div class="col-sm-7">
                    <h1 class="page-heading">
                        <?php echo $this->getPageTitle(); ?>
                    </h1>
                </div>
            </div>
        </div>
        <?php if ($this->uniqueid == "admin" && $this->action->Id == "index"){
            echo $content;
        }elseif($this->uniqueid == "admin" && $this->action->Id == "edit"){
            ?>
            <div class="content">
                <div class="block">

                    <?php echo $content; ?>

                </div>
            </div>
            <?php
        }else{
            ?>
            <div class="content">
                <div class="block">
                    <div class="block-content">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </main>
    <!-- Footer -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">

        <div class="pull-right"> <i class="fa fa-code" aria-hidden="true"></i> with <i class="fa fa-heart"></i> by
            <a class="font-w600" href="http://abptechnologies.com/" target="_blank">ABP Technologies</a>
        </div>
        <div class="pull-left">
            <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">Cyclone</a> &copy; <span
                class="js-year-copy"></span>
        </div>
    </footer>
    <!-- END Footer -->
</div>
</body>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/jquery.min.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/bootstrap.min.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/jquery.slimscroll.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/jquery.scrollLock.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/jquery.appear.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/jquery.countTo.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/jquery.placeholder.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/core/js.cookie.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/app.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/pages/base_tables_datatables.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/plugins/jquery-validation/jquery.validate.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/plugins/jquery-validation/additional-methods.min.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/../plugins/js/plugins/select2/select2.full.min.js', CClientScript::POS_END);
?>
</html>
