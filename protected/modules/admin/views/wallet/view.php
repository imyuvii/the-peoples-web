<?php
/* @var $this WalletController */
/* @var $model Wallet */

$this->pageTitle = 'View Wallet';
$id = $model->wallet_id; 
?>
<div class="pull-right m-b-10">
	<?php echo CHtml::link('Go to list', array('wallet/admin'), array('class' => 'btn btn-minw btn-square btn-warning')); ?> 
	<?php if($model->transaction_status != 2) { ?>
	<?php echo CHtml::link('Create', array('wallet/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?> 
	<?php echo CHtml::link('Update', array('wallet/update/'.$id), array('class' => 'btn btn-minw btn-square btn-warning')); ?>
	<?php } ?>
</div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table'),
	'attributes'=>array(
		'wallet_id',
		[
			'name' => 'user_id',
			'value' => function($model){
			$userName = UserInfo::model()->findByAttributes(['user_id' => $model->user_id]);
				return $userName->full_name;
			}
		],
		[
			'name' => 'wallet_type_id',
			'value' => function($model){
				$wallet_type = WalletTypeEntity::model()->findByAttributes(['wallet_type_id' => $model->wallet_type_id]);
				return $wallet_type->wallet_type;
			}
		],
		[
			'name' => 'transaction_type',
			'value' => function($model){
				$fieldId = CylFields::model()->findByAttributes(['field_name' => 'transaction_type']);
				$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->transaction_type]);
				return $orderStatus->field_label;
			}
		],
		[
			'name' => 'reference_id',
			'value' => function($model){
				$wallet_type = WalletMetaEntity::model()->findByAttributes(['reference_id' => $model->reference_id]);
				return $wallet_type->reference_desc;
			}
		],
		'reference_num',
		'transaction_comment',
		[
			'name' => 'denomination_id',
			'value' => function($model){
				$wallet_type = Denomination::model()->findByAttributes(['denomination_id' => $model->denomination_id]);
				$fieldId = CylFields::model()->findByAttributes(['field_name' => 'denomination_id']);
				$fieldLabel = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id,'predefined_value' => $wallet_type->denomination_type]);
				return $fieldLabel['field_label']. ' ' . $wallet_type->currency;
			}
		],
		[
			'name' => 'transaction_status',
			'value' => function($model){
				$fieldId = CylFields::model()->findByAttributes(['field_name' => 'transaction_status']);
				$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->transaction_status]);
				return $orderStatus->field_label;
			}
		],
		[
			'name' => 'portal_id',
			'value' => function($model){
				$wallet_type = Portals::model()->findByAttributes(['portal_id' => $model->portal_id]);
				return $wallet_type->portal_name;
			}
		],
		'amount',
		'updated_balance',
		'created_at',
		'modified_at',
	),
)); ?>
