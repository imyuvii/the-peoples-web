<?php
/* @var $this WalletController */
/* @var $model Wallet */

$this->pageTitle = 'Wallets';
 ?>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right m-b-10">
		<?php echo CHtml::link('Create', array('Wallet/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?>
		</div>

<?php
$walletData = Wallet::model()->findAll();
if($walletData != null){
	?>

		<div class="pull-left">
			<div class="btn-group dropdown ">
				<button class="form-group btn dropdown-toggle field-list" data-toggle="dropdown">
					<label>Select Fields</label>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu">
					<li><input type="checkbox" class="hidecol" value="SelectAll" id="select_all">Select All</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<br/>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'wallet-grid',
	'dataProvider'=>$model->search(),
	//'enableSorting' => true,
	// 'enablePagination' => true,
	'type' => TbHtml::GRID_TYPE_BORDERED,
	'filter'=> $model,
	'summaryText' => false,
	'itemsCssClass' => 'js-dataTable-full',
	'columns'=>array(
	[
	'class'=>'CButtonColumn',
	'template' => '{view}{update}',
	'header' => 'Action',
		'buttons' => [
			'view' => [
			'title' => 'view',
			'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
			'imageUrl' => false,
			'url' => 'Yii::app()->createUrl("admin/Wallet/view/$data->wallet_id")',
			'options' => array('class' => 'btn-view', 'title' => 'View'),
			],
			'update' => [
			'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
			'imageUrl' => false,
			'url' => 'Yii::app()->createUrl("admin/Wallet/update/$data->wallet_id")',
			'options' => array('class' => 'btn-update', 'title' => 'Edit'),
			'visible' => '$data->transaction_status!=2',
			],
		],
	],
	'wallet_id',
	[
		'name' => 'user_id',
		'value' => function($model){
			$userName = UserInfo::model()->findByAttributes(['user_id' => $model->user_id]);
			return $userName->full_name;
		}
	],
	[
		'name' => 'wallet_type_id',
		'value' => function($model){
			$wallet_type = WalletTypeEntity::model()->findByAttributes(['wallet_type_id' => $model->wallet_type_id]);
			return $wallet_type->wallet_type;
		}
	],
	[
		'name' => 'transaction_type',
		'value' => function($model){
			$fieldId = CylFields::model()->findByAttributes(['field_name' => 'transaction_type']);
			$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->transaction_type]);
			return $orderStatus->field_label;
		}
	],
	[
		'name' => 'reference_id',
		'value' => function($model){
			$wallet_type = WalletMetaEntity::model()->findByAttributes(['reference_id' => $model->reference_id]);
			return $wallet_type->reference_desc;
		}
	],
	'reference_num',
	'transaction_comment',
	[
		'name' => 'denomination_id',
		'value' => function($model){
			$wallet_type = Denomination::model()->findByAttributes(['denomination_id' => $model->denomination_id]);
			$fieldId = CylFields::model()->findByAttributes(['field_name' => 'denomination_id']);
			$fieldLabel = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id,'predefined_value' => $wallet_type->denomination_type]);
			return $fieldLabel['field_label']. ' ' . $wallet_type->currency;
		}
	],
	/*[
		'name' => 'transaction_status',
		'value' => function($model){
			$fieldId = CylFields::model()->findByAttributes(['field_name' => 'transaction_status']);
			$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->transaction_status]);
			return $orderStatus->field_label;
		}
	],
	'portal_id',
	[
		'name' => 'portal_id',
		'value' => function($model){
			$wallet_type = Portals::model()->findByAttributes(['portal_id' => $model->portal_id]);
			return $wallet_type->portal_name;
		}
	],
	'amount',
	'updated_balance',
	'created_at',
	'modified_at',
	*/
	),
));
} else{ ?>
<div class="raw m-b-10">
	<span class="empty">No results found.</span>
</div>
<?php } ?>