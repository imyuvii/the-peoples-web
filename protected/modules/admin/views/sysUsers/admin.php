<?php
/* @var $this SysUsersController */
/* @var $model SysUsers */

$this->pageTitle = 'System Users';

?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right m-b-10">
            <?php echo CHtml::link('Create', array('SysUsers/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?></div>
        <?php
        $exist = SysUsers::model()->findAll();
        if(count($exist) > 0){ ?>
        <div class="pull-left">
            <div class="btn-group dropdown ">
                <button class="btn dropdown-toggle field-list" data-toggle="dropdown">
                    <label>Select Fields</label>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu">
                    <li><input type="checkbox" class="hidecol" value="SelectAll" id="select_all">Select All</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'user-info-grid',
    'dataProvider'=>$model->search(),
    'enableSorting' => false,
    'enablePagination' => false,
    'type' => TbHtml::GRID_TYPE_BORDERED,
    'filter'=> null,
    'itemsCssClass' => 'js-dataTable-full',
    'columns'=>array(
        array(
            'header' => 'Action',
            'class' => 'CButtonColumn',
            'template' => '{view}{update}{delete}',
            'buttons' => [
                'view' => [
                    'title' => 'view',
                    'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/sysUsers/view/$data->id")',
                    'options' => array('class' => 'btn-view', 'title' => 'View'),
                ],
                'update' => [
                    'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/sysUsers/update/$data->id")',
                    'options' => array('class' => 'btn-update', 'title' => 'Edit'),
                    'visible' => 'Yii::app()->params[\'mandatoryFields\'][\'admin_id\'] != $data->id',
                ],
                'delete' => [
                    'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-times"></i></button>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/sysUsers/delete/$data->id")',
                    'options' => array('class' => 'btn-delete', 'title' => 'Delete'),
                    'visible' => 'Yii::app()->params[\'mandatoryFields\'][\'admin_id\'] != $data->id',
                ],
            ],
        ),
        'id',
        'username',
        'password',
        'email',
        'activekey',
        'auth_level',
        'status',
        array(
            'name' => 'created_at',
            'header' => 'Registration Date',
            'value' => 'Yii::app()->dateFormatter->format("yyyy-MM-dd",strtotime($data->created_at))',
        ),
    ),
)); } else {?>
    <div class="raw m-b-10">
        <span class="empty">No results found.</span>
    </div>
<?php } ?>
