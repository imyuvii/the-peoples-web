<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->pageTitle = 'Categories';
?>
<div class="pull-right m-b-10">
	<?php echo CHtml::link('Create', array('Categories/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?></div>

<?php 
$exist = Categories::model()->findAll();
if(count($exist) > 0){ ?>
	<div class="pull-left">
		<div class="btn-group dropdown ">
			<button class="form-group btn dropdown-toggle field-list" data-toggle="dropdown">
				<label>Select Fields</label>
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu"></ul>
		</div>
	</div>
	<div class="error-msg">
<?php if(Yii::app()->user->hasFlash('delete-error')){ 
				echo Yii::app()->user->getFlash('delete-error'); 
			Yii::app()->user->setFlash('delete-error', null); 
	}?>
	</div>
	<br/><br/>
<?php 
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'categories-grid',
	'dataProvider'=>$model->search(),
	'enableSorting' => false,
	'enablePagination' => false,
	'type' => TbHtml::GRID_TYPE_BORDERED,
	'filter'=> $model,
	'summaryText' => false,
	'itemsCssClass' => 'js-dataTable-full',
	'columns'=>array(
		array(
			'class'=>'CButtonColumn',
			'header' => 'Action',
			'template' => '{view}{update}{delete}',
			'buttons' => [
				'view' => [
				'title' => 'view',
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/categories/view/$data->category_id")',
				'options' => array('class' => 'btn-view', 'title' => 'View'),
				],
				'update' => [
				'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/categories/update/$data->category_id")',
				'options' => array('class' => 'btn-update', 'title' => 'Edit'),
				'visible' => 'Yii::app()->params[\'mandatoryFields\'][\'category_id\'] != $data->category_id',
				],
				'delete' => [
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-times"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/categories/delete/$data->category_id")',
				'options' => array('class' => 'btn-delete', 'title' => 'Delete'),
				'visible' => 'Yii::app()->params[\'mandatoryFields\'][\'category_id\'] != $data->category_id',
			],
			],
		),
		'category_id',
		'category_name',
		'description',
		'is_active',
	),
)); } else {?>
	<div class="raw m-b-10">
		<span class="empty">No results found.</span>
	</div>
<?php } ?>