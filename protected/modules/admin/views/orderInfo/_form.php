<?php
/* @var $this OrderInfoController */
/* @var $model OrderInfo */
/* @var $form CActiveForm */
?>
<style>
	.selected-address {
		padding: 10px 15px;
		border: 1px solid #eee;
		border-radius: 10px;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="block">
			<div class="block-content block-content-narrow">
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'order-info-form',
				'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
				'enableAjaxValidation'=>false,
				'htmlOptions' => array('enctype' => 'multipart/form-data', 'name' => 'CreateOrder'),
			)); ?>

<!--				<div class="form-material has-error">-->
<!--					<p id="productError" class="help-block has-error" style="display: none;"></p>-->
<!--				</div>-->
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-12">
							<div class="form-group">
								<?php
								$usersList = CHtml::listData(UserInfo::model()->findAll(["order" => "full_name"]), "user_id",
									function ($data){
										return "{$data->first_name}  {$data->last_name}";
									});
								echo $form->dropDownListControlGroup($model, "user_id", $usersList, [
									"prompt" => "Select User",
									"class" => "js-select2 form-control",
								]);
								?>
							</div>
						</div>
											</div>
				</div>
				<div class="row">
					<div class="col-md-12">
												<div class="col-md-12 hide" id="address-selector">
							<div class="form-group">
								<div class="control-group">
									<label class="control-label required" for="OrderInfo_user_id">Addresses <span class="required">*</span></label>
									<div class="controls">
										<input type="radio" id="homeAddr" name="select_address" value="0" checked/> <label for="homeAddr">Personal</label>&nbsp;&nbsp;
										<input type="radio" id="workAddr" name="select_address" value="1"/> <label for="workAddr">Business</label>
									</div>
								</div>
							</div>
						</div>
												<div class="col-md-12 selected-address hide"></div>
					</div>
				</div>
				<div class="block hide">
					<div class="block-header">
						<!--						<h3 class="block-title">Add Product</h3>-->
					</div>
					<h4>Address</h4>
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12 ">
								<div class="form-group <?php echo $model->hasErrors('vat_number') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'vat_number', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('company') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'company', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('building') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'building', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('street') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'street', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
						</div>
						<div class="col-md-6">

							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('city') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'city', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('region') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'region', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('country') ? 'has-error' : ''; ?> ">
									<?php echo $form->dropDownListControlGroup($model, 'country', Yii::app()->ServiceHelper->getNationalOptions(), array('prompt' => 'Select Country', 'class' => 'js-select2 form-control')); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group <?php echo $model->hasErrors('postcode') ? 'has-error' : ''; ?> ">
									<?php echo $form->textFieldControlGroup($model, 'postcode', array('autofocus' => 'on', 'class' => 'form-control')); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="block">
					<div class="block-header">
						<!--						<h3 class="block-title">Add Product</h3>-->
					</div>
					<div class="">
						<h4>Product</h4>
						<div class="row">
							<div class="col-md-12">
								<table class="table ">
									<thead>
									<tr>
										<th>Product Name</th>
										<th class="text-center">Qty</th>
										<th class="text-center">Discount</th>
										<th class="text-center">Price</th>
										<th class="text-center">Action</th>
									</tr>
									</thead>
									<tbody class="table" id="productControl">
									<tr class="addMoreProduct">
										<td class="col-md-6">
											<div class="col-md-12">
												<div class="form-group">
													<?php
													$productList = CHtml::listData(ProductInfo::model()->findAll(['order' => 'name']), 'product_id', 'name');
													echo $form->dropDownList($orderItem, 'product_id[]', $productList, [
														'prompt' => 'Select Product',
														'class' => 'form-control',
													]);
													?>
												</div>
											</div>
										</td>
										<td class="col-md-2">
											<div class="col-md-12">
												<div class="form-group <?php echo $orderItem->hasErrors('item_qty') ? 'has-error' : ''; ?> ">
													<?php echo $form->textField($orderItem, 'item_qty[]', [
														'autofocus' => 'on',
														'class' => 'form-control',
														'placeholder' => 'Qty',
//															'ajax' => [
//																'type'=>'POST',
//																'url'=>Yii::app()->createUrl('OrderInfo/loadprice'), //  get states list
//																//'update'=>'#itemPrice', // add the state dropdown id
//																'data'=>array('qty'=>'js:this.value','sku'=>'js:OrderLineItem_item_sku.value'),
//																'success'=>'function(result){
//																	alert(result);
//																}',
//															]
													]); ?>
												</div>
											</div>
										</td>
										<td class="col-md-2">
											<div class="col-md-12">
												<div class="form-group <?php echo $orderItem->hasErrors('item_disc') ? 'has-error' : ''; ?> ">
													<?php echo $form->textField($orderItem, 'item_disc[]', array('autofocus' => 'on','readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Discount')); ?>
												</div>
											</div>
										</td>
										<td class="col-md-2 text-center">
											<div class="col-md-12">
												<div class="form-group <?php echo $orderItem->hasErrors('item_price') ? 'has-error' : ''; ?> ">
													<?php echo $form->textField($orderItem, 'item_price[]', [
														'autofocus' => 'on',
														'readonly' => 'readonly',
														'class' => 'form-control',
														'placeholder' => 'Price',
														'id' => 'itemPrice'
													]); ?>
												</div>
											</div>
										</td>
										<td class="col-md-2 text-center">
											<div class="col-md-12">
												<button type="button" class="btn btn-success btn-add-product">
													<span>+</span>
												</button>
											</div>
										</td>
									</tr>
									<tr id="beforePrice">
										<td colspan="4" class="text-right"><strong>Total Price:</strong></td>
										<td class="text-right"  id="totalPrice">0</td>
									</tr>
									<tr>
										<td colspan="4" class="text-right"><strong>Total Discount:</strong></td>
										<td class="text-right"  id="totalDiscount">0</td>
									</tr>
									<tr class="success">
										<td colspan="4" class="text-right text-uppercase"><strong>Net Total:</strong></td>
										<td class="text-right"><strong  id="netTotal">0</strong></td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="block">
					<div class="">
						<h4>Payment</h4>
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_mode') ? 'has-error' : ''; ?> ">
										<?php
										$modeFieldId = CylFields::model()->findByAttributes([ 'field_name' => 'payment_mode']);
										$paymentModeList = CHtml::listData(CylFieldValues::model()->findAllByAttributes(['field_id' => $modeFieldId->field_id]), 'predefined_value', 'field_label');
										echo $form->dropDownListControlGroup($orderPayment, 'payment_mode', $paymentModeList, [
											'prompt' => 'Select Payment Mode',
											'class' => 'js-select2 form-control',
										]);
										?>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_ref_id') ? 'has-error' : ''; ?> ">
										<?php echo $form->textFieldControlGroup($orderPayment, 'payment_ref_id', array('autofocus' => 'on', 'class' => 'form-control')); ?>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_status') ? 'has-error' : ''; ?> ">
										<?php
										$fieldId = CylFields::model()->findByAttributes(['field_name' => 'order_status']);
										$statusList = CHtml::listData(CylFieldValues::model()->findAllByAttributes(['field_id' => $fieldId->field_id]), 'predefined_value', 'field_label');
										echo $form->dropDownListControlGroup($orderPayment, 'payment_status', $statusList, [
											'prompt' => 'Select Status',
											'class' => 'js-select2 form-control',
											'disabled' => 'disabled',
											'options' => array(1 =>array('selected'=>true))
										]);
										?>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_date') ? 'has-error' : ''; ?> ">
										<?php echo $form->labelEx($orderPayment, 'paymentDate', array('class' => 'control-label')); ?>
										<?php
										$this->widget('zii.widgets.jui.CJuiDatePicker', array(
											'model' => $orderPayment,
											'attribute' => 'payment_date',
											//'value'=>$model->dateOfBirth,
											// additional javascript options for the date picker plugin
											'options' => array(
												'showAnim' => '',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
												'dateFormat' => 'yy-mm-dd',
												'maxDate' => date('Y-m-d'),
												'changeYear' => true,           // can change year
												'changeMonth' => true,
												'yearRange' => '1900:' . date('Y'),
											),
											'htmlOptions' => array(
												'class' => 'form-control'
												//'style'=>'height:20px;background-color:green;color:white;',
											),
										));
										?>
										<span class="help-block"><?php echo $form->error($orderPayment, 'payment_date'); ?> </span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array(
							'class' => 'btn btn-primary',
						)); ?>
						<?php echo CHtml::link('Cancel', array('productInfo/admin'),
							array(
								'class' => 'btn btn-default'
							)
						);
						?>
					</div>
				</div>

		<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>

<script>
	/*

	 */
	function ProductPrice(productDetails) {
		var validQtyFlag = 0;
		// get selected product
		var productItem = productDetails.find('#OrderLineItem_product_id').val();
		// get entered quantity
		var productQty = productDetails.find('#OrderLineItem_item_qty').val();
		var validNum = /[^\d].+/;

		if(productItem != '' && productQty != ''){
			var data = {qty : productQty,productId : productItem};
			$.ajax({
				type : "POST",
				url : '<?php echo Yii::app()->getUrlManager()->createUrl('admin/OrderInfo/loadprice'); ?>',
				data : data,
				success : function(result){
					var productData = JSON.parse(result);
					if(productData.result == true){
						productDetails.find('#itemPrice').val(productData.productPrice);
						setPriceTotal();
					}
				}
			});
		}else{
			productDetails.find('#itemPrice').val('');
		}
	}

	// TODO: Select Address
	$(document).on('change','#OrderInfo_user_id',function (e) {
		$("#address-selector").addClass("hide");
		var data = {user_id : $(this).val()};
		$.ajax({
			type : "POST",
			url : '<?php echo Yii::app()->createUrl('admin/OrderInfo/getaddress'); ?>',
			data : data,
			success : function(result){
				var addressData = JSON.parse(result);
				if(addressData.result == true){
					if(addressData['userInfo']['business_name'] != 0) {
						if(addressData['userInfo']['business_name'] != "") {
							$("#address-selector").removeClass("hide");
						}
					}
					setAddress(0, addressData);
				} else {
					alert("could not load address")
				}
			}
		});
	});

	$("#homeAddr, #workAddr").change(function () {
		var data = {user_id : $('#OrderInfo_user_id').val()};
		var addressType = $(this).val();
		$.ajax({
			type : "POST",
			url : '<?php echo Yii::app()->createUrl('admin/OrderInfo/getaddress'); ?>',
			data : data,
			success : function(result){
				var addressData = JSON.parse(result);
				if(addressData.result == true){
					setAddress(addressType, addressData);
				} else {
					alert("could not load address")
				}
			}
		});
	});

	function setAddress(adType, addressData) {
		if(adType == 0) {
			var personalAddress = '';
			personalAddress += (addressData['userInfo']['full_name'] == 'NULL' || addressData['userInfo']['full_name'] == '0' || addressData['userInfo']['full_name'] === 0 || !addressData['userInfo']['full_name'])?'':"<b>" + addressData['userInfo']['full_name']+"</b>,<br/>";
			personalAddress += (addressData['userInfo']['building_num'] == 'NULL' || addressData['userInfo']['building_num'] == '0' || addressData['userInfo']['building_num'] === 0 || !addressData['userInfo']['building_num'] )?'':addressData['userInfo']['building_num']+", ";
			personalAddress += (addressData['userInfo']['street'] == 'NULL' || addressData['userInfo']['street'] == '0' || addressData['userInfo']['street'] === 0 || !addressData['userInfo']['street'])?'':addressData['userInfo']['street'] + ',<br/>';
			personalAddress += (addressData['userInfo']['city'] == 'NULL' || addressData['userInfo']['city'] == '0' || addressData['userInfo']['city'] === 0 || !addressData['userInfo']['city'])?'':addressData['userInfo']['city'] + ' - ';
			personalAddress += (addressData['userInfo']['postcode'] == 'NULL' || addressData['userInfo']['postcode'] == '0' || addressData['userInfo']['postcode'] === 0 || !addressData['userInfo']['postcode'] )?'':addressData['userInfo']['postcode'] + ', ';
			personalAddress += (addressData['userInfo']['region'] == 'NULL' || addressData['userInfo']['region'] == '0' || addressData['userInfo']['region'] === 0 || !addressData['userInfo']['region'] )?'':addressData['userInfo']['region'] + ', ';
			personalAddress += (addressData['userInfo']['country'] == 'NULL' || addressData['userInfo']['country'] == '0' || addressData['userInfo']['country'] === 0 || !addressData['userInfo']['country'] )?'':addressData['userInfo']['country'];

			$(".selected-address").removeClass("hide");
			$(".selected-address").html(personalAddress);

			$('#OrderInfo_vat_number').val('');
			$('#OrderInfo_company').val('');
			$('#OrderInfo_street').val(addressData['userInfo']['street']);
			$('#OrderInfo_city').val(addressData['userInfo']['city']);
			$('#OrderInfo_region').val(addressData['userInfo']['region']);
			$('#OrderInfo_country').val(addressData['userInfo']['country']);
			$('#OrderInfo_postcode').val(addressData['userInfo']['postcode']);

		} else {
			var businessAddr = '';
			businessAddr += (addressData['userInfo']['business_name'] == 'NULL' || addressData['userInfo']['business_name'] == '0' || addressData['userInfo']['business_name'] === 0 || !addressData['userInfo']['business_name'])?'':"<b>" + addressData['userInfo']['business_name']+"</b>,<br/>";
			businessAddr += (addressData['userInfo']['busAddress_building_num'] == 'NULL' || addressData['userInfo']['busAddress_building_num'] == '0' || addressData['userInfo']['busAddress_building_num'] === 0 || !addressData['userInfo']['busAddress_building_num'] )?'':addressData['userInfo']['busAddress_building_num']+", ";
			businessAddr += (addressData['userInfo']['busAddress_street'] == 'NULL' || addressData['userInfo']['busAddress_street'] == '0' || addressData['userInfo']['busAddress_street'] === 0 || !addressData['userInfo']['busAddress_street'] )?'':addressData['userInfo']['busAddress_street'] + ',<br/>';
			businessAddr += (addressData['userInfo']['busAddress_city'] == 'NULL' || addressData['userInfo']['busAddress_city'] == '0' || addressData['userInfo']['busAddress_city'] === 0 || !addressData['userInfo']['busAddress_city'] )?'':addressData['userInfo']['busAddress_city'] + ' - ';
			businessAddr += (addressData['userInfo']['busAddress_postcode'] == 'NULL' || addressData['userInfo']['busAddress_postcode'] == '0' || addressData['userInfo']['busAddress_postcode'] === 0 || !addressData['userInfo']['busAddress_postcode'] )?'':addressData['userInfo']['busAddress_postcode'] + ', ';
			businessAddr += (addressData['userInfo']['busAddress_region'] == 'NULL' || addressData['userInfo']['busAddress_region'] == '0' || addressData['userInfo']['busAddress_region'] === 0 || !addressData['userInfo']['busAddress_region'] )?'':addressData['userInfo']['busAddress_region'] + ', ';
			businessAddr += (addressData['userInfo']['busAddress_country'] == 'NULL' || addressData['userInfo']['busAddress_country'] == '0' || addressData['userInfo']['busAddress_country'] === 0 || !addressData['userInfo']['busAddress_country'] )?'':addressData['userInfo']['busAddress_country'] + ', <br/>';
			businessAddr += (addressData['userInfo']['vat_number'] == 'NULL' || addressData['userInfo']['vat_number'] == '0' || addressData['userInfo']['vat_number'] === 0 || !addressData['userInfo']['vat_number'] )?'':'' + addressData['userInfo']['vat_number'];

			$(".selected-address").removeClass("hide");
			$(".selected-address").html(businessAddr);

			$('#OrderInfo_vat_number').val(addressData['userInfo']['vat_number']);
			$('#OrderInfo_company').val(addressData['userInfo']['business_name']);
			$('#OrderInfo_building').val(addressData['userInfo']['busAddress_building_num']);
			$('#OrderInfo_street').val(addressData['userInfo']['busAddress_street']);
			$('#OrderInfo_city').val(addressData['userInfo']['busAddress_city']);
			$('#OrderInfo_region').val(addressData['userInfo']['busAddress_region']);
			$('#OrderInfo_country').val(addressData['userInfo']['busAddress_country']);
			$('#OrderInfo_postcode').val(addressData['userInfo']['busAddress_postcode']);
		}
	}

	//    function checkQty(productName,productQty){
	//        console.info();
	//        var data = {sku : productName,qty : productQty};
	//        $.ajax({
	//            type : "POST",
	//            url : '//',
	//            data : data,
	//            success : function(result){
	//                var productData = JSON.parse(result);
	//                if(productData.result == true){
	//                    productDetails.find('#itemPrice').val(productData.productPrice);
	//                    console.info('true');
	//                }else{
	//                    console.info('false');
	//                }
	//            },
	//        });
	//    }


</script>