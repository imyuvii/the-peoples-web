<?php
/* @var $this OrderInfoController */
/* @var $model OrderInfo */

$this->pageTitle = 'View Order';
$id = $model->order_info_id; 
?>
<div class="pull-right m-b-10">
	<?php echo CHtml::link('Go to list', array('orderInfo/admin'), array('class' => 'btn btn-minw btn-square btn-warning')); ?> 
	<?php echo CHtml::link('Create', array('orderInfo/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?> 
	<?php if($model->order_status != 1){ ?>
	<?php echo CHtml::link('Update', array('orderInfo/update/'.$id), array('class' => 'btn btn-minw btn-square btn-warning')); ?>
	<?php } ?>
</div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table'),
	'attributes'=>array(
	'order_info_id',
	'order_id',
	[
		'name' => 'user_id',
		'value' => function($model){
			$userName = UserInfo::model()->findByAttributes(['user_id' => $model->user_id]);
			return $userName->first_name . ' ' . $userName->last_name;
		}
	],
	'vat',
	'vat_number',
	'company',
	[
		'name' => 'order_status',
		'value' => function($model){
			$fieldId = CylFields::model()->findByAttributes(['field_name' => 'order_status']);
			$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->order_status]);
			return $orderStatus->field_label;
		}
	],
	'building',
	'street',
	'city',
	'region',
	'country',
	'postcode',
	'orderTotal',
	'discount',
	'netTotal',
	'invoice_number',
	'invoice_date',
	'created_date',
	'modified_date',

	[
		'name'=> 'payment_mode',
		'value' => function($model){
			$fieldId = CylFields::model()->findByAttributes(['field_name' => 'payment_mode']);
			$paymentMode = OrderPayment::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
			$paymentRow = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $paymentMode->payment_mode]);
			return $paymentRow->field_label;
		}
	],
	[
		'name'=> 'payment_ref_id',
		'value' => function($model){
			$paymentRef= OrderPayment::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
			return $paymentRef->payment_ref_id;
		}
	],
	[
		'name' => 'payment_status',
		'value' => function($model){
			$fieldId = CylFields::model()->findByAttributes(['field_name' => 'payment_status']);
			$paymentStatus = OrderPayment::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
			$paymentRow = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $paymentStatus->payment_status]);
			return $paymentRow->field_label;
		}
	],
	[
		'name'=> 'payment_date',
		'value' => function($model){
			$paymentRef= OrderPayment::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
			return $paymentRef->payment_date;
		}
	],
	[
		'name'=> 'created_at',
		'value' => function($model){
			$paymentRef= OrderPayment::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
			return $paymentRef->created_at;
		}
	],
	[
		'name'=> 'modified_at',
		'value' => function($model){
			$paymentRef= OrderPayment::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
			return $paymentRef->modified_at;
		}
	],
	),
)); ?>

<table class="table">
	<thead>
	<tr>
		<th>Product Name</th>
		<th>Product Qty</th>
		<th>Product Discount</th>
		<th>Product Price</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($itemModel as $key => $item){ ?>
		<tr>
			<td><?php
				$itemSku = ProductInfo::model()->findByAttributes(['product_id' => $item['product_id']]);
				echo $itemSku->name;
				?></td>
			<td><?php echo $item['item_qty']; ?></td>
			<td><?php echo $item['item_disc']; ?></td>
			<td><?php echo $item['item_price']; ?></td>
		</tr>
<?php } ?>
	</tbody>
</table>
