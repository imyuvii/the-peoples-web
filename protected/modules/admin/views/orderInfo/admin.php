<?php
/* @var $this OrderInfoController */
/* @var $model OrderInfo */

$this->pageTitle = 'Orders';


?>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right m-b-10">
			<?php echo CHtml::link('Create', array('orderInfo/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?>
		</div>
<?php
$orderData = OrderInfo::model()->findAll();
if($orderData != null){
?>		<div class="pull-left">
			<div class="btn-group dropdown ">
				<button class="form-group btn dropdown-toggle field-list" data-toggle="dropdown">
					<label>Select Fields</label>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu">
					<li><input type="checkbox" class="hidecol" value="SelectAll" id="select_all">Select All</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'order-info-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText' => false,
	'enableSorting' => false,
	'enablePagination' => false,
	'type' => TbHtml::GRID_TYPE_BORDERED,
	'itemsCssClass' => 'js-dataTable-full',
	'columns'=>array(
		[
			'class'=>'CButtonColumn',
			'template' => '{view}{update}{creditMemo}',
			'header' => 'Action',
			'buttons' => [
				'view' => [
				'title' => 'view',
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderInfo/view/$data->order_info_id")',
				'options' => array('class' => 'btn-view', 'title' => 'View'),
				],
				'update' => [
				'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderInfo/update/$data->order_info_id")',
				'options' => array('class' => 'btn-update', 'title' => 'Edit'),
				'visible' => '$data->order_status!=1',
				],
				'creditMemo' =>[
				'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-external-link"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderInfo/creditMemo/$data->order_info_id")',
				'options' => array('class' => 'btn-memo', 'title' => 'Credit Memo'),
				],
				'delete' => [
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-times"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderInfo/delete/$data->order_info_id")',
				'options' => array('class' => 'btn-delete', 'title' => 'Delete'),
				],
			],
		],
		'order_info_id',
		'order_id',
		[
			'name' => 'user_id',
			'value' => function($model){
				$userName = UserInfo::model()->findByAttributes(['user_id' => $model->user_id]);
				return $userName->first_name . ' ' . $userName->last_name;
			}
		],
		'vat',
		'vat_number',
		'company',
		[
			'name' => 'order_status',
			'value' => function($model){
				$fieldId = CylFields::model()->findByAttributes(['field_name' => 'order_status']);
				$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->order_status]);
				return $orderStatus->field_label;
			}
		],
		'building',
		'street',
		'city',
		'region',
		'country',
		'postcode',
		'orderTotal',
		'discount',
		'netTotal',
		'invoice_number',
		'invoice_date',
		'created_date',
		'modified_date',
	),
)); } else{ ?>
	<div class="raw m-b-10">
		<span class="empty">No results found.</span>
	</div>
<?php } ?>