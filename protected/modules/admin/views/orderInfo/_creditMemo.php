<?php
/* @var $this OrderInfoController */
/* @var $model OrderInfo */
/* @var $form CActiveForm */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="block">
            <div class="block-content block-content-narrow">
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'order-info-credit-form',
                    'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                )); ?>

                <div class="form-material has-error">
                    <p id="memoError" class="help-block has-error" style="display: none;"></p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php if($orderItem != NULL){ ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Product Name</th>
                                    <th class="text-center">Qty</th>
                                    <th>Qty Refunded</th>
                                    <th class="text-center">Amount</th>
                                    <th>Memo Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($orderItem as $key => $item) { ?>
                                <tr>
                                    <?php $productName = ProductInfo::model()->findByAttributes(['product_id' => $item['product_id']]) ?>
                                    <td class="col-md-2 text-center">
                                        <?php echo $productName->name ?>
                                        <span style="display: none" id="productId"><?php echo $item['product_id']; ?></span>
                                    </td>
                                    <td class="col-md-1 text-center" id="itemQty"><?php echo $item['item_qty']; ?></td>
                                    <td class="col-md-2">
                                        <div class="col-md-12">
                                            <input autofocus="autofocus" class="form-control" min="1" max="<?php echo $item['item_qty']; ?>" name="OrderCreditMemo[qty_refund]" id="OrderCreditMemo_qty_refund" type="number" value="1" >
                                        </div>
                                    </td>
                                    <td class="col-md-1 text-center">
                                        <?php echo $item['item_price']; ?>
                                    </td>
                                    <td class="col-md-2 col-md-12">
                                        <div class="form-group">
                                            <?php
                                            $fieldId = CylFields::model()->findByAttributes(['field_name' => 'memo_status']);
                                            $statusList = CHtml::listData(CylFieldValues::model()->findAllByAttributes(['field_id' => $fieldId->field_id]), 'predefined_value', 'field_label');
                                            echo $form->dropDownList($creditMemo , 'memo_status', $statusList, [
                                                'prompt' => 'Select Status',
                                                'class' => 'form-control',
                                                'disabled' => 'disabled',
                                                'options' => array(1 =>array('selected'=>true))
                                            ]);
                                            ?>
                                        </div>
                                    </td>
                                    <td class="col-md-2 text-center">
                                        <button class="btn btn-danger push-5-r push-10 btnCancel" type="button"><i class="fa fa-times"></i> Cancel</button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <?php } else { ?>
                            <span>No Product Found</span>
                        <?php } ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<script>
//    $(function () {
//        $(".btnCancel").confirm({
//            text: "Are you sure you want to delete that comment?",
//        });
//    });

    $(document).on('click',".btnCancel",function (e) {
        if (confirm('Are you sure you want to create Credit Memo ?')) {
            $(this).prev('span.text').remove();
            var item = $(this).parents('tr');
            var refundQty = item.find('#OrderCreditMemo_qty_refund').val();
            var productId = item.find('#productId').text();
            var status = item.find('#OrderCreditMemo_memo_status').val();

            var data = {
                productId : productId,
                qty : refundQty,
                orderId : <?php echo $model->order_info_id; ?>,
                invoiceNo : <?php echo $model->invoice_number; ?>,
                status : status,
            };
            console.info(data);
            $.ajax({
                type : "POST",
                url : '<?php echo Yii::app()->getUrlManager()->createUrl('admin/OrderInfo/creditMemoCreate'); ?>',
                data : data,
                success : function(result){
                    var productData = JSON.parse(result);
                    console.info(result);
                    if(productData.result == true){
                        window.location.reload();
                    }else{
                        $('#memoError').text(productData.error);
                        $('#memoError').css('display','block');
                    }
                }
            });
        }
        //console.info($(this).parents('tr'));

    });
</script>
