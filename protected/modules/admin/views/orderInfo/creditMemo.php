<?php
    /* @var $this OrderInfoController */
    /* @var $model OrderInfo */

$this->pageTitle = 'Create CreditMemo';

?>

<?php $this->renderPartial('_creditMemo', array('model'=>$model, 'orderItem' => $orderItem, 'creditMemo' => $creditMemo)); ?>