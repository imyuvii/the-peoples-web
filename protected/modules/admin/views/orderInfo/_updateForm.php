<?php
/* @var $this OrderInfoController */
/* @var $model OrderInfo */
/* @var $form CActiveForm */
?>

<div class="row">
	<div class="col-lg-12">
		<div class="block">
			<div class="block-content block-content-narrow">
				<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'order-info-form',
				'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
				'enableAjaxValidation'=>false,
				'htmlOptions' => array('enctype' => 'multipart/form-data'),
			)); ?>
				<?php echo $form->errorSummary($model); ?>
				<div class="form-material has-error">
					<p id="productError" class="help-block has-error" style="display: none;"></p>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-12">
							<div class="form-group">
								<?php
								$usersList = CHtml::listData(UserInfo::model()->findAll(["order" => "full_name"]), "user_id",
									function ($data){
										return "{$data->first_name}  {$data->last_name}";
									});
								echo $form->dropDownListControlGroup($model, "user_id", $usersList, [
									"prompt" => "Select User",
									"class" => "js-select2 form-control",
							
								]);
								?>
							</div>
						</div>

					</div>
				</div>
				<h4>Address</h4>
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-12 ">
							<div class="form-group <?php echo $model->hasErrors('vat_number') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'vat_number', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('company') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'company', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('building') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'building', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('street') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'street', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
					</div>
					<div class="col-md-6">

						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('city') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'city', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('region') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'region', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('country') ? 'has-error' : ''; ?>">
								<?php echo $form->dropDownListControlGroup($model, 'country', Yii::app()->ServiceHelper->getNationalOptions(), array('prompt' => 'Select Country', 'class' => 'js-select2 form-control')); ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group <?php echo $model->hasErrors('postcode') ? 'has-error' : ''; ?>">
								<?php echo $form->textFieldControlGroup($model, 'postcode', array('autofocus' => 'on', 'class' => 'form-control')); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="block">
					<div class="block-header">
						<!--						<h3 class="block-title">Add Product</h3>-->
					</div>
					<div class="">
						<h4>Product</h4>
						<div class="row">
							<div class="col-md-12">
								<table class="table">
									<thead>
									<tr>
										<th>Product Name</th>
										<th class="text-center">Qty</th>
										<th class="text-center">Discount</th>
										<th class="text-center">Price</th>
										<th class="text-center">Action</th>
									</tr>
									</thead>
									<tbody class="table" id="productControl">
									<?php
									$count = 1;
									foreach($orderItem as $key => $item){
										$count++;
									?>
										<tr class="addMoreProduct">
										<td class="col-md-6">
											<div class="col-md-12">
												<div class="form-group">
													<?php
													$productList = CHtml::listData(ProductInfo::model()->findAll(['order' => 'name']), 'product_id', 'name');
													echo $form->dropDownList($item, 'product_id[]', $productList, [
														'prompt' => 'Select Product',
														'class' => 'form-control',
                                                        'options' => array($item['product_id'] =>array('selected'=>true))
													]);
													?>
												</div>
											</div>
										</td>
										<td class="col-md-2">
											<div class="col-md-12">
												<div class="form-group <?php echo $item->hasErrors('item_qty') ? 'has-error' : ''; ?>">
													<input autofocus="autofocus" class="form-control" placeholder="Qty" name="OrderLineItem[item_qty][]" id="OrderLineItem_item_qty" value="<?php echo $item->attributes['item_qty'];  ?>" type="text">
<!--													-->												</div>
											</div>
										</td>
										<td class="col-md-2">
											<div class="col-md-12">
												<div class="form-group ">
													<input autofocus="autofocus" readonly="readonly" class="form-control" placeholder="Discount" name="OrderLineItem[item_disc][]" id="OrderLineItem_item_disc" value="<?php echo $item->attributes['item_disc'];  ?>" type="text">
													<?php //echo $form->textField($orderItem, 'item_disc[]', array('autofocus' => 'on','readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Discount')); ?>
												</div>
											</div>
										</td>
										<td class="col-md-2 text-center">
											<div class="col-md-12">
												<div class="form-group ">
													<input autofocus="autofocus" readonly="readonly" class="form-control" placeholder="Price" id="itemPrice" name="OrderLineItem[item_price][]" value="<?php echo $item->attributes['item_price'];  ?>" type="text">
<!--													-->												</div>
											</div>
										</td>
										<td class="col-md-2 text-center">
											<div class="col-md-12">
												<?php
                                            end($orderItem);
                                            if($key == key($orderItem)){ ?>
                                                <button type="button" class="btn btn-success btn-add-product">
                                                    <span>+</span>
                                                </button>
												<?php }else{ ?>
                                                <button type="button" class="btn btn-danger btn-remove-product">
                                                    <span>-</span>
                                                </button>
												<?php } ?>
											</div>
										</td>
									</tr>
									<?php } ?>
									<tr id="beforePrice">
										<td colspan="4" class="text-right"><strong>Total Price:</strong></td>
										<td class="text-right"  id="totalPrice"><?php echo $model['orderTotal']; ?></td>
									</tr>
									<tr>
										<td colspan="4" class="text-right"><strong>Total Discount:</strong></td>
										<td class="text-right"  id="totalDiscount">0</td>
									</tr>
									<tr class="success">
										<td colspan="4" class="text-right text-uppercase"><strong>Net Total:</strong></td>
										<td class="text-right"><strong  id="netTotal"><?php echo $model['netTotal']; ?></strong></td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="block">
					<div class="">
						<h4>Payment</h4>
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_mode') ? 'has-error' : ''; ?>">
										<?php
										$modeFieldId = CylFields::model()->findByAttributes([ 'field_name' => 'payment_mode']);
										$paymentModeList = CHtml::listData(CylFieldValues::model()->findAllByAttributes(['field_id' => $modeFieldId->field_id]), 'predefined_value', 'field_label');
										echo $form->dropDownListControlGroup($orderPayment, 'payment_mode', $paymentModeList, [
											'prompt' => 'Select Payment Mode',
											'class' => 'js-select2 form-control',
										]);
										?>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_ref_id') ? 'has-error' : ''; ?>">
										<?php echo $form->textFieldControlGroup($orderPayment, 'payment_ref_id', array('autofocus' => 'on', 'class' => 'form-control')); ?>
									</div>
								</div>

							</div>
							<div class="col-md-6">
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_status') ? 'has-error' : ''; ?>">
										<?php
										$fieldId = CylFields::model()->findByAttributes(['field_name' => 'order_status']);
										$statusList = CHtml::listData(CylFieldValues::model()->findAllByAttributes(['field_id' => $fieldId->field_id]), 'predefined_value', 'field_label');
										echo $form->dropDownListControlGroup($orderPayment, 'payment_status', $statusList, [
											'prompt' => 'Select Status',
											'class' => 'form-control',
											'disabled' => 'disabled',
											'options' => array(1 =>array('selected'=>true))
										]);
										?>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group <?php echo $orderPayment->hasErrors('payment_date') ? 'has-error' : ''; ?>">
										<?php echo $form->labelEx($orderPayment, 'paymentDate', array('class' => 'control-label')); ?>
										<?php
										$this->widget('zii.widgets.jui.CJuiDatePicker', array(
											'model' => $orderPayment,
											'attribute' => 'payment_date',
											//'value'=>$model->dateOfBirth,
											// additional javascript options for the date picker plugin
											'options' => array(
												'showAnim' => '',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
												'dateFormat' => 'yy-mm-dd',
												'maxDate' => date('Y-m-d'),
												'changeYear' => true,           // can change year
												'changeMonth' => true,
												'yearRange' => '1900:' . date('Y'),
											),
											'htmlOptions' => array(
												'class' => 'form-control'
												//'style'=>'height:20px;background-color:green;color:white;',
											),
										));
										?>
										<span class="help-block"><?php echo $form->error($orderPayment, 'date_of_birth'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array(
							'class' => 'btn btn-primary',
						)); ?>
						<?php echo CHtml::link('Cancel', array('productInfo/admin'),
							array(
								'class' => 'btn btn-default'
							)
						);
						?>
					</div>
				</div>

				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</div>

<script>
	/*

	 */
	function ProductPrice(productDetails) {
		var validQtyFlag = 0;
		// get selected product
		var productItem = productDetails.find('#OrderLineItem_product_id').val();
		// get entered quantity
		var productQty = productDetails.find('#OrderLineItem_item_qty').val();
		var validNum = /[^\d].+/;

		if(productItem != '' && productQty != ''){
			var data = {qty : productQty,productId : productItem};
			$.ajax({
				type : "POST",
				url : '<?php echo Yii::app()->getUrlManager()->createUrl('admin/OrderInfo/loadprice'); ?>',
				data : data,
				success : function(result){
					var productData = JSON.parse(result);
					if(productData.result == true){
						productDetails.find('#itemPrice').val(productData.productPrice);
						setPriceTotal();
					}
				}
			});
		}else{
			productDetails.find('#itemPrice').val('');
		}
	}

	// Select Address
	$(document).on('change','#orderAddress',function (e) {
		var data = {addressMapId : $(this).val()};
		$.ajax({
			type : "POST",
			url : '<?php echo Yii::app()->createUrl('admin/OrderInfo/getaddress'); ?>',
			data : data,
			success : function(result){
				var addressData = JSON.parse(result);
				if(addressData.result == true){
					$('#OrderInfo_vat_number').val(addressData['orderAddress']['vat_number']);
					$('#OrderInfo_company').val(addressData['orderAddress']['company_name']);
					$('#OrderInfo_building').val(addressData['orderAddress']['building_no']);
					$('#OrderInfo_street').val(addressData['orderAddress']['street']);
					$('#OrderInfo_city').val(addressData['orderAddress']['city']);
					$('#OrderInfo_region').val(addressData['orderAddress']['region']);
					$('#OrderInfo_country').val(addressData['orderAddress']['country']);
					$('#OrderInfo_postcode').val(addressData['orderAddress']['postcode']);
				}
			}
		});
	});

	//    function checkQty(productName,productQty){
	//        console.info();
	//        var data = {sku : productName,qty : productQty};
	//        $.ajax({
	//            type : "POST",
	//            url : '//',
	//            data : data,
	//            success : function(result){
	//                var productData = JSON.parse(result);
	//                if(productData.result == true){
	//                    productDetails.find('#itemPrice').val(productData.productPrice);
	//                    console.info('true');
	//                }else{
	//                    console.info('false');
	//                }
	//            },
	//        });
	//    }
</script>