<?php $this->pageTitle = ''; ?>
<div class="content bg-image overflow-hidden"
	 style="background-image: url('<?php echo Yii::app()->createUrl('css') ?>/photo3@2x.jpg');">
	<div class="push-50-t push-15">
		<h1 class="h2 text-white animated zoomIn">Dashboard</h1>
		<h2 class="h5 text-white-op animated zoomIn">Welcome Administrator</h2>
	</div>
</div>
<!-- Page Content -->
<div class="content border-b">
	<div class="row items-push text-uppercase">

		<div class="col-xs-6 col-sm-3 <?php echo $navigation['user']; ?> module-box">
			<div class="font-w700 text-gray-darker animated fadeIn">Users
				<a href="<?php echo Yii::app()->createUrl('/admin/userInfo/admin') ?>">
					<div class="text-muted animated fadeIn pull-right">
						<img src="<?php echo Yii::app()->createUrl('/../admin/plugins/images/users.png') ?>">
					</div>
				</a>
			</div>
			<?php
			if (Yii::app()->db->schema->getTable('user_info')) {
				$users = UserInfo::model()->findAll(); ?>
				<a class="h2 font-w300 text-primary" href="<?php echo Yii::app()->createUrl('/userInfo/admin') ?>"
				   data-toggle="countTo" data-to="<?php echo count($users); ?>"><?php echo count($users); ?></a>
				<?php
			}
			?>

		</div>

		<div class="col-xs-6 col-sm-3 module-box <?php echo $navigation['product']; ?>">
			<div class="font-w700 text-gray-darker animated fadeIn">Products
				<a href="<?php echo Yii::app()->createUrl('/admin/productInfo/admin') ?>">
					<div class="text-muted animated fadeIn pull-right">
						<img src="<?php echo Yii::app()->createUrl('/../admin/plugins/images/product.png') ?>">
					</div>
				</a>
			</div>
			<?php
			if (Yii::app()->db->schema->getTable('product_info')) {
				$products = ProductInfo::model()->findAll(); ?>
				<a class="h2 font-w300 text-primary" href="<?php echo Yii::app()->createUrl('/ProductInfo/admin') ?>"
				   data-toggle="countTo" data-to="<?php echo count($products); ?>"><?php echo count($products); ?></a>
			<?php } ?>
		</div>

		<div class="col-xs-6 col-sm-3 module-box <?php echo $navigation['order']; ?>">
			<div class="font-w700 text-gray-darker animated fadeIn">Order
				<a href="<?php echo Yii::app()->createUrl('/admin/OrderInfo/admin') ?>">
					<div class="text-muted animated fadeIn pull-right">
						<img src="<?php echo Yii::app()->createUrl('/../admin/plugins/images/order.png') ?>">
					</div>
				</a>
			</div>
			<?php
			if (Yii::app()->db->schema->getTable('order_info')) {
				$orders = OrderInfo::model()->findAll(); ?>
				<a class="h2 font-w300 text-primary" href="<?php echo Yii::app()->createUrl('/admin/OrderInfo/admin') ?>"
				   data-toggle="countTo" data-to="<?php echo count($orders); ?>"><?php echo count($orders); ?></a>
				<?php
			}
			?>
		</div>

		<div class="col-xs-6 col-sm-3 module-box <?php echo $navigation['wallet']; ?>">
			<div class="font-w700 text-gray-darker animated fadeIn">Wallet
				<a href="<?php echo Yii::app()->createUrl('/admin/wallet/admin') ?>">
					<div class="text-muted animated fadeIn pull-right">
						<img src="<?php echo Yii::app()->createUrl('/../admin/plugins/images/wallet.png') ?>">
					</div>
				</a>
			</div>
			<?php
			if (Yii::app()->db->schema->getTable('wallet')) {
				$wallet = Wallet::model()->findAll(); ?>
				<a class="h2 font-w300 text-primary" href="<?php echo Yii::app()->createUrl('/wallet/admin') ?>"
				   data-toggle="countTo" data-to="<?php echo count($wallet); ?>"><?php echo count($wallet); ?></a>
			<?php } ?>
		</div>

		<div class="col-xs-6 col-sm-3 module-box <?php echo $navigation['report']; ?>">
			<div class="font-w700 text-gray-darker animated fadeIn">Report
				<a href="<?php echo Yii::app()->createUrl('/admin/report/admin') ?>">
					<div class="text-muted animated fadeIn pull-right">
						<img src="<?php echo Yii::app()->createUrl('/../admin/plugins/images/reports.png') ?>">
					</div>
				</a>
			</div>
			<a class="h2 font-w300 text-primary" href="<?php echo Yii::app()->createUrl('/report/admin') ?>"
			   data-toggle="countTo" data-to=""></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-8">
			<!-- Main Dashboard Chart -->
			<div class="block <?php echo $navigation['user']; ?>">
				<div class="block-header">
					<ul class="block-options">
						<li>
							<button type="button" data-toggle="block-option" data-action="refresh_toggle"
									data-action-mode="demo"><i class="si si-refresh"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Users Overview</h3>
				</div>
				<div class="block-content block-content-full bg-gray-lighter text-center">
					<!-- Chart.js Charts (initialized in js/pages/base_pages_dashboard.js), for more examples you can check out http://www.chartjs.org/docs/ -->
					<div style="height: 374px;">
						<canvas class="js-dash-chartjs-lines"></canvas>
					</div>
				</div>
				<div class="block-content text-center">
					<div class="row items-push text-center">
						<div class="col-xs-6 col-lg-3">
							<div class="push-10"><i class="si si-user-follow fa-2x"></i></div>
							<div class="font-s12 font-w700">Today</div>
							<div class="h5 font-w300 text-muted">
								<?php
								$timestamp = strtotime(date('Y-m-d H:i:s'));
								$U24 = date('Y-m-d H:i:s', $timestamp - 86400);
								$U7day = date('Y-m-d H:i:s', $timestamp - 604800);
								$ULastMonth = date('Y-m-d H:i:s', $timestamp - 2628000);
								if (Yii::app()->db->schema->getTable('user_info')) {
									$User24 = UserInfo::model()->findAll(array(
										'condition' => 'created_at >= :date',
										'params' => array(':date' => $U24),
									));
									print_r(count($User24));
								}
								?>
							</div>
						</div>
						<div class="col-xs-6 col-lg-3">
							<div class="push-10"><i class="si si-user-follow fa-2x"></i></div>
							<div class="font-s12 font-w700">Last Week</div>
							<div class="h5 font-w300 text-muted">
								<?php
								if (Yii::app()->db->schema->getTable('user_info')) {
									$User7 = UserInfo::model()->findAll(array(
										'condition' => 'created_at >= :date',
										'params' => array(':date' => $U7day),
									));
									print_r(count($User7));
								}
								?>
							</div>
						</div>
						<div class="col-xs-6 col-lg-3 visible-lg">
							<div class="push-10"><i class="si si-user-follow fa-2x"></i></div>
							<div class="font-s12 font-w700">Last Month</div>
							<div class="h5 font-w300 text-muted">
								<?php
								if (Yii::app()->db->schema->getTable('user_info')) {
									$UserLastMonth = UserInfo::model()->findAll(array(
										'condition' => 'created_at >= :date',
										'params' => array(':date' => $ULastMonth),
									));
									print_r(count($UserLastMonth));
								}
								?>
							</div>
						</div>
						<div class="col-xs-6 col-lg-3 visible-lg">
							<div class="push-10"><i class="si si-users fa-2x"></i></div>
							<div class="font-s12 font-w700">All Users</div>
							<div class="h5 font-w300 text-muted">
								<?php
								if (Yii::app()->db->schema->getTable('user_info')) {
									$TotalUser = UserInfo::model()->findAll();
									print_r(count($TotalUser));
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END Main Dashboard Chart -->
		</div>
		<div class="col-lg-4">
			<!-- Latest Sales Widget -->
			<div class="block <?php echo $navigation['order']; ?>">
				<div class="block-header">
					<ul class="block-options">
						<li>
							<button type="button" data-toggle="block-option" data-action="refresh_toggle"
									data-action-mode="demo"><i class="si si-refresh"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Latest Orders</h3>
				</div>
				<div class="block-content bg-gray-lighter">
					<div class="row items-push">
						<?php
						$timestamp = strtotime(date('Y-m-d H:i:s'));
						$last24 = $timestamp - 86400;
						$l24 = date('Y-m-d H:i:s', $last24);
						$last7day = date('Y-m-d H:i:s', $timestamp - 604800);
						if (Yii::app()->db->schema->getTable('order_info') && OrderInfo::model()->findAll()) {
							$orders = OrderInfo::model()->findAll(array(
								'condition' => 'created_date >= :date',
								'params' => array(':date' => $l24),
							));
							?>
							<div class="col-xs-4">
								<div class="text-muted">
									<small><i class="si si-calendar"></i> 24 hrs</small>
								</div>
								<div class="font-w600">
									<?php
									print_r(count($orders));
									?>
								</div>
							</div>
						<?php }else{
							?>
							<div class="text-muted text-center">
								<small><i class="si si-calendar"></i> No Orders</small>
							</div>
							<?php
						} ?>
						<?php
						if (Yii::app()->db->schema->getTable('order_info') && OrderInfo::model()->findAll()) {
							$orders = OrderInfo::model()->findAll(array(
								'condition' => 'created_date >= :date',
								'params' => array(':date' => $last7day),
							));
							?>
							<div class="col-xs-4">
								<div class="text-muted">
									<small><i class="si si-calendar"></i> 7 days</small>
								</div>
								<div class="font-w600">
									<?php
									print_r(count($orders));
									?>
								</div>
							</div>
							<?php
						}
						if (Yii::app()->db->schema->getTable('order_info') && OrderInfo::model()->findAll()) {
							$total = OrderInfo::model()->findAll();
							foreach ($total as $totalA) {
								$totalAmt[] = $totalA->netTotal;
							}
							?>
							<div class="col-xs-4">
								<div class="text-muted">
									<small><i class="si si-dollar"></i>Total Sale</small>
								</div>
								<div class="font-w600">
									$<?php echo array_sum($totalAmt) ?>
								</div>
							</div>
							<?php
						}
						?>


					</div>
				</div>

				<?php
				if (Yii::app()->db->schema->getTable('order_info') && OrderInfo::model()->findAll()) {
					?>
					<div class="block-content">
						<div class="pull-t pull-r-l">
							<!-- Slick slider (.js-slider class is initialized in App() -> uiHelperSlick()) -->
							<!-- For more info and examples you can check out http://kenwheeler.github.io/slick/ -->

							<div class="js-slider remove-margin-b" data-slider-autoplay="true"
								 data-slider-autoplay-speed="2500">
								<div>
									<table class="table remove-margin-b font-s13">
										<thead>
										<tr>
											<th class="font-w600">User</th>
											<th class="font-w600 text-right" style="width: 120px;">Product Qty</th>
											<th class="font-w600 text-right" style="width: 100px;">Amount</th>
										</tr>
										</thead>
										<tbody>
										<?php
										$count = 0;
										if (Yii::app()->db->schema->getTable('order_info')) {
											$Orders = OrderInfo::model()->findAll(array(
												"order" => "order_id DESC",
												"limit" => 16,
											));
											foreach ($Orders as $ord) {
												$count++;
												if ($count <= 8) {
													?>
													<tr>
														<?php $user = UserInfo::model()->findByAttributes(['user_id' => $ord->user_id]) ?>
														<td class="font-w600">
															<a href="javascript:void(0)"><?php echo $user->full_name ?></a>
														</td>
														<td class="hidden-xs text-muted text-right"
															style="width: 100px;">
															<?php
															$ord_line_item = OrderLineItem::model()->findAllByAttributes(['order_info_id' => $ord->order_info_id]);
															echo count($ord_line_item);
															?>
														</td>
														<?php
														$order_info = OrderInfo::model()->findByAttributes(['order_info_id' => $ord->order_info_id]);
														?>
														<td class="font-w600 text-success text-right"
															style="width: 100px;">
															$<?php echo $order_info->netTotal ?></td>
													</tr>
													<?php
												}
											}
										}
										?>
										</tbody>
									</table>
								</div>
								<div>
									<table class="table remove-margin-b font-s13">
										<thead>
										<tr>
											<th class="font-w600">User</th>
											<th class="font-w600 text-right" style="width: 120px;">Product Qty</th>
											<th class="font-w600 text-right" style="width: 100px;">Amount</th>
										</tr>
										</thead>
										<tbody>
										<?php
										$count = 0;
										if (Yii::app()->db->schema->getTable('order_info')) {
											$Orders = OrderInfo::model()->findAll(array(
												"order" => "order_id DESC",
												"limit" => 16,
											));

											foreach ($Orders as $ord) {
												$count++;
												if ($count > 8) {
													?>
													<tr>
														<?php $user = UserInfo::model()->findByAttributes(['user_id' => $ord->user_id]) ?>
														<td class="font-w600">
															<a href="javascript:void(0)"><?php echo $user->full_name ?></a>
														</td>
														<td class="hidden-xs text-muted text-right"
															style="width: 100px;">
															<?php
															$ord_line_item = OrderLineItem::model()->findAllByAttributes(['order_info_id' => $ord->order_info_id]);
															echo count($ord_line_item);
															?>
														</td>
														<?php
														$order_info = OrderInfo::model()->findByAttributes(['order_info_id' => $ord->order_info_id]);
														?>
														<td class="font-w600 text-success text-right"
															style="width: 100px;">
															$<?php echo $order_info->netTotal ?></td>
													</tr>
													<?php
												}
											}
										}

										?>
										</tbody>
									</table>
								</div>
							</div>

							<!-- END Slick slider -->
						</div>
					</div>
				<?php } else {
				} ?>

			</div>
			<!-- END Latest Sales Widget -->
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="block <?php echo $navigation['wallet']; ?>">
				<div class="block-content bg-gray-lighter">
					<div class="row items-push">
						<div class="col-sm-12">
							<h1 class="text-center">
								Wallet Summary
							</h1>
						</div>
					</div>
				</div>
				<div class="block-content">
					<div class="pull-t pull-r-l">
						<div class="col-sm-12 bg-gray-lighter">
							<?php
							if (Yii::app()->db->schema->getTable('wallet')) {
								$Wallets = WalletTypeEntity::model()->findAll();
								foreach ($Wallets as $wall) {
									?>
									<div class="col-sm-3">
										<a class="block block-link-hover3 text-center" href="javascript:void(0)">
											<div class="block-content block-content-full">
												<div class="h1 font-w700">$
													<span data-toggle="countTo" data-to="8970"></span></div>
											</div>
											<div
												class="block-content block-content-full block-content-mini bg-primary-op font-w600">
												<?php echo $wall->wallet_type ?>
											</div>
										</a>
									</div>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
<?php
$current_year = date("Y");
if (Yii::app()->db->schema->getTable('user_info')) {
	$Jancriteria = new CDbCriteria;
	$Jancriteria->condition = "created_at >= '$current_year-01-01 00:00:00' AND created_at <= '$current_year-01-31 23:59:59'";
	$janUser = UserInfo::model()->find($Jancriteria);

	$Febcriteria = new CDbCriteria;
	$Febcriteria->condition = "created_at >= '$current_year-02-01 00:00:00' AND created_at <= '$current_year-02-28 23:59:59'";
	$febUser = UserInfo::model()->findAll($Febcriteria);

	$Marcriteria = new CDbCriteria;
	$Marcriteria->condition = "created_at >= '$current_year-03-01 00:00:00' AND created_at <= '$current_year-03-31 23:59:59'";
	$marUser = UserInfo::model()->findAll($Marcriteria);

	$Aprcriteria = new CDbCriteria;
	$Aprcriteria->condition = "created_at >= '$current_year-04-01 00:00:00' AND created_at <= '$current_year-04-30 23:59:59'";
	$aprUser = UserInfo::model()->findAll($Aprcriteria);

	$Maycriteria = new CDbCriteria;
	$Maycriteria->condition = "created_at >= '$current_year-05-01 00:00:00' AND created_at <= '$current_year-05-31 23:59:59'";
	$mayUser = UserInfo::model()->findAll($Maycriteria);

	$Juncriteria = new CDbCriteria;
	$Juncriteria->condition = "created_at >= '$current_year-06-01 00:00:00' AND created_at <= '$current_year-06-30 23:59:59'";
	$junUser = UserInfo::model()->findAll($Juncriteria);

	$Julcriteria = new CDbCriteria;
	$Julcriteria->condition = "created_at >= '$current_year-07-01 00:00:00' AND created_at <= '$current_year-07-31 23:59:59'";
	$julUser = UserInfo::model()->findAll($Julcriteria);

	$Augcriteria = new CDbCriteria;
	$Augcriteria->condition = "created_at >= '$current_year-08-01 00:00:00' AND created_at <= '$current_year-08-31 23:59:59'";
	$augUser = UserInfo::model()->findAll($Augcriteria);

	$Sepcriteria = new CDbCriteria;
	$Sepcriteria->condition = "created_at >= '$current_year-09-01 00:00:00' AND created_at <= '$current_year-09-30 23:59:59'";
	$sepUser = UserInfo::model()->findAll($Sepcriteria);

	$Octcriteria = new CDbCriteria;
	$Octcriteria->condition = "created_at >= '$current_year-10-01 00:00:00' AND created_at <= '$current_year-10-31 23:59:59'";
	$octUser = UserInfo::model()->findAll($Octcriteria);

	$Novcriteria = new CDbCriteria;
	$Novcriteria->condition = "created_at >= '$current_year-11-01 00:00:00' AND created_at <= '$current_year-11-30 23:59:59'";
	$novUser = UserInfo::model()->findAll($Novcriteria);

	$Deccriteria = new CDbCriteria;
	$Deccriteria->condition = "created_at >= '$current_year-12-01 00:00:00' AND created_at <= '$current_year-12-31 23:59:59'";
	$decUser = UserInfo::model()->findAll($Deccriteria);
}

?>
<script src="<?php echo Yii::app()->createUrl('/../plugins/js/plugins/slick/slick.min.js'); ?>"></script>
<script src="<?php echo Yii::app()->createUrl('/../plugins/js/plugins/chartjs/Chart.min.js'); ?>"></script>
<script src="<?php echo Yii::app()->createUrl('/../plugins/js/core/jquery.countTo.min.js'); ?>"></script>

<script>
	jQuery(function () {
		// Init page helpers (Slick Slider plugin)
		App.initHelpers('slick');
	});

	jQuery(function () {
		// Init page helpers (Appear + CountTo plugins)
		App.initHelpers(['appear', 'appear-countTo']);
	});

	/*
	 *  Document   : base_pages_dashboard.js
	 *  Author     : pixelcave
	 *  Description: Custom JS code used in Dashboard Page
	 */

	var BasePagesDashboard = function () {
		// Chart.js Chart, for more examples you can check out http://www.chartjs.org/docs
		var initDashChartJS = function () {
			// Get Chart Container
			var $dashChartLinesCon = jQuery('.js-dash-chartjs-lines')[0].getContext('2d');

			// Set Chart and Chart Data variables
			var $dashChartLines, $dashChartLinesData;

			// Lines Chart Data
			var $dashChartLinesData = {
				labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				datasets: [
					{
						label: 'Last Week',
						fillColor: 'rgba(44, 52, 63, .1)',
						strokeColor: 'rgba(44, 52, 63, .55)',
						pointColor: 'rgba(44, 52, 63, .55)',
						pointStrokeColor: '#fff',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(44, 52, 63, 1)',
						data: [
							<?php
							if (Yii::app()->db->schema->getTable('user_info')) {
							echo count($janUser); ?>,
							<?php echo count($febUser); ?>,
							<?php echo count($marUser); ?>,
							<?php echo count($aprUser); ?>,
							<?php echo count($mayUser); ?>,
							<?php echo count($junUser); ?>,
							<?php echo count($julUser); ?>,
							<?php echo count($augUser); ?>,
							<?php echo count($sepUser); ?>,
							<?php echo count($octUser); ?>,
							<?php echo count($novUser); ?>,
							<?php echo count($decUser);
							}
							?>

						]
					}
				]
			};

			// Init Lines Chart
			$dashChartLines = new Chart($dashChartLinesCon).Line($dashChartLinesData, {
				scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
				scaleFontColor: '#999',
				scaleFontStyle: '600',
				tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
				tooltipCornerRadius: 3,
				maintainAspectRatio: false,
				responsive: true
			});
		};

		return {
			init: function () {
				// Init ChartJS chart
				initDashChartJS();
			}
		};
	}();

	// Initialize when page loads
	jQuery(function () {
		BasePagesDashboard.init();
	});

</script>