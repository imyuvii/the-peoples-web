<?php
/* @var $this ProductInfoController */
/* @var $model ProductInfo */

$this->pageTitle = 'Products';

?>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right m-b-10">
		<?php echo CHtml::link('Create', array('productInfo/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?>		</div>

<?php 
$exist = ProductInfo::model()->findAll();
if(count($exist) > 0){ ?>
		<div class="pull-left">
			<div class="btn-group dropdown ">
				<button class="form-group btn dropdown-toggle field-list" data-toggle="dropdown">
					<label>Select Fields</label>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu">
					<li><input type="checkbox" class="hidecol" value="SelectAll" id="select_all">Select All</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php 
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'product-info-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText' => false,
	'enableSorting' => false,
	'enablePagination' => false,
	'type' => TbHtml::GRID_TYPE_BORDERED,
	'itemsCssClass' => 'js-dataTable-full',
	'columns'=>array(
	[
	'class'=>'CButtonColumn',
	'template' => '{view}{update}{delete}',
	'header' => 'Action',
		'buttons' => [
			'view' => [
			'title' => 'view',
			'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
			'imageUrl' => false,
			'url' => 'Yii::app()->createUrl("admin/productInfo/view/$data->product_id")',
			'options' => array('class' => 'btn-view', 'title' => 'View'),
			],
			'update' => [
			'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
			'imageUrl' => false,
			'url' => 'Yii::app()->createUrl("admin/productInfo/update/$data->product_id")',
			'options' => array('class' => 'btn-update', 'title' => 'Edit'),
			],
			'delete' => [
			'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-times"></i></button>',
			'imageUrl' => false,
			'url' => 'Yii::app()->createUrl("admin/productInfo/delete/$data->product_id")',
			'options' => array('class' => 'btn-delete', 'title' => 'Delete'),
			],
		],
	],
	'product_id',
	'sku',
	'name',
	'price',
	'description',
	[
		'type' => 'raw',
		'header' => 'Is Active',
		'value' => function ($data) {
			if (strtolower($data->is_active) == 1) {
				return 'Yes';
			} else {
				return 'No';
			}
		}
	],
	/*
	'image',
	*/
		'created_at',
		'modified_at',
		'is_delete',
	),
)); } else {?>
<div class="raw m-b-10">
	<span class="empty">No results found.</span>
</div>
<?php } ?>