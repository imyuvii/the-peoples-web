<?php
/* @var $this OrderCreditMemoController */
/* @var $model OrderCreditMemo */

$this->pageTitle = 'View CreditMemo';


    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'htmlOptions' => array('class' => 'table'),
        'attributes'=>array(
            'credit_memo_id',
            'order_info_id',
            'qty_refunded',
            'amount_to_refund',
            'invoice_number',
            [
                'name' => 'memo_status',
                'value' => function($model) {
                    $fieldId = CylFields::model()->findByAttributes(['field_name' => 'memo_status']);
                    $orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->memo_status]);
                    return $orderStatus->field_label;
                }
            ],
            [
                'name' => 'product_name',
                'value' => function($model) {
                    $productName = ProductInfo::model()->findByAttributes(['product_id' => $model->product_id]);
                    return $productName->name;
                }
            ],
            [
                'name' => 'user',
                'value' => function($model) {
                    $userId = OrderInfo::model()->findByAttributes(['order_info_id' => $model->order_info_id]);
                    $userName = UserInfo::model()->findByAttributes(['user_id' => $userId->user_id]);
                    return $userName->full_name;
                }
            ],
            'created_at',
            'modified_at',
        ),
    )); ?>