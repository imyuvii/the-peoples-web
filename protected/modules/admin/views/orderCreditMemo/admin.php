<?php
/* @var $this OrderCreditMemoController */
/* @var $model OrderCreditMemo */

$this->pageTitle = 'CreditMemo';

?>

<?php
$orderData = OrderCreditMemo::model()->findAll();
if($orderData != null){
	?><div class="pull-left">
	<div class="btn-group dropdown ">
		<button class="form-group btn dropdown-toggle field-list" data-toggle="dropdown">
			<label>Select Fields</label>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu">
			<li><input type="checkbox" class="hidecol" value="SelectAll" id="select_all">Select All</li>
		</ul>
	</div>
</div>
<br/><br/>
<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
		'id'=>'order-credit-memo-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'summaryText' => false,
		'enableSorting' => false,
		'enablePagination' => false,
		'type' => TbHtml::GRID_TYPE_BORDERED,
		'itemsCssClass' => 'js-dataTable-full',
		'columns'=>array(
		[
			'class'=>'CButtonColumn',
			'template' => '{view}{update}{delete}',
			'header' => 'Action',
			'buttons' => [
				'view' => [
				'title' => 'view',
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderCreditMemo/view/$data->credit_memo_id")',
				'options' => array('class' => 'btn-view', 'title' => 'View'),
				],
				'update' => [
				'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderCreditMemo/update/$data->credit_memo_id")',
				'options' => array('class' => 'btn-update', 'title' => 'Edit'),
				],
				'delete' => [
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-times"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/orderCreditMemo/delete/$data->credit_memo_id")',
				'options' => array('class' => 'btn-delete', 'title' => 'Delete'),
			],
			],
		],
			'credit_memo_id',
			//'order_info_id',
			[
				'name' => 'product_id',
				'value' => function($model){
					$productName = ProductInfo::model()->findByAttributes(['product_id' => $model->product_id]);
					return $productName->name;
				}
			],
			'qty_refunded',
			'amount_to_refund',
			//'invoice_number',
			[
				'name' => 'memo_status',
				'value' => function($model) {
					$fieldId = CylFields::model()->findByAttributes(['field_name' => 'memo_status']);
					$orderStatus = CylFieldValues::model()->findByAttributes(['field_id' => $fieldId->field_id, 'predefined_value' => $model->memo_status]);
					return $orderStatus->field_label;
				}
			],
            //'memo_status',
            'created_at',
            'modified_at',
		),
	));
} else{ ?>
	<div class="raw m-b-10">
		<span class="empty">No results found.</span>
	</div>
<?php } ?>