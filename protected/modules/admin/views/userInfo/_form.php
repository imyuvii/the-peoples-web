<?php
/* @var $this UserInfoController */
/* @var $model UserInfo */
/* @var $form CActiveForm */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="block">
            <!--			<div class="block-header">-->
            <!--				<h3 class="block-title">Fields with <span class="required">*</span> are required.</h3>-->
            <!--			</div>-->
            <div class="block-content block-content-narrow">
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
					'id' => 'users-info-form',
					'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
					'enableAjaxValidation' => false,
				    'htmlOptions' => array(
                        'name' => 'UserCreate'
                        )
				));
				?>
                <div class="row">
                    <div
                        class="col-md-4 <?php echo $model->hasErrors('first_name') ? 'has-error' : ''; ?>">
                        <?php echo $form->textFieldControlGroup($model, 'first_name', array('size' => 50, 'maxlength' => 50, 'autofocus' => 'on', 'class' => 'form-control', 'placeholder' => 'First Name')); ?>
                    </div>
                    <div
                        class="col-md-4  <?php echo $model->hasErrors('last_name') ? 'has-error' : ''; ?>">
                        <?php echo $form->textFieldControlGroup($model, 'last_name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Last Name')); ?>
                    </div>
                    <div
                        class="col-md-4 <?php echo $model->hasErrors('full_name') ? 'has-error' : ''; ?>">
                        <?php echo $form->textFieldControlGroup($model, 'full_name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Full Name', 'readOnly'=>true)); ?>
                    </div>
                </div>
                <div class="row">
                    <div
                        class="col-md-6 email-validate <?php echo $model->hasErrors('email') ? 'has-error' : ''; ?>">
                        <?php echo $form->textFieldControlGroup($model, 'email', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                        <p id='email-valid' class='animated fadeInDown  help-block'>Please enter valid Email address.</p>                    </div>
                    <div
                        class="col-md-6 <?php echo $model->hasErrors('password') ? 'has-error' : ''; ?>">
                        <?php echo $form->passwordFieldControlGroup($model, 'password', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <h4>Personal Information</h4>
                    </div>
                </div>

                    <?php echo $form->labelEx($model, 'gender', array('class' => 'control-label')); ?>
                <div class="col-md-12 <?php echo $model->hasErrors('gender') ? 'has-error' : ''; ?>">
                    <div class="radio">
                        <?php echo $form->radioButtonList($model, 'gender', array(
								'1' => 'Male', '2' => 'Female',
							),array(
                            'labelOptions'=>array('style'=>'display:inline'), // add this code
                            'separator'=>'  ')); ?>
                    </div>
                    <span class="help-block"><?= $form->error($model, 'gender'); ?></span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div
                                class="form-group <?php echo $model->hasErrors('sponsor_id') ? 'has-error' : ''; ?>">
                                <div class="controls">
                                    <?php echo $form->label($model, 'sponsor_id', array('class' => 'control-label')); ?>
                                    <span class="required">*</span>
                                    <?php $list = CHtml::listData(UserInfo::model()->findAll(), 'user_id', 'full_name');
									echo $form->dropDownList($model, 'sponsor_id', $list, array('class' => 'form-control',
										'empty' => 'Select Sponsor'));
									?>
                                    <span
                                        class="help-block"><?= $form->error($model, 'sponsor_id'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div
                                class="form-group <?php echo $model->hasErrors('passport_no') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'passport_no', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div
                                class="form-group <?php echo $model->hasErrors('is_enabled') ? 'has-error' : ''; ?>">
                                <?php echo $form->dropDownListControlGroup($model, 'is_enabled', array('1' => 'Yes', '0' => 'No'), array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'is_enabled'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div
                                class="form-group <?php echo $model->hasErrors('date_of_birth') ? 'has-error' : ''; ?>">
                                <?php echo $form->labelEx($model, 'dateOfBirth', array('class' => 'control-label')); ?>
                                <?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									'attribute' => 'date_of_birth',
									//'value'=>$model->dateOfBirth,
									// additional javascript options for the date picker plugin
									'options' => array(
										'showAnim' => '',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
										'dateFormat' => 'yy-mm-dd',
										'maxDate' => date('Y-m-d'),
										'changeYear' => true,           // can change year
										'changeMonth' => true,
										'yearRange' => '1900:' . date('Y'),
									),
									'htmlOptions' => array(
										'class' => 'form-control',
										'readOnly'=>true,
										//'style'=>'height:20px;background-color:green;color:white;',
									),
								));
								?>
                                <span
                                    class="help-block"><?php echo $form->error($model, 'date_of_birth'); ?></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div
                                class="form-group <?php echo $model->hasErrors('is_active') ? 'has-error' : ''; ?>">
                                <?php echo $form->dropDownListControlGroup($model, 'is_active', array('1' => 'Yes', '0' => 'No'), array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'is_active'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 custom_fields">
                                                                    <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('nickname') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'nickname', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'nickname'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('profile_pic') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'profile_pic', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'profile_pic'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('description_bio') ? 'has-error' : ''; ?>">
                                                    <label class="">
                                                        <?php echo $form->labelEx($model, 'description_bio', array('class' => 'control-label')); ?>                                                    </label>
                                                    <?php echo $form->textArea($model,'description_bio', array('class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'description_bio'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('skype') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'skype', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'skype'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_twitter_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_twitter_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_twitter_url'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_fb_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_fb_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_fb_url'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_youtube_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_youtube_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_youtube_url'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_snapchat_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_snapchat_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_snapchat_url'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_pinterest_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_pinterest_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_pinterest_url'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <?php echo $form->labelEx($model, 'terms_conditions', array('class' => 'control-label')); ?>                                                <div class="form-group <?php echo $model->hasErrors('terms_conditions') ? 'has-error' : ''; ?>"  style="padding-left: 20px;">
                                                    <div class="radio-list">
                                                        <?php echo $form->radioButtonList($model, 'terms_conditions', array('1'=>'Accepted',)); ?>                                                    </div>
                                                    <span class="help-block"><?php echo $form->error($model, 'terms_conditions'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_instagram_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_instagram_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_instagram_url'); ?></span>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group <?php echo $model->hasErrors('social_linkedin_url') ? 'has-error' : ''; ?>">
                                                    <?php echo $form->textFieldControlGroup($model, 'social_linkedin_url', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                                    <span class="help-block"><?php echo $form->error($model, 'social_linkedin_url'); ?></span>
                                                </div>
                                            </div>
                                                                </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Address</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('building_num') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'building_num', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('street') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'street', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('region') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'region', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('city') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'city', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('postcode') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'postcode', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('country') ? 'has-error' : ''; ?>">
                                <?php echo $form->dropDownListControlGroup($model, 'country', Yii::app()->ServiceHelper->getNationalOptions(), array('prompt' => 'Select Country', 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('phone') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'phone', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                <p id='phone-valid' class='help-block'>Please enter valid Phone number.</p>                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Business</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('business_name') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'business_name', array('class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('vat_number') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'vat_number', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('busAddress_building_num') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'busAddress_building_num', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('busAddress_street') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'busAddress_street', array('class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('busAddress_region') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'busAddress_region', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('busAddress_city') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'busAddress_city', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('busAddress_postcode') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'busAddress_postcode', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('busAddress_country') ? 'has-error' : ''; ?>">
                                <?php echo $form->dropDownListControlGroup($model, 'busAddress_country', Yii::app()->ServiceHelper->getNationalOptions(), array('prompt' => 'Select Country', 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group <?php echo $model->hasErrors('business_phone') ? 'has-error' : ''; ?>">
                                <?php echo $form->textFieldControlGroup($model, 'business_phone', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                                <p id='business-phone-valid' class='help-block'>Please enter valid Phone number.</p>                            </div>
                        </div>
                    </div>
                <div class="row col-md-12">
                    <div class="form-group">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array(
                        'class' => 'btn btn-primary',
                    )); ?>
                    <?php echo CHtml::link('Cancel', array('userInfo/admin'),
                        array(
                            'class' => 'btn btn-default'
                        )
                    );
                    ?>
                    </div>
                </div>
                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>
        </div>
    </div>
    <!--Hide Show Business Fields Based On Address Type Selection-->
    <script type="text/javascript">
        showBusinessField();
        $("#Addresses_address_type").change(function (e) {
            showBusinessField();
        });
        function showBusinessField() {
            if ($('#Addresses_address_type').val() == 1) {
                $('#business_company').show();
                $('#business_vat').show();
                /* Show Business dynamic fields */
                $('#business_fields').show();
            } else {
                $('#business_company').hide();
                $('#business_vat').hide();
                /* Hide Business dynamic fields */
                $('#business_fields').hide();
            }
        }
    </script>
