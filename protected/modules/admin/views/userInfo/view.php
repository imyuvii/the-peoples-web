<?php
/* @var $this UserInfoController */
/* @var $model UserInfo */

$this->pageTitle = 'View User';
$id = $model->user_id; 
$admin = Yii::app()->params['mandatoryFields']['admin_id'];
?>

<div class="pull-right m-b-10">
	<?php echo CHtml::link('Go to list', array('userInfo/admin'), array('class' => 'btn btn-minw btn-square btn-warning')); ?> 
	<?php echo CHtml::link('Create', array('userInfo/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?> 
	<?php if ($id != $admin){ echo CHtml::link('Update', array('userInfo/update/'.$id), array('class' => 'btn btn-minw btn-square btn-warning')); }?>
	<?php echo CHtml::link('Change Password', array('userInfo/changePassword/'.$id), array('class' => 'btn btn-minw btn-square btn-warning')); ?>
	<?php
		$user = UserInfo::model()->findByPk(['user_id' => $id]);
		$userTable = CylTables::model()->findByAttributes(['table_name' => 'user_info']);
		$activeField = CylFields::model()->findByAttributes(['table_id' => $userTable->table_id, 'field_name' => 'is_active']);
		if($user->is_active == 1){ $userActive = 0; }else { $userActive = 1; }
		$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $activeField->field_id, 'predefined_value' => $userActive]);
		if ($id != $admin) { echo CHtml::link($fieldValue->field_label, array('userInfo/userActive/', 'id' => $id, 'is_active' => $user->is_active), array('class' => 'btn btn-minw btn-square btn-warning'));}?>
</div>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table'),
	'attributes'=>array(
	'user_id',
	'full_name',
	'first_name',
	'last_name',
	'email',
	'password',
	'date_of_birth',
	[
		'name' => 'gender',
		'value' => function($model){
			$tableName = CylTables::model()->findByAttributes(['table_name' => 'user_info']);
			$fieldName = CylFields::model()->findByAttributes(['field_name' => 'gender', 'table_id' => $tableName->table_id]);
			$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $fieldName->field_id, 'predefined_value' => $model->gender ]);
			return $fieldValue->field_label;
		}
	],
	'passport_no',
	[
		'name' => 'sponsor_id',
		'value' => function($model){
			$userName = UserInfo::model()->findByAttributes(['user_id' => $model->sponsor_id]);
			if(!empty($userName)) {
				return $userName->full_name;
			}
		}
	],
	[
		'name' => 'is_enabled',
		'value' => function($model){
			$tableName = CylTables::model()->findByAttributes(['table_name' => 'user_info']);
			$fieldName = CylFields::model()->findByAttributes(['field_name' => 'is_enabled', 'table_id' => $tableName->table_id]);
			$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $fieldName->field_id, 'predefined_value' => $model->is_enabled ]);
			return $fieldValue->field_label;
		}
	],
	[
		'name' => 'is_active',
		'value' => function($model){
			$tableName = CylTables::model()->findByAttributes(['table_name' => 'user_info']);
			$fieldName = CylFields::model()->findByAttributes(['field_name' => 'is_active', 'table_id' => $tableName->table_id]);
			$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $fieldName->field_id, 'predefined_value' => $model->is_active ]);
			return $fieldValue->field_label;
		}
	],
		'created_at',
		'modified_at',
		'business_name',
		'vat_number',
		'busAddress_building_num',
		'busAddress_street',
		'busAddress_region',
		'busAddress_city',
		'busAddress_postcode',
		'busAddress_country',
		'business_phone',
		'building_num',
		'street',
		'region',
		'city',
		'postcode',
		'country',
		'phone',
		'is_delete',
		'nickname',
		'profile_pic',
		'description_bio',
		'skype',
		'social_twitter_url',
		'social_fb_url',
		'social_youtube_url',
		'social_snapchat_url',
		'social_pinterest_url',
		'terms_conditions',
		'social_instagram_url',
		'social_linkedin_url',
	),
)); ?>
