<?php
/* @var $this UserInfoController */
/* @var $model UserInfo */

$this->pageTitle = 'Users';
?>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right m-b-10">
			<?php echo CHtml::link('Create', array('UserInfo/create'), array('class' => 'btn btn-minw btn-square btn-warning')); ?>		</div>

<?php 
$exist = UserInfo::model()->findAll();
if(count($exist) > 0){ ?>

		<div class="pull-left">
			<div class="btn-group dropdown ">
				<button class="form-control btn dropdown-toggle field-list" data-toggle="dropdown">
					<label>Select Fields</label>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" id="userList" role="menu" aria-labelledby="dropdownMenu">
					<li><input type="checkbox" class="hidecol" value="SelectAll" id="select_all">Select All</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php 
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-info-grid',
	'dataProvider'=>$model->search(),
	// 'enableSorting' => false,
	//'enablePagination' => true,
	'type' => TbHtml::GRID_TYPE_BORDERED,
	'filter'=> $model,
	'summaryText' => false,
	'itemsCssClass' => 'js-dataTable-full',
	'columns'=>array(
		array(
			'header' => 'Action',
			'class' => 'CButtonColumn',
			'template' => '{view}{update}',
			'buttons' => [
				'view' => [
				'title' => 'view',
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-eye"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/userInfo/view/$data->user_id")',
				'options' => array('class' => 'btn-view', 'title' => 'View'),
				],
				'update' => [
				'label' => '<button class="btn btn-xs btn-default" type="button" ><i class="fa fa-pencil"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/userInfo/update/$data->user_id")',
				'options' => array('class' => 'btn-update', 'title' => 'Edit'),
				'visible' => 'Yii::app()->params[\'mandatoryFields\'][\'admin_id\'] != $data->user_id',
				],
				'delete' => [
				'label' => '<button class="btn btn-xs btn-default" type="button"><i class="fa fa-times"></i></button>',
				'imageUrl' => false,
				'url' => 'Yii::app()->createUrl("admin/userInfo/delete/$data->user_id")',
				'options' => array('class' => 'btn-delete', 'title' => 'Delete'),
				'visible' => 'Yii::app()->params[\'mandatoryFields\'][\'admin_id\'] != $data->user_id',
				],
			],
		),
		'user_id',
		'full_name',
		'first_name',
		'last_name',
		'email',
		'password',
		'date_of_birth',
		[
			'name' => 'gender',
			'value' => function($model){
			$fields = CylFields::model()->findByAttributes(['field_name' => 'gender', 'table_id' => 1, ]);
			$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $fields->field_id, 'predefined_value' => $model->gender]);
			return $fieldValue->field_label;
		}
		],
		'passport_no',
		[
			'name' => 'is_enabled',
			'value' => function($model){
			$fields = CylFields::model()->findByAttributes(['field_name' => 'is_enabled', 'table_id' => 1, ]);
			$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $fields->field_id, 'predefined_value' => $model->is_enabled]);
			return $fieldValue->field_label;
		}
		],
		[
			'name' => 'is_active',
			'value' => function($model){
			$fields = CylFields::model()->findByAttributes(['field_name' => 'is_active', 'table_id' => 1, ]);
			$fieldValue = CylFieldValues::model()->findByAttributes(['field_id' => $fields->field_id, 'predefined_value' => $model->is_active]);
			return $fieldValue->field_label;
		}
		],
		'business_name',
		'vat_number',
		'busAddress_building_num',
		'busAddress_street',
		'busAddress_region',
		'busAddress_city',
		'busAddress_postcode',
		'busAddress_country',
		'business_phone',
		'building_num',
		'street',
		'region',
		'city',
		'postcode',
		'country',
		'phone',
		'is_delete',
			'nickname',
		'profile_pic',
		'description_bio',
		'skype',
		'social_twitter_url',
		'social_fb_url',
		'social_youtube_url',
		'social_snapchat_url',
		'social_pinterest_url',
		'terms_conditions',
		'social_instagram_url',
		'social_linkedin_url',
		array(
			'name' => 'Sponsor',
			'value' => function ($model) {
				if ($model->sponsor_id > 0) {
					$sponsor = UserInfo::model()->findByAttributes(array('user_id' => $model->sponsor_id));
					if($sponsor){
						return CHtml::encode($sponsor->full_name);
					}else{
						return CHtml::encode("Sponser Deleted");
					}
				} else {
					return 'NA';
				}
			},
			'filter' => CHtml::activeDropDownList($model, 'sponsor_id', CHtml::listData(
				UserInfo::model()->findAll(array('order' => 'full_name')
				), 'user_id', 'full_name'), array('empty' => 'Select User')),
		),
		array(
			'name' => 'created_at',
			'header' => 'Registration Date',
			'value' => 'Yii::app()->dateFormatter->format("yyyy-MM-dd",strtotime($data->created_at))',
		),
        'created_at',
        'modified_at',
	),
)); } else {?>
	<div class="raw m-b-10">
		<span class="empty">No results found.</span>
	</div>
<?php } ?>
