<?php
/**
 * Created by PhpStorm.
 * User: Sagar
 * Date: 2/10/2017
 * Time: 11:12 AM
 */

?>
<?php $this->pageTitle = 'Report List'; ?>
<div class="pull-right m-b-10">
    <?php echo CHtml::link('Go to list', array('Report/userReport'), array('class' => 'btn btn-minw btn-square btn-warning')); ?>
</div>
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-lg-12">
        <div class="block">
            <iframe style="overflow-x: hidden;"  src="<?php echo Yii::app()->createUrl("report_"); ?>" width="100%" id="userReportFrame"  height="2000px" scrolling="no"  frameborder="0" ></iframe>
        </div>
    </div>
</div>

<script>
    /*function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }*/


</script>