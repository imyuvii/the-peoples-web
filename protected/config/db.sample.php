<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 20/03/17
 * Time: 12:12 PM
 */
return array(
    'connectionString' => 'mysql:host=localhost;dbname=your_db_name',
    'emulatePrepare' => true,
    'username' => 'username',
    'password' => 'password',
    'charset' => 'utf8',
    // 'tablePrefix' => 'tbl_',
);