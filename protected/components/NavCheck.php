<?php

/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 5/12/16
 * Time: 1:00 PM
 */
class NavCheck
{
    public static function navigationCheck()
    {
        $userChk = is_file(Yii::getPathOfAlias('application.modules.admin.views.userInfo.admin').'.php')?'yes':'no';
        $proChk = is_file(Yii::getPathOfAlias('application.modules.admin.views.productInfo.admin').'.php')?'yes':'no';
        $orderChk = is_file(Yii::getPathOfAlias('application.modules.admin.views.orderInfo.admin').'.php')?'yes':'no';
        $walletChk = is_file(Yii::getPathOfAlias('application.modules.admin.views.wallet.admin').'.php')?'yes':'no';
        $reportChk = is_file(Yii::getPathOfAlias('application.modules.admin.views.reportInfo.admin').'.php')?'yes':'no';
        $otherChk = is_file(Yii::getPathOfAlias('application.modules.admin.views.otherInfo.admin').'.php')?'yes':'no';

        if ($userChk == "yes"){
            $usernav = "";
        }else{
            $usernav = "user-nav";
        }
        if ($proChk == "yes"){
            $productnav = "";
        }else{
            $productnav = "product-nav";
        }

        if ($orderChk == "yes"){
            $ordernav = "";
        }else{
            $ordernav = "order-nav";
        }

        if ($walletChk == "yes"){
            $walletnav = "";
        }else{
            $walletnav = "wallet-nav";
        }

        if ($reportChk == "yes"){
            $reportnav = "";
        }else{
            $reportnav = "report-nav";
        }

        if ($otherChk == "yes"){
            $othernav = "";
        }else{
            $othernav = "other-nav";
        }

        $navigation = [
            'user' => $usernav,
            'product' => $productnav,
            'order' => $ordernav,
            'wallet' => $walletnav,
            'report' => $reportnav,
            'other' => $othernav,
        ];

        return $navigation;
    }
}