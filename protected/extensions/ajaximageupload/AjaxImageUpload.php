<?php

/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 09/03/17
 * Time: 5:13 PM
 */
class AjaxImageUpload extends CWidget
{
    public function init() {
        return parent::init();
    }

    public function run() {
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($assets . '/imgareaselect.css');
        $cs->registerScriptFile($assets . '/jquery.imgareaselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($assets . '/jquery.form.js',CClientScript::POS_END);
        $cs->registerScriptFile($assets . '/custom.js',CClientScript::POS_END);
        $this->renderHtml($assets);
    }

    protected function renderHtml($assets) {
        ?>

        <?php
    }
}