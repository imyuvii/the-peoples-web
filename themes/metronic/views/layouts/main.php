<?php /* @var $this Controller */ ?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<?php // Yii::app()->bootstrap->register(); ?>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

	<!-- BEGIN PAGE STYLES -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>
	<!-- graph theme -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/stats-graph/nv.d3.css" rel="stylesheet" type="text/css"/>

	<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/favicon.ico"/>
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
<div class="page-header navbar-fixed-top header">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner clearfix">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?php echo Yii::app()->request->baseUrl; ?>">
				<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler">
				<i class="fa fa-bars"></i>
			</div>
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
		</div>
		<!-- END LOGO -->

		<div class="nav notify-row" id="top_menu">
			<!--  notification start -->
			<ul class="nav top-menu">
				<!-- inbox dropdown start-->
				<li id="header_inbox_bar" class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void()">
						<i class="fa fa-envelope-o"></i>
						<span class="badge bg-important">4</span>
					</a>
					<ul class="dropdown-menu extended inbox">
						<li>
							<p class="red">You have 4 Mails</p>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></span>
								<span class="subject">
                                        <span class="from">Jonathan Smith</span>
                                        <span class="time">Just now</span>
                                        </span>
								<span class="message">
                                            Hello, this is an example msg.
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-3.jpg"></span>
								<span class="subject">
                                        <span class="from">Jane Doe</span>
                                        <span class="time">2 min ago</span>
                                        </span>
								<span class="message">
                                            Nice admin template
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"></span>
								<span class="subject">
                                        <span class="from">Tasi sam</span>
                                        <span class="time">2 days ago</span>
                                        </span>
								<span class="message">
                                            This is an example msg.
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></span>
								<span class="subject">
                                        <span class="from">Mr. Perfect</span>
                                        <span class="time">2 hour ago</span>
                                        </span>
								<span class="message">
                                            Hi there, its a test
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">See all messages</a>
						</li>
					</ul>
				</li>
				<!-- inbox dropdown end -->
				<!-- notification dropdown start-->
				<li id="header_notification_bar" class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void()">

						<i class="fa fa-bell-o"></i>
						<span class="badge bg-warning">3</span>
					</a>
					<ul class="dropdown-menu extended notification">
						<li>
							<p>Notifications</p>
						</li>
						<li>
							<div class="alert alert-info clearfix">
								<span class="alert-icon"><i class="fa fa-bolt"></i></span>
								<div class="noti-info">
									<a href="javascript:void()"> Server #1 overloaded.</a>
								</div>
							</div>
						</li>
						<li>
							<div class="alert alert-danger clearfix">
								<span class="alert-icon"><i class="fa fa-bolt"></i></span>
								<div class="noti-info">
									<a href="javascript:void()"> Server #2 overloaded.</a>
								</div>
							</div>
						</li>
						<li>
							<div class="alert alert-success clearfix">
								<span class="alert-icon"><i class="fa fa-bolt"></i></span>
								<div class="noti-info">
									<a href="javascript:void()"> Server #3 overloaded.</a>
								</div>
							</div>
						</li>

					</ul>
				</li>
				<!-- notification dropdown end -->
				<!-- inbox dropdown start-->
				<li id="header_inbox_bar" class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void()">
						<i class="fa fa-calendar-o"></i>
						<span class="badge bg-success">8</span>
					</a>
					<ul class="dropdown-menu extended inbox">
						<li>
							<p class="red">You have 4 Mails</p>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></span>
								<span class="subject">
                                        <span class="from">Jonathan Smith</span>
                                        <span class="time">Just now</span>
                                        </span>
								<span class="message">
                                            Hello, this is an example msg.
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-3.jpg"></span>
								<span class="subject">
                                        <span class="from">Jane Doe</span>
                                        <span class="time">2 min ago</span>
                                        </span>
								<span class="message">
                                            Nice admin template
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"></span>
								<span class="subject">
                                        <span class="from">Tasi sam</span>
                                        <span class="time">2 days ago</span>
                                        </span>
								<span class="message">
                                            This is an example msg.
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">
								<span class="photo"><img alt="avatar" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></span>
								<span class="subject">
                                        <span class="from">Mr. Perfect</span>
                                        <span class="time">2 hour ago</span>
                                        </span>
								<span class="message">
                                            Hi there, its a test
                                        </span>
							</a>
						</li>
						<li>
							<a href="javascript:void()">See all messages</a>
						</li>
					</ul>
				</li>

				<li>
					<a href="wallet.html">
						<i class="icon-wallet"></i>
					</a>
				</li>

				<li>
					<a href="javascript:void()">
						<i class="icon-blog"></i>
					</a>
				</li>
			</ul>
			<!--  notification end -->
		</div>

		<ul class="nav pull-right top-menu">
			<li>
				<input class="form-control search" placeholder=" Search" type="text">
			</li>
			<li class="dropdown language">
				<a href="javascript:void()" class="dropdown-toggle" data-toggle="dropdown"  data-close-others="true">
					<img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/flags/us.png">
					<span class="username">US</span>
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void()"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/flags/nl.png"> NL</a></li>
					<li><a href="javascript:void()"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/flags/de.png"> DE</a></li>
					<li><a href="javascript:void()"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/flags/fr.png"> FR</a></li>
					<li><a href="javascript:void()"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/flags/es.png"> SP</a></li>
				</ul>
			</li>
			<li class="dropdown profile">
				<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void()">
					<img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/avatar1_small.jpg">
					<span class="username"><?php echo (!Yii::app()->user->isGuest)?Yii::app()->user->name:''; ?></span>
					<b class="caret"></b>
				</a>
				<?php
				$this->widget('zii.widgets.CMenu',array(
					'encodeLabel' => false,
					'items'=>array(
						array('label'=>'<i class=" fa fa-suitcase"></i> Personal Settings', 'url'=>array('/user/personal')),
						array('label'=>'<i class=" fa fa-cog"></i> Account Settings', 'url'=>array('/user/account')),
						array('label'=>'<i class=" fa fa-lock"></i> Privacy Settings', 'url'=>array('/user/privacy')),
						array('label'=>'<i class="fa fa-sign-in"></i> Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'<i class="fa fa-sign-out"></i> Logout', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
					'htmlOptions' => array('class' => 'dropdown-menu extended logout')
				));
				?>
			</li>
		</ul>
	</div>
</div>
<div class="clearfix"></div>

<div class="page-container">

	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<?php $this->widget('zii.widgets.CMenu',array(
				'encodeLabel' => false,
				'activateParents' => true,
				'items'=>array(
					array('label'=>'<i class="fa fa-dashboard"></i><span>Dashboard</span>', 'url'=>array('/')),
					array('label'=>'<i class="icon-account-grey"></i><span>Your Account</span><span class="arrow"></span>', 'url'=>'javascript:;','items' => array(
						array('label'=>'<span>Overview</span>', 'url'=>array('account/index')),
						array('label'=>'<span>Direct Sales Bonus</span>', 'url'=>array('/account/directSales')),
						array('label'=>'<span>Fast Track Bonus</span>', 'url'=>array('/account/fastrack')),
						array('label'=>'<span>Ripple Bonus</span>', 'url'=>array('/account/ripple')),
						array('label'=>'<span>Ripple Matching Bonus</span>', 'url'=>array('/account/rippleMatching')),
						array('label'=>'<span>Ripple Royalty Bonus</span>', 'url'=>array('/account/rippleRoyalty')),
						array('label'=>'<span>Binary Bonus</span>', 'url'=>array('/account/binary')),
					),'htmlOptions'=>array('class'=>'test')),
					array('label'=>'<i class="fa fa-sitemap"></i><span>Genealogy</span><span class="arrow"></span>', 'url'=>array('/'),'items' => array(
						array('label'=>'<span>Overview</span>', 'url'=>array('/')),
						array('label'=>'<span>Direct Sales Bonus</span>', 'url'=>array('/')),
						array('label'=>'<span>Fast Track Bonus</span>', 'url'=>array('/')),
						array('label'=>'<span>Ripple Bonus</span>', 'url'=>array('/')),
						array('label'=>'<span>Ripple Matching Bonus</span>', 'url'=>array('/')),
						array('label'=>'<span>Ripple Royalty Bonus</span>', 'url'=>array('/')),
						array('label'=>'<span>Binary Bonus</span>', 'url'=>array('/')),
					)),
//					array('label'=>'<i class="fa fa-rss"></i><span>Activity Feed</span>', 'url'=>array('/ActivityFeed/index')),
//					array('label'=>'<i class="fa fa-envelope"></i><span>Email</span><span class="arrow"></span>', 'url'=>array('/'),'items' => array(
//						array('label'=>'<span>Inbox</span>', 'url'=>array('Email/index')),
//						array('label'=>'<span>Compose Mail</span>', 'url'=>array('Email/compose')),
//						array('label'=>'<span>Sent Mail</span>', 'url'=>array('Email/sent')),
//						array('label'=>'<span>Trash</span>', 'url'=>array('Email/trash')),
//					)),
					array('label'=>'<i class="icon-wallet2"></i><span>Wallet</span>', 'url'=>array('/Wallet/index')),
				),
				'htmlOptions' => array(
					'class' => 'page-sidebar-menu',
					'data-keep-expanded' => 'false',
					'data-auto-scroll' => 'true',
					'data-slide-speed' => '200'
				),
				'submenuHtmlOptions' => array(
					'class' => 'sub-menu'
				)
			)); ?>
		</div>
	</div>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h1 class="page-title"><?php echo $this->pageTitle; ?></h1>
			<?php if(isset($this->breadcrumbs)):?>
				<div class="page-bar">
					<?php $imgHtml = '<img src="'. Yii::app()->theme->baseUrl . '/assets/admin/layout/img/small-logo.jpg">'; ?>
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
						'homeLink'=> '<li class="sub">'.CHtml::link($imgHtml, array('/home/index')).'</li>',
						'links'=>$this->breadcrumbs,
						'htmlOptions' => array(
							'class' => 'page-breadcrumb'
						),
						'tagName'		=>'ul',
						'separator'		=>'', // no separator
						'activeLinkTemplate'	=>'<li class="sub"><a href="{url}">{label}</a></li>',
						'inactiveLinkTemplate'	=>'<li class="sub"><a>{label}</a></li>', // in-active link template
					)); ?>
				</div>
			<?php endif?>
			<?php if($this->uniqueid == 'account'): ?>
				<section class="tabbable-line tabs-overview tabbable-full-width">
					<?php $acImgPath = Yii::app()->theme->baseUrl.'/assets/admin/pages/img/'; ?>
					<?php $this->widget('zii.widgets.CMenu',array(
						'encodeLabel' => false,
						'items'=>array(
							array('label'=> CHtml::image($acImgPath.'icon-overview.png','Overview',array('class' => 'image-responsive')), 'url'=>array('/account/index')),
							array('label'=> CHtml::image($acImgPath.'icon-direct-sales-bonus.png','Direct Sales Bonus',array('class' => 'image-responsive')), 'url'=>array('/account/directSales')),
							array('label'=> CHtml::image($acImgPath.'icon-fast-track-bonus.png','Fast Track Bonus',array('class' => 'image-responsive')), 'url'=>array('/account/fastrack')),
							array('label'=> CHtml::image($acImgPath.'icon-royalty-bonus-ripple.png','Ripple Bonus',array('class' => 'image-responsive')), 'url'=>array('/account/ripple')),
							array('label'=> CHtml::image($acImgPath.'icon-ripple-matching-bonus.png','Ripple Matching Bonus',array('class' => 'image-responsive')), 'url'=>array('/account/rippleMatching')),
							array('label'=> CHtml::image($acImgPath.'icon-ripple-royalty-bonus.png','Ripply Royalty Bonus',array('class' => 'image-responsive')), 'url'=>array('/account/rippleRoyalty')),
							array('label'=> CHtml::image($acImgPath.'icon-binary.png','Binary Bonus',array('class' => 'image-responsive')), 'url'=>array('/account/binary')),
						),
						'htmlOptions' => array(
							'class' => 'nav nav-tabs-block bg-white'
						)
					)); ?>
					<!-- end tabs -->

					<div class="tab-content no-bg">
						<?php echo $content; ?>
					</div>
				</section>
				<?php  elseif($this->uniqueid == 'email'): ?>
					<div class="row">
						<div class="col-md-3 col-sm-4">
							<section class="panel">
								<div class="panel-body">
									<a href="compose"  class="btn btn-compose">
										Compose Mail
									</a>
                                    <?php $this->widget('zii.widgets.CMenu',array(
                                        'encodeLabel' => false,
                                        'items'=>array(
                                            array('label'=>'<i class="fa fa-inbox"></i> Inbox  <span class="label label-danger pull-right inbox-notification">9</span>', 'url'=>array('/Email/index')),
                                            array('label'=>'<i class="fa fa-envelope-o"></i> Sent Mail ', 'url'=>array('/Email/sent')),
                                            array('label'=>'<i class="fa fa-certificate"></i> Important ', 'url'=>array('/Email/important')),
                                            array('label'=>'<i class="fa fa-file-text-o"></i> Drafts  <span class="label label-info pull-right inbox-notification">123</span>', 'url'=>array('/Email/draft')),
                                            array('label'=>'<i class="fa fa-trash-o"></i> Trash ', 'url'=>array('/Email/trash')),
                                            array('label'=>'<i class="fa fa-ban"></i> Spam ', 'url'=>array('/Email/spam')),
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'nav nav-pills nav-stacked mail-nav'
                                        )
                                    )); ?>
								</div>
							</section>

							<div class="portlet box light">
								<div class="portlet-title">
									<h4 class="uppercase">Online Friends</h4>
									<div class="tools">
										<a class="reload" href="javascript:void()" data-original-title="" title=""></a>
									</div>
								</div>

								<div class="portlet-body">
									<ul class="user-list">
										<li>
											<a class="user-img" href="javascript:void()"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"><i class="buzy"></i></a>
											<div class="activty-cont">
												<h5><a href="javascript:void()">Anna Opichia</a></h5>
												<small class="grey">Buzy</small>
											</div>
										</li>

										<li>
											<a class="user-img" href="javascript:void()"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"><i class="available"></i></a>
											<div class="activty-cont">
												<h5><a href="javascript:void()">Anna Opichia</a></h5>
												<small class="grey">Available</small>
											</div>
										</li>

										<li>
											<a class="user-img" href="javascript:void()"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-3.jpg"><i class="available"></i></a>
											<div class="activty-cont">
												<h5><a href="javascript:void()">Boby Socks</a></h5>
												<small class="grey">Available</small>
											</div>
										</li>

										<li>
											<a class="user-img" href="javascript:void()"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"><i class="idle"></i></a>
											<div class="activty-cont">
												<h5><a href="javascript:void()">Rogger Flopple</a></h5>
												<small class="grey">Idle</small>
											</div>
										</li>

										<li>
											<a class="user-img" href="javascript:void()"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"><i class="offline"></i></a>
											<div class="activty-cont">
												<h5><a href="javascript:void()">Deel McApple</a></h5>
												<small class="grey">Offline</small>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- left mail action -->
						<?php echo $content; ?>
					</div>
					<script>
						$(document).ready(function() {
							$('#selectall').click(function(event) {  //on click
								if(this.checked) { // check select status
									$('.css-checkbox').each(function() { //loop through each checkbox
										this.checked = true;  //select all checkboxes with class "checkbox1"
									});
								}else{
									$('.css-checkbox').each(function() { //loop through each checkbox
										this.checked = false; //deselect all checkboxes with class "checkbox1"
									});
								}
							});

						});
					</script>
			<?php else: ?>
				<?php echo $content; ?>
			<?php endif; ?>
		</div>
	</div>


</div>
<div class="page-footer">
	<div class="page-footer-inner">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-4">
					<h1 class="email-support"> Email Support</h1>
					<span><strong>General inquiries:</strong> <a href="mailto:info@iriscall.com" class="white">info@iriscall.com</a></span><br>
					<span><strong>Technical inquiries or support:</strong> <a href="mailto:info@iriscall.com" class="white">support@iriscall.com</a></span><br>
					<span><strong>Financial inquiries:</strong> <a href="mailto:info@iriscall.com" class="white">accounting@iriscall.com</a></span>
				</div>

				<div class="col-lg-5 col-md-4">
					<h1 class="live-support">Live Support</h1>
					<span><strong>Phone:</strong> +32 493 445 853</span><br>
					<span><strong>VAIOX:</strong> Fan KickStart Support</span><br>
					<span><strong>Skype:</strong> Fan KickStart Support</span>
				</div>

				<div class="col-lg-2 col-md-4">
					<h1 class="follow-us"> Follow Us</h1>
					<a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-white-fb.png"/></a>
					<a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-white-twit.png"/></a>
				</div>
			</div>
		</div>
	</div>
	<div class="copyrights">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="about-us.html">About Us</a> | <a href="terms-of-service.html">Terms And Conditions</a><br>
					&copy; 2016- IrisCall  - All Rights Reserved - IrisCall is a trade name of <span class="blue">Force International CVBA.</span>
				</div>
			</div>
		</div>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<!-- date range js -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/scripts/index.js" type="text/javascript"></script>

<!-- multibar -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/stats-graph/d3.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/stats-graph/nv.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/stats-graph/stream_layers.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/stats-graph/stats-graph.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/scripts/timeline.js" type="text/javascript"></script>
<script>
	jQuery(document).ready(function() {
		Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		QuickSidebar.init(); // init quick sidebar
		Demo.init(); // init demo features
		Index.init();
		Index.initDashboardDaterange();
		Index.initCalendar(); // init index page's custom scripts
	});
</script>
</body>
</html>
