<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Wallet";
$this->breadcrumbs=array(
    'Wallet'
);
?>

<div class="row">
    <div class="col-md-7">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">CHECKING ACCOUNT</h4>
            </div>

            <div class="portlet-body">
                <div class="chking-account-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-direct-sales-bonus.png" width="36"></td>
                            <td width="45%"><h5 class="green no-margin">Direct Sales Bonus</h5></td>
                            <td>Direct to account</td>
                            <td align="right">&euro; 150,00</td>
                        </tr>
                    </table>
                </div>

                <div class="chking-account-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-fast-track-bonus.png" width="36"></td>
                            <td width="45%"><h5 class="green no-margin">Fast Track Bonus</h5></td>
                            <td>Direct to account</td>
                            <td align="right">&euro; 550,00</td>
                        </tr>
                    </table>
                </div>

                <div class="chking-account-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-royalty-bonus-ripple.png" width="36"></td>
                            <td width="45%"><h5 class="green no-margin">Ripple Bonus</h5></td>
                            <td>May 01, 2015</td>
                            <td align="right">€ 78,04</td>
                        </tr>
                    </table>
                </div>

                <div class="chking-account-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-ripple-matching-bonus.png" width="36"></td>
                            <td width="45%"><h5 class="green no-margin">Ripple Matching Bonus</h5></td>
                            <td>May 01, 2015</td>
                            <td align="right">€ 78,04</td>
                        </tr>
                    </table>
                </div>

                <div class="chking-account-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-ripple-royalty-bonus.png" width="36"></td>
                            <td width="45%"><h5 class="green no-margin">Ripple Infinity Bonus</h5></td>
                            <td>May 01, 2015</td>
                            <td align="right">€ 78,04</td>
                        </tr>
                    </table>
                </div>

                <div class="chking-account-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-binary.png" width="36"></td>
                            <td width="45%"><h5 class="green no-margin">Binary Bonus</h5></td>
                            <td>April 20, 2015</td>
                            <td align="right">€ 460,00</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end checking account -->

    <div class="col-md-5">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Current Account</h4>
            </div>

            <div class="portlet-body">
                <div class="chking-account-row margin-bottom-10">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50%">Available Account</td>
                            <td align="right">€ 1500,00</td>
                        </tr>
                    </table>
                </div>
                <a href="#myModal" class="btn btn-success margin-bottom-10" data-toggle="modal">Place Withdrawal Request</a>
                <div class="clearfix"></div>
                <small>Note: Your pay-out details can be changed in your profile.<br>
                    <a href="user-account-setting.html" class="green">Click here</a> to go directly.</small>
            </div>
        </div>
        <!-- end CA  -->

        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Credit Account, IrisCoins</h4>
            </div>

            <div class="portlet-body">
                <div class="chking-account-row margin-bottom-10">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50%">Available Account</td>
                            <td align="right">€ 1500,00</td>
                        </tr>
                    </table>
                </div>
                <a href="#myModal" class="btn btn-success" data-toggle="modal">Transfer to Member</a>
            </div>
        </div>
    </div>

</div>
<!-- end checking account -->

<!-- dialog box -->
<div id="myModal" class="modal fade col-md-offset-3 col-md-7">
    <div class="portlet box light">
        <div class="portlet-title">
            <h4 class="uppercase">Withdraw funds from your account</h4>
            <div class="tools"><a href="javascript:void()" data-dismiss="modal" aria-hidden="true" class="grey"><i class="fa fa-close"></i></a></div>
        </div>

        <div class="portlet-body">
            <div class="form-horizontal">
                <div class="row">
                    <div class="col-lg-6 margin-bottom-10">
                        <div class="form-group">
                            <label class="col-xs-4 control-label col-lg-4">Amount:</label>
                            <div class="col-xs-7">
                                <input type="text" id="inputSuccess" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 margin-bottom-10">
                        <div class="form-group">
                            <label class="col-xs-4 control-label col-lg-4">In currency:</label>
                            <div class="col-xs-7">
                                <input type="text" id="inputSuccess" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 margin-bottom-10">
                        <h5 class="bold">Currently our preferred payment partner for the pay-our option is Neteller.</h5>
                        <a href="javascript:void()" class="btn btn-success margin-bottom-15">Create new Neteller Account</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="col-xs-8 control-label col-lg-8">Your Neteller Account ID*</label>
                            <div class="col-xs-4">
                                <input type="text" id="inputSuccess" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="col-xs-8 control-label col-lg-8">Your Neteller Account email</label>
                            <div class="col-xs-4">
                                <input type="text" id="inputSuccess" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 inbox-small-cells">
                        <input type="checkbox" class="css-checkbox" id="accept" name="accept">
                        <label class="css-label" for="accept"> I accept the withdrawal section in the Terms & Conditions</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void()" class="btn btn-success margin-bottom-10">Confirm Withdrawal</a>
                        <a href="javascript:void()" class="btn btn-success margin-bottom-10">Cancel</a>

                        <p>Please keep in mind that during the pre-launch period of Iriscall we are processing payouts at a
                            maximum lever of 500 per month. No further action is required. Thank you.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
