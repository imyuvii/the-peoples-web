<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Activity Feed";
$this->breadcrumbs=array(
    'Activity Feed'
);
?>

<div class="row">
    <div class="col-lg-12">
        <div class="timeline">
            <!-- TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-days-badge">
                    <div class="btn-block btn bg-green">Today</div>
                </div>
            </div>
            <!-- END TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-badge">
                    <div class="timeline-badge-user">
                        <a href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user1.png"></a>
                    </div>
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"></div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <strong><a href="profile.html" class="black">Anna Opichia</a></strong> become gold member
                            <span class="timeline-body-time font-grey-cascade">29 mins ago</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-badge">
                    <div class="timeline-badge-user">
                        <a href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user2.png"></a>
                    </div>
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"></div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <strong><a href="profile.html" class="black">Bobby Socks</a></strong> registered for business member group
                            <span class="timeline-body-time font-grey-cascade">30 mins ago</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-badge">
                    <div class="timeline-badge-user">
                        <a href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user2.png"></a>
                    </div>
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"></div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <strong><a href="user-profile.html" class="black">Bobby Socks</a></strong>
                            <div class="btn-group">
                                <a data-close-others="true" class="friend-list" data-hover="dropdown" data-toggle="dropdown" type="button" aria-expanded="false">5 people to his friend list                                                           		</a>
                                <ul role="menu" class="dropdown-menu users" style="min-width:250px;">
                                    <li>
                                        <a class="user-img" href="user-profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user3.png"></a>
                                        <div class="activty-cont">
                                            <strong><a href="user-profile.html" class="black">Megan Dean</a></strong>
                                            <span class="jobpost grey">Copywriter</span>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="user-img" href="user-profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user4.png"></a>
                                        <div class="activty-cont">
                                            <strong><a href="user-profile.html" class="black">Roger Har</a></strong>
                                            <span class="jobpost grey">Marketing Executive</span>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="user-img" href="user-profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user5.png"></a>
                                        <div class="activty-cont">
                                            <strong><a href="user-profile.html" class="black">Sara Holland</a></strong>
                                            <span class="jobpost grey">Team Leader - Marketing</span>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="user-img" href="user-profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user6.png"></a>
                                        <div class="activty-cont">
                                            <strong><a href="user-profile.html" class="black">Walter Fox</a></strong>
                                            <span class="jobpost grey">Vice President - Software.org</span>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="user-img" href="user-profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user7.png"></a>
                                        <div class="activty-cont">
                                            <strong><a href="user-profile.html" class="black">Craig Stone</a></strong>
                                            <span class="jobpost grey">UI Designer</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <span class="timeline-body-time font-grey-cascade">30 mins ago</span>

                        </div>
                    </div>
                </div>
            </div>
            <!-- TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-badge">
                    <div class="timeline-badge-user">
                        <a href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user8.png"></a>
                    </div>
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"></div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <strong><a href="profile.html" class="black">Roger Flopple</a></strong> added a new task
                            <span class="timeline-body-time font-grey-cascade">30 mins ago</span>
                            <p class="margin-top-10">Aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing vehicula tortor dapibus adipiscing. Nullam quis quam massa. Donec vitae metus tortor. Vestibulum vel diam orci. Etiam sollicitudin venenatis justo ut posuere. Etiam facilisis
                                est ut ligula ornare accumsan. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-days-badge">
                    <div class="btn-block btn bg-grey-mint">Yesterday</div>
                </div>
            </div>
            <!-- TIMELINE ITEM -->
            <div class="timeline-item">
                <div class="timeline-badge">
                    <div class="timeline-badge-user">
                        <a href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/activity-user9.png"></a>
                    </div>
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"></div>
                    <div class="timeline-body-head">
                        <div class="timeline-body-head-caption">
                            <strong><a href="profile.html" class="black">Deel McAppe</a></strong> become gold member
                            <span class="timeline-body-time font-grey-cascade">29 hours ago</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TIMELINE ITEM -->
        </div>
    </div>
</div>
<!-- end tabs overview -->
