<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */
/* @var $this SiteController */

$this->pageTitle="Personal Settings";
$this->breadcrumbs=array(
    'User',
    'Privacy Settings',
);
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box light">
            <div class="portlet-title"><h4 class="uppercase">Current Privacy Settings</h4></div>
            <!-- end portlet title -->

            <div class="portlet-body">
                <div class="row margin-bottom-20">
                    <div class="col-md-12">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10%">Privacy </td>
                                <td><select id="subject" class="pull-left">
                                        <option value="Open">None</option>
                                        <option value="In Progress">Privacy 1</option>
                                        <option value="Close">Privacy 2</option>
                                    </select></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- end privacy -->

                <div class="row">
                    <div class="col-md-12"><h4 class="grey uppercase">Privacy Policy</h4></div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="scroll-inside">
                            <div class="scroller" style="height: 455px;" data-always-visible="1" data-rail-visible="0">
                                <div class="privacy-content">
                                    <h5>HOW WE PROTECT YOUR PRIVACY</h5>
                                    <p>This privacy policy tells you how we collect, use, and protect your personal information. By visiting our website, you accept and agree to the terms and conditions of this privacy policy. In particular, you consent to our collection and use of your personal information as described in this privacy policy.</p>

                                    <h5>MINORS</h5>
                                    <p>We do not provide services or sell products to children. If you are below the age of 18, you may use our website only with the permission and active involvement of a parent or legal guardian. If you are a minor, please do not provide us or other website visitors with any personal information.</p>

                                    <h5>POLICY IS PART OF OUR TERMS AND CONDITIONS OF USE</h5>
                                    <p>Our privacy policy is part of, and subject to, our website's terms and conditions of use. You may view these terms and conditions on our website.</p>

                                    <h5>THE TYPE OF INFORMATION WE COLLECT FROM YOU</h5>
                                    <p>Like most places on the Internet, simply by visiting our website you automatically tell us certain information. This includes basic information such as your IP address, when you visited, the website from where you came prior to visiting us, the website where you go when you leave our website, your computer's operating system, and the type of web browser that you are using. Our website automatically records this basic information about you.</p>

                                    <p>And like many other websites, we may use cookies. In plain English, this means information that our website's server transfers to your computer. This information can be used to track your session on our website. Cookies may also be used to customize our website content for you as an individual. If you are using one of the common Internet web browsers, you can set up your browser to either let you know when you receive a cookie or to deny cookie access to your computer.</p>

                                    <p>We may also collect any data that you provide us by posting it at our website or by e-mail. You can always choose not to provide us with information. However, if you do withhold information, we may deny you access to some or all of our website's services and features.</p>

                                    <p>Some transactions between you and our website may involve payment by credit card, debit card, checks, money orders, and/or third party online payment services. In such transactions, we will collect information related to the transaction as part of the course of doing business with you, including your billing address, telephone number, and other information related to the transaction.</p>

                                    <h5>WHAT WE DO WITH YOUR INFORMATION</h5>
                                    <p>We use your information to operate our website's business activities. For example, we may use this data to contact you about changes to our website, new services, or special offers, resolve disputes, troubleshoot issues, and enforce our website's terms and conditions.</p>

                                    <p>As a general rule, we will not give your data to third parties without your permission. However, there are some important exceptions to this rule that are described in the following paragraphs.</p>

                                    <p>We may, in our sole discretion, provide information about you to law enforcement or other government officials for purposes of fraud investigations, alleged intellectual property infringement, or any other suspected illegal activity or matters that may expose us to legal liability.</p>

                                    <p>Although we do not disclose individually identifiable information, we may disclose aggregate data about our website's visitors to advertisers or other third parties for marketing and promotional purposes.</p>

                                    <p>From time to time, we may use third party suppliers to provide services on our website. If a supplier wants to collect information about you, you will be notified. However, you will never be required to give information to a third party supplier. We restrict the way third party suppliers can use your information. They are not allowed to sell or give your information to others.</p>

                                    <h5>USER NAMES AND PASSWORDS</h5>
                                    <p>Your access to parts of our website may be protected by a user name and a password. Do not give your password to anyone. If you enter a section of our website that requires a password, you should log out when you leave. As a safety precaution, you should also close out of your web browser completely and re-open it before viewing other parts of the Internet.</p>

                                    <h5>YOUR USE OF INFORMATION AND UNSOLICITED JUNK E-MAIL</h5>
                                    <p>If you obtain personally identifiable information about another website user, you are not allowed to disclose this information to anyone else without the consent of the user and our consent too.</p>

                                    <p>We hate junk e-mail (Spam). Information you obtain from our website about us or other site users cannot be used by you or others to send unsolicited commercial e-mail or to send unsolicited commercial communications via our website's posting or other communication systems.</p>

                                    <h5>YOUR VOLUNTARY DISCLOSURE OF INFORMATION TO THIRD PARTIES WHO ARE NOT OUR SUPPLIERS</h5>
                                    <p>You may choose to provide personal information to website visitors or other third parties who are not our suppliers. Please use caution when doing so. The privacy policies and customs of these third parties determine what is done with your information.</p>

                                    <h5>AUTORESPONDERS</h5>
                                    <p>We may use autoresponders to communicate with you by e-mail. To protect your privacy, we use a verified opt-in system for such communications and you can always opt-out of such communications using the links contained in each autoresponder message. If you have difficulties opting out, you may contact us by sending an e-mail to info[at]mcmmgroup.com, or sending us mail to the address listed below.</p>

                                    <h5>POLICY CHANGES</h5>
                                    <p>The terms of this policy may change from time to time. If such changes are made, we will notify you by a notice posted on our website's home page of the changes that have been made. If you disagree with the changes that have been made, please contact us (by e-mail, using a website contact form, or in writing by mail), and any changes made to this policy will not apply to information we have collected from you prior to making the changes.</p>
                                    <p>If you are concerned about the topic covered by this policy, you should read it each time before you use our website. Any questions or concerns about this policy should be brought to our attention by sending an e-mail to info[at]mcmmgroup.com and providing us with information relating to your concern.</p>

                                    <p>You may also mail your concerns to us at the following address:</p>

                                    <p>MCMM Group BV<br>
                                        Ortelliuslaan 23<br>
                                        Utrecht, Utrecht 3528 BA<br>
                                        the Netherlands</p>

                                    This privacy policy was last updated on 03-15-2012.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body -->
        </div>
    </div>
</div>
