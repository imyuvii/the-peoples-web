<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Account Settings";
$this->breadcrumbs=array(
    'User',
    'Account Settings',
);
?>


<div class="row">
    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">LOGIN INFO</h4>
                <div class="tools">
                    <a class="grey" href="javascript:void()" data-original-title="" title=""><i class="fa fa-pencil"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="30%" align="right">Username</td>
                        <td><strong>johndoe1985</strong></td>
                    </tr>
                    <tr>
                        <td align="right">Password</td>
                        <td><strong>************</strong></td>
                    </tr>
                    <tr>
                        <td align="right">Security Question</td>
                        <td><strong>Childhood name</strong></td>
                    </tr>
                    <tr>
                        <td align="right">Security Answer</td>
                        <td><strong>John</strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- end login -->
    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">CHANGE PASSWORD</h4>
            </div>
            <div class="portlet-body">
                <table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="30%" align="right">New Password</td>
                        <td><input name="" type="text" class="form-control"></td>
                    </tr>
                    <tr>
                        <td width="30%" align="right">Retype Password</td>
                        <td><input name="" type="text" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><a href="javascript:void()" class="btn btn-success">Change Password</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- end change -->
</div>
<!-- end tabs overview -->

<div class="row">
    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Payout information</h4>
                <div class="tools">
                    <a title="" data-original-title="" href="javascript:void()" class="grey"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12 margin-bottom-10">
                        <h5 class="bold">Currently our preferred payment partner for the pay-our option is Neteller.</h5>
                        <a class="btn btn-success margin-bottom-15" href="javascript:void()">Create new Neteller Account</a>
                    </div>
                </div>
                <div class="row margin-bottom-20">
                    <div class="form-group">
                        <label class="col-xs-6 control-label col-lg-6">Your Neteller Account ID*</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="inputSuccess">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-6 control-label col-lg-6">Your Neteller Account email</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="inputSuccess">
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portlet body -->
        </div>
        <!-- end portlet -->
    </div>
</div>