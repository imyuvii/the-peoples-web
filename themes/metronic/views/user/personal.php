<?php
/* @var $this SiteController */

$this->pageTitle="Personal Settings";
$this->breadcrumbs=array(
    'User',
    'Personal Settings',
);
?>

<div class="row">
    <div class="col-md-3">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Profile Image</h4>
            </div>
            <div class="portlet-body">
                <div class="profile-information">
                    <div class="profile-pic text-center">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/profile.jpg" class="img-responsive" alt="">
                        <i></i>
                    </div>
                </div>
                <?php

                $this->widget('Uploader');  //for the upload form
                ?>
                <a data-target="#upload" data-toggle="modal" href="javascript:void()" class="profile-btns">Upload Image</a>
                <a href="javascript:void()" class="profile-btns">Click from Webcam</a>
                <a href="javascript:void()" class="profile-btns">Remove Image</a>
            </div>
        </div>
    </div>
    <!-- end profile -->
    <div class="col-md-9">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">My Personal Id Card</h4>
                <div class="tools">
                    <a href="" class="grey"><i class="fa fa-edit"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-7 col-sm-7 margin-bottom-20">
                        <table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="30%" align="right">Full Name</td>
                                <td><strong><?php echo $model->first_name.' '.$model->last_name; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="right">Nickname</td>
                                <td><strong><?php echo $model->nickname; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="right">Member Since</td>
                                <td><strong><?php echo $model->created_at; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="right">Account</td>
                                <td><strong><?php echo $model->email; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="right">Parent/Sponsor</td>
                                <td><strong>See Contact Sheet</strong></td>
                            </tr>
                            <tr>
                                <td align="right" valign="top">Network</td>
                                <td><strong>3 Personal<br>12 Team</strong></td>
                            </tr>
                        </table>
                    </div>
                    <!-- end profile detail -->
                    <div class="col-md-5 col-sm-5">
                        <div class="profile-information big">
                            <div class="profile-pic text-center">
                                <img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/profile.jpg">
                                <i></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end profile-top -->
            </div>
        </div>
    </div>
    <!-- end profile 2 -->
</div>
<!-- end personal overview -->

<div class="row">
    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Personal Info</h4>
                <div class="tools">
                    <a href="" class="grey"><i class="fa fa-edit"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="right">Lives.in</td>
                        <td><strong><?php echo $model->city; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Country</td>
                        <td><strong><?php echo $model->country; ?></strong></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">Description</td>
                        <td><strong><?php echo $model->description_bio; ?></strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- end personal info -->

    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Contact Info</h4>
                <div class="tools">
                    <a href="" class="grey"><i class="fa fa-edit"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="30%" align="right">Address 1</td>
                        <td><strong><?php echo $model->building_num . ' '.$model->street.' '.$model->region; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Address 2</td>
                        <td><strong><?php echo $model->city; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Phone</td>
                        <td><strong><?php echo $model->phone; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Email</td>
                        <td><strong><?php echo $model->city; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Skype</td>
                        <td><strong><?php echo $model->skype; ?></strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- end change -->
</div>

<div class="row">
    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Company Info</h4>
                <div class="tools">
                    <a class="grey" href=""><i class="fa fa-edit"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="table1">
                    <tbody><tr>
                        <td width="30%" align="right">Company Name</td>
                        <td><strong><?php echo $model->business_name; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Address 1</td>
                        <td><strong><?php echo $model->busAddress_building_num.' '.$model->busAddress_street; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Address 2</td>
                        <td><strong><?php echo $model->busAddress_region; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Postal Code City</td>
                        <td><strong><?php echo $model->busAddress_city . ' - ' . $model->busAddress_postcode; ?></strong></td>
                    </tr>
                    <tr>
                        <td align="right">Country</td>
                        <td><strong><?php echo $model->busAddress_country; ?></strong></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">Vat No</td>
                        <td><strong><?php echo $model->vat_number; ?></strong></td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
    </div>
    <!-- end company profile -->

    <div class="col-md-6">
        <div class="portlet box light">
            <div class="portlet-title">
                <h4 class="uppercase">Social Media profiles</h4>
                <div class="tools">
                    <a class="grey" href=""><i class="fa fa-edit"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <p>Click on an icon to share your affiliate URL on your social media profiles.</p>
                <div class="scroller" style="height:200px">
                    <ul class="share-links">
                        <li class="form-group">
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-facebook-square"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_fb_url; ?>" placeholder="Facebook" class="form-control">
                            </div>
                        </li>

                        <li>
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-twitter-square"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_twitter_url; ?>" placeholder="Twitter" class="form-control">
                            </div>
                        </li>

                        <li>
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-youtube-square"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_youtube_url; ?>" placeholder="Youtube" class="form-control">
                            </div>
                        </li>

                        <li>
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-snapchat-square"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_snapchat_url; ?>" placeholder="Snapchat" class="form-control">
                            </div>
                        </li>

                        <li>
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-pinterest-square"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_pinterest_url; ?>" placeholder="Pinterest" class="form-control">
                            </div>
                        </li>

                        <li>
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-linkedin-square"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_linkedin_url; ?>" placeholder="LinkedIn" class="form-control">
                            </div>
                        </li>

                        <li>
                            <div class="input-group">
                                        <span class="input-group-addon">
                                        <i class="fa fa-instagram"></i>
                                        </span>
                                <input type="text" value="<?php echo $model->social_instagram_url; ?>" placeholder="Instagram" class="form-control">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end social media -->
</div>
<!-- end profile-bottom --->