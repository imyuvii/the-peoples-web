<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Direct Sales Bonus";
$this->breadcrumbs=array(
    'Account',
    'Ripply Royalty Bonus',
);
?>
<div class="row">
    <div class="col-lg-6">
        <h3>Ripple Royalty Bonus</h3>
        <div class="table-scrollable rrb-nest-block">
            <div class="scroller" style="height:500px;" data-always-visible="1" data-rail-visible="0">
                <div class="dd rrb-nest-reward" id="nestable_list_1">
                    <ol class="dd-list">
                        <li class="dd-item" data-id="2">
                            <div class="dd-handle reward-nest">
                                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><i class="reward president"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-president.png"/></i></td>
                                        <td width="55"><a href="profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                        <td><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                            ID:123456 </td>
                                        <td align="center"><h1 class="green bold">4%</h1></td>
                                        <td align="right"><strong class="f18">Current Rank</strong><br>
                                            <h4 class="green">President</h4></td>
                                    </tr>
                                </table>
                            </div>
                            <ol class="dd-list">
                                <li class="dd-item" data-id="5">
                                    <div class="dd-handle inline-block reward-nest">
                                        <i class="levl-per">1%</i>
                                        <i class="levl-per bonus">2%</i>
                                        <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <td width="60"><i class="reward"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-leader.png"/></i><a href="profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                <td width="100"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                    ID:123456 </td>
                                                <td width="50" align="right"><h1 class="green bold">3%</h1></td>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td colspan="2"><strong>Ripple Revenue</strong></td>
                                                <td align="right"><h5 class="green">&euro;345,67</h5></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="6">
                                            <div class="dd-handle inline-block reward-nest">
                                                <i class="levl-per">1%</i>
                                                <i class="levl-per bonus">1%</i>
                                                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <thead>
                                                    <tr>
                                                        <td width="60"><i class="reward"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-ambassador.png"/></i><a href="profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                        <td width="100"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                            ID:123456 </td>
                                                        <td width="50" align="right"><h1 class="green bold">2%</h1></td>
                                                    </tr>
                                                    </thead>
                                                    <tr>
                                                        <td colspan="2"><strong>Ripple Revenue</strong></td>
                                                        <td align="right"><h5 class="green">&euro;345,67</h5></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle inline-block reward-nest">
                                                        <i class="levl-per">1%</i>
                                                        <i class="levl-per bonus">1%</i>
                                                        <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <thead>
                                                            <tr>
                                                                <td width="60"><i class="reward"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-premier.png"/></i><a href="profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                                <td width="100"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                                    ID:123456 </td>
                                                                <td width="50" align="right"><h1 class="green bold">1%</h1></td>
                                                            </tr>
                                                            </thead>
                                                            <tr>
                                                                <td colspan="2"><strong>Ripple Revenue</strong></td>
                                                                <td align="right"><h5 class="green">&euro;345,67</h5></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                    <!-- end list-->
                                </li>
                                <li class="dd-item" data-id="6">
                                    <div class="dd-handle inline-block reward-nest">
                                        <i class="levl-per">2%</i>
                                        <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <td width="60"><i class="reward"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-ambassador.png"/></i><a href="profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                <td width="100"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                    ID:123456 </td>
                                                <td width="50" align="right"><h1 class="green bold">2%</h1></td>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td colspan="2"><strong>Ripple Revenue</strong></td>
                                                <td align="right"><h5 class="green">&euro;345,67</h5></td>
                                            </tr>
                                        </table>
                                    </div>
                                </li>
                                <!-- end -->
                                <li class="dd-item" data-id="6">
                                    <div class="dd-handle inline-block reward-nest">
                                        <i class="levl-per">3%</i>
                                        <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <td width="60"><i class="reward"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-premier.png"/></i><a href="profile.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                <td width="100"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                    ID:123456 </td>
                                                <td width="50" align="right"><h1 class="green bold">1%</h1></td>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td colspan="2"><strong>Ripple Revenue</strong></td>
                                                <td align="right"><h5 class="green">&euro;345,67</h5></td>
                                            </tr>
                                        </table>
                                    </div>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- user dt -->
    </div>
    <!-- left column -->
    <div class="col-lg-6">
        <h3>Points Bonus Details</h3>
        <div class="table-scrollable">
            <table class="table f14" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th colspan="2">User Details</th>
                    <th>Rank</th>
                    <th align="center">Ripple Revenue</th>
                    <th align="center">RRB%</th>
                    <th align="right">Commission</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td width="55"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                    <td><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                        ID:123456 </td>
                    <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-diamond-leader.png" width="50"/></td>
                    <td align="center"><strong class="green bold">€345,6</strong></td>
                    <td align="center"><strong class="bold">4%</strong></td>
                    <td align="right"><strong class="green bold">€ 69,134</strong></td>
                </tr>
                <tr>
                    <td width="55"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                    <td><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                        ID:123456 </td>
                    <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-leader.png" width="50"/></td>
                    <td align="center"><strong class="green bold">€345,6</strong></td>
                    <td align="center"><strong class="bold">3%</strong></td>
                    <td align="right"><strong class="green bold">€ 69,134</strong></td>
                </tr>
                <tr>
                    <td width="55"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                    <td><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                        ID:123456 </td>
                    <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-leader.png" width="50"/></td>
                    <td align="center"><strong class="green bold">€345,6</strong></td>
                    <td align="center"><strong class="bold">2%</strong></td>
                    <td align="right"><strong class="green bold">€ 69,134</strong></td>
                </tr>
                <tr>
                    <td width="55"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                    <td><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                        ID:123456 </td>
                    <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-leader.png" width="50"/></td>
                    <td align="center"><strong class="green bold">€345,6</strong></td>
                    <td align="center"><strong class="bold">1%</strong></td>
                    <td align="right"><strong class="green bold">€ 69,134</strong></td>
                </tr>
                <tr>
                    <td width="55"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                    <td><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                        ID:123456 </td>
                    <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-leader.png" width="50"/></td>
                    <td align="center"><strong class="green bold">€345,6</strong></td>
                    <td align="center"><strong class="bold">0%</strong></td>
                    <td align="right"><strong class="green bold">€ 69,134</strong></td>
                </tr>
                <tr class="bg-black">
                    <td colspan="3">Total RRB in 'month'</td>
                    <td align="center"><strong class="bold">€ 17600,88</strong></td>
                    <td align="center">&nbsp;</td>
                    <td align="right"><strong class="bold">€ 276,536</strong></td>
                </tr>
                <tr class="bg-black">
                    <td colspan="3">Total 2014</td>
                    <td align="center"><strong class="bold">€ 17600,88</strong></td>
                    <td align="center">&nbsp;</td>
                    <td align="right"><strong class="bold">€ 17600,88</strong></td>
                </tr>
                <tr class="bg-green">
                    <td colspan="3">All Time Total</td>
                    <td align="center"><strong class="bold">€ 17600,88</strong></td>
                    <td align="center">&nbsp;</td>
                    <td align="right"><strong class="bold">€ 17600,88</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- end point bonus -->
    </div>
</div>
