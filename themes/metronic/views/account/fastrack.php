<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Fast Track Bonus";
$this->breadcrumbs=array(
    'Account',
    'Fast Track Bonus',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="block margin-bottom-20 bg-grey">
            <div class="table-scrollable">
                <h3><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-direct-sales-bonus.png" width="36"></i> Personal Fast Track Bonus</h3>
                <table class="table max1000" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th width="50%" colspan="3">User Details</th>
                        <th width="12%" align="center">You</th>
                        <th width="12%" align="center">Level 1</th>
                        <th width="12%" align="center">Level 2</th>
                        <th width="12%" align="right">Total</th>
                    </tr>
                    </thead>
                    <tr>
                        <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                        <td>
                            <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                        </td>
                        <td>
                            All Time Accumulated Totals
                        </td>
                        <td align="center">&euro; 500</td>
                        <td align="center">&euro; 500</td>
                        <td align="center">&euro; 500</td>
                        <td valign="middle" align="right"><h5 class="green bold">&euro; 500</h5></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end ftb month -->

<div class="panel-group accordion" id="accordion3">
    <div class="panel panel-default bg-grey">
        <h4 class="panel-title">
            <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                <i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-fast-track-bonus.png" width="36"/></i> Level 1 Fast-Track Bonus

            </a>
        </h4>

        <div id="collapse_3_1" class="panel-collapse in active">
            <div class="panel-body pad15">
                <div class="table-scrollable">
                    <table class="table max1000 margin-bottom-20" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th colspan="3">User Details</th>
                            <th align="center">Date</th>
                            <th align="center">FTB Q</th>
                            <th align="center">Progress</th>
                            <th align="center">Potential Bonus</th>
                            <th align="right">Bonus</th>
                        </tr>
                        </thead>
                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center"><i class="fa fa-close red"></i></td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center">&nbsp;</td>
                            <td align="right"><h4 class="green bold">&euro; 0</h4></td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    </div>
                                </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center"><h4 class="green bold">&euro; 150</h4></td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                    </div>
                                </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center"><h4 class="green bold">&euro; 35</h4></td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <i class="fa fa-check green"></i>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center">&nbsp;</td>
                            <td align="right"><h4 class="green bold">&euro; 500</h4></td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                    </div>
                                </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center"><h4 class="green bold">&euro; 10</h4></td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr class="bg-green">
                            <td colspan="6"><h4 class="bold">Total</h4></td>
                            <td align="center"><h4 class="bold">&euro; 195</h4></td>
                            <td align="right" colspan="3"><h4 class="bold">&euro; 500</h4></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end accordian level 1 -->

    <div class="panel panel-default bg-grey">
        <h4 class="panel-title">
            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
                <i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-fast-track-bonus.png" width="36"/></i> Level 2 Fast-Track Bonus
            </a>
        </h4>
        <div id="collapse_3_2" class="panel-collapse collapse">
            <div class="panel-body pad15">
                <div class="table-scrollable">
                    <table class="table max1000 margin-bottom-20" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr>
                            <th colspan="3">User Details</th>
                            <th align="center">Date</th>
                            <th align="center">FTB Q</th>
                            <th align="center">Progress</th>
                            <th align="center">Potential Bonus</th>
                            <th align="right">Bonus</th>
                        </tr>
                        </thead>
                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center"><i class="fa fa-close red"></i></td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center">&nbsp;</td>
                            <td align="right"><h4 class="green bold">&euro; 0</h4></td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    </div>
                                </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center"><h4 class="green bold">&euro; 150</h4></td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                    </div>
                                </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center"><h4 class="green bold">&euro; 35</h4></td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <i class="fa fa-check green"></i>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center">&nbsp;</td>
                            <td align="right"><h4 class="green bold">&euro; 500</h4></td>
                        </tr>

                        <tr>
                            <td width="55"><a href="javascript:void()" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"><i class="online"></i></a></td>
                            <td>
                                <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                ID:123456
                            </td>
                            <td>
                                John Doe<br>
                                johndoe1985@gmail.com
                            </td>
                            <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                            <td align="center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                    </div>
                                </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-user"></i>
                                <i class="fa fa-plus"></i>
                            </td>
                            <td align="center"><h4 class="green bold">&euro; 10</h4></td>
                            <td align="right">&nbsp;</td>
                        </tr>

                        <tr class="bg-green">
                            <td colspan="6"><h4 class="bold">Total</h4></td>
                            <td align="center"><h4 class="bold">&euro; 195</h4></td>
                            <td align="right" colspan="3"><h4 class="bold">&euro; 500</h4></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end accordian level 2 -->
</div>
