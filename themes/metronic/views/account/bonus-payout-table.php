<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Bounus Payout Table</title>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
</head>
<style>
    .f28 {
        font-size: 28px;
    }
    body{
        font-family: Open Sans,Arial,sans-serif;
    }
    .bg-grey td {background:#dce0ea; padding:5px;}
    .bg-grey td.first {border-radius:10px 0 0 10px; padding-left:5px; min-width:40px;}
    .bg-grey td.last {border-radius:0 10px 10px 0; padding-right:5px;}
    .rotae90{-moz-transform: rotate(-90deg);-webkit-transform: rotate(-90deg);-o-transform: rotate(-90deg);-ms-transform: rotate(-90deg);
        transform: rotate(-90deg)}
    .img-59 {max-width:59px;}
    .numeric {text-align:center; padding-left:3px; padding-right:3px;}
    .page-title {
        color: #2e71b5;
        display: block;
        font-size: 30px;
        text-align:center;
        font-weight: 500;
        letter-spacing: -1px;
        margin: 0 0 20px;
        font-weight:bold;
        padding: 0;
    }
    td.no-bg {background:none;}
    table.td-border tr td {border-bottom:2px solid #ebecef; font-size:14px;text-align:center;}
    table.td-border thead tr td {border-bottom:none !Important;}
    .bonus-payout {padding-left:74px; position:relative;}
    .bonus-payout .rotae90 {position:absolute; left: -72px;  position: absolute; top: 275px;}
    .tble-perct.td-border img{max-width:100%;}


</style>
<body>
<h1 class="page-title">BONUS PAYOUT TABLE (IN %)</h1>
<div class="small-font">
    <table class="tble-perct td-border" width="100%" cellspacing="0" cellpadding="0" align="center">
        <thead>
        <tr>
            <td width="12%">&nbsp;</td>
            <td width="10%"><h5>Level 1</h5></td>
            <td class="numeric" width="10%"><h5>Level 2</h5></td>
            <td class="numeric" width="10%"><h5>Level 3</h5></td>
            <td class="numeric" width="10%"><h5>Level 4</h5></td>
            <td class="numeric" width="10%"><h5>Level 5</h5></td>
            <td class="numeric" width="10%"><h5>Level 6</h5></td>
            <td class="numeric" width="10%"><h5>Level 7</h5></td>
            <td class="numeric" width="10%"><h5>Level 8</h5></td>
        </tr>
        </thead>
        <tbody>
        <tr class="bg-grey">
            <td class="first"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/pack-free.png"></td>
            <td>1.5</td>
            <td class="numeric">1.5</td>
            <td class="numeric">1.5</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-business-associate.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-associate-manager.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-team-manager.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-lift.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-premier.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">-</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-ambassador.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">-</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-leader.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">1</td>
            <td class="numeric last">-</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-silver-leader.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">1</td>
            <td class="numeric last">1</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-gold-leader.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">1</td>
            <td class="numeric last">1</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-diamond-leader.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">1</td>
            <td class="numeric last">1</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-global-leader.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">4</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">1</td>
            <td class="numeric last">1</td>
        </tr>
        <tr class="bg-grey">
            <td class="first"><span class="numeric"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/img/badges/icon-president.png"></span></td>
            <td>5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">5</td>
            <td class="numeric">3</td>
            <td class="numeric">2</td>
            <td class="numeric">1</td>
            <td class="numeric last">1</td>
        </tr>
        </tbody>
    </table>
    <br>
    <i class="lgrey">* commissions will be paid as long as there are products or services sold to customers</i> </div>
</body>
</html>
