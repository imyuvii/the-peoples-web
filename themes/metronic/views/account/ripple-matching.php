<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Ripple Matching Bonus";
$this->breadcrumbs=array(
    'Account',
    'Ripple Matching Bonus',
);
?>
<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="rank-block">
            <h3>Ripple Matching Bonus</h3>
            <div class="rank-active"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/badges/icon-business-associate.png" class="img-responsive"><i></i></div>
            <div class="rank-body">
                <h4 class="margin-bottom-10 grey">Current Rank</h4>
                <h4 class="green bold uppercase">Business Associate</h4>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- user dt -->
        <div class="block bg-grey margin-bottom-15">
            <h3>My Current Payout Levels</h3>
            <table width="100%" class="table no-space" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <th width="60%">&nbsp;</th>
                <th align="right"><img width="45" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-badges/icon-badges1.png"></th>
                </thead>
                <tr>
                    <td>Level 1</td>
                    <td align="right">20%</td>
                </tr>
                <tr>
                    <td>Level 2</td>
                    <td align="right">10%</td>
                </tr>
                <tr>
                    <td>Level 3</td>
                    <td align="right">10%</td>
                </tr>
                <tr>
                    <td>Level 4</td>
                    <td align="right">10%</td>
                </tr>
            </table>
            <div class="clearfix"></div>
        </div>
        <!-- end levels -->
        <div class="block bg-grey margin-bottom-15">
            <h3>Historic Earnings</h3>
            <table width="100%" class="table no-space" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <th width="50%">Run</th>
                <th align="right">Commission</th>
                </thead>
                <tr>
                    <td>January, 2014</td>
                    <td align="right"><strong class="green">€ 5,20</strong></td>
                </tr>
                <tr>
                    <td>February, 2014</td>
                    <td align="right"><strong class="green">€ 10</strong></td>
                </tr>
                <tr>
                    <td>March, 2014</td>
                    <td align="right"><strong class="green">€ 35</strong></td>
                </tr>
                <tr>
                    <td>April, 2014</td>
                    <td align="right"><strong class="green">€ 10</strong></td>
                </tr>
                <tr class="bg-black">
                    <td>Total 2014</td>
                    <td align="right">€125</td>
                </tr>
                <tr class="bg-green">
                    <td>All Time Total</td>
                    <td align="right">€12520</td>
                </tr>
            </table>
            <div class="clearfix"></div>
        </div>
        <!-- historic earnings -->
    </div>
    <!-- left column -->
    <div class="col-lg-8 col-md-7">
        <div class="block bg-grey margin-bottom-30">
            <h3>Your Ripple Matching Earnings</h3>
            <p>Our privacy policy allows us only to show your first comission line</p>
            <div class="table-scrollable">
                <table class="table max650" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th align="center">Current</th>
                        <th align="center">Monthly Resi. Income</th>
                        <th align="right">Total Accrued</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Commission Level 1</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td>Commission Level 2</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td>Commission Level 3</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr class="bg-black">
                        <td>Total RMB Earned in 'month'</td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="right"><strong class="bold">€17600,88</strong></td>
                    </tr>
                    <tr class="bg-black">
                        <td>Total 2014</td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="right"><strong class="bold">€17600,88</strong></td>
                    </tr>
                    <tr class="bg-green">
                        <td>All Time Total</td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="right"><strong class="bold">€17600,88</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- ripple earning -->
        <div class="margin-bottom-20">
            <h3>Commission Level</h3>
            <p>Our privacy policy allows us only to show your first comission line</p>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable noborder">
                        <div class="dd" id="nestable_list_1">
                            <ol class="dd-list">
                                <li class="dd-item" data-id="2">
                                    <div class="dd-handle with-thead">
                                        <table class="max550" width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <th colspan="2" width="22%" height="35" align="center">User Details</th>
                                                <th width="38%" align="center">Current Month RB</th>
                                                <th width="38%"> <span class="pull-right">Level 1 Matching 20%</span> </th>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td width="55" align="center"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                <td align="left"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                    ID:123456 </td>
                                                <td align="center"><h4 class="green bold">&euro;123,14</h4></td>
                                                <td align="right"><h4 class="green bold">&euro;69,134</h4></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle with-thead">
                                                <table class="max550" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="2" width="22%" height="35" align="center">User Details</th>
                                                        <th width="38%" align="center">Current Month RB</th>
                                                        <th width="38%"> <span class="pull-right">Level 2 Matching 10%</span> </th>
                                                    </tr>
                                                    </thead>
                                                    <tr>
                                                        <td width="55" align="center"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                        <td align="left"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                            ID:123456 </td>
                                                        <td align="center"><h4 class="green bold">&euro;123,14</h4></td>
                                                        <td align="right"><h4 class="green bold">&euro;69,134</h4></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle with-thead">
                                                        <table class="max550" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <thead>
                                                            <tr>
                                                                <th colspan="2" width="22%" height="35" align="center">User Details</th>
                                                                <th width="38%" align="center">Current Month RB</th>
                                                                <th width="38%"> <span class="pull-right">Level 3 Matching 10%</span> </th>
                                                            </tr>
                                                            </thead>
                                                            <tr>
                                                                <td width="55" align="center"><a href="profile.html" class="online"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png" class="user-img"></a></td>
                                                                <td align="left"><h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                                                    ID:123456 </td>
                                                                <td align="center"><h4 class="green bold">&euro;123,14</h4></td>
                                                                <td align="right"><h4 class="green bold">&euro;69,134</h4></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end commission level 1 -->
    </div>
</div>
