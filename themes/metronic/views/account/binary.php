<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Binary Bonus";
$this->breadcrumbs=array(
    'Account',
    'Binary',
);
?>
<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="rank-block">
            <h3>Binary Bonus</h3>
            <div class="rank-active"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/badges/icon-business-associate.png" class="img-responsive"><i></i></div>
            <div class="rank-body">
                <h4 class="margin-bottom-10 grey">Current Rank</h4>
                <h4 class="green bold uppercase">Business Associate</h4>
            </div>

            <div class="clearfix"></div>
        </div>
        <!-- user dt -->

        <div class="block bg-grey margin-bottom-15">
            <h3>My Current Payout Levels</h3>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="table no-space">
                <tbody><tr>
                    <td class="noborder">Payout Percentage</td>
                    <td class="noborder" align="right">10%</td>
                </tr>
                <tr>
                    <td>Weekly CAP</td>
                    <td align="right">&euro; 2000</td>
                </tr>
                </tbody></table>
            <div class="clearfix"></div>
        </div>
        <!-- end levels -->

        <div class="block bg-grey margin-bottom-15">
            <h3>Historic Earnings</h3>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="table no-space">
                <thead>
                <tr><th >Run</th>
                    <th align="right">Commission</th>
                </tr></thead>
                <tbody><tr>
                    <td>Week 12 2014</td>
                    <td align="right"><strong class="green">€ 5,20</strong></td>
                </tr>
                <tr>
                    <td>Week 11 2014</td>
                    <td align="right"><strong class="green">€ 10</strong></td>
                </tr>
                <tr>
                    <td>Week 10 2014</td>
                    <td align="right"><strong class="green">€ 35</strong></td>
                </tr>
                <tr>
                    <td>Week 9 2014</td>
                    <td align="right"><strong class="green">€ 10</strong></td>
                </tr>
                <tr class="bg-black">
                    <td>Total 2014</td>
                    <td align="right">€125</td>
                </tr>
                <tr class="bg-green">
                    <td>All Time Total</td>
                    <td align="right">€12520</td>
                </tr>
                </tbody></table>
            <div class="clearfix"></div>
        </div>
        <!-- historic earnings -->
    </div>
    <!-- left column -->

    <div class="col-lg-8 col-md-7">
        <div class="block bg-grey margin-bottom-15">
            <h3>Points for Current Run</h3>
            <div class="table-scrollable">
                <table class="table max550" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-month.png"></td>
                        <td width="45%">Run Date from/to</td>
                        <td><span class="highlight">24-05-2014   00:00:00</span></td>
                        <td><span class="highlight">31-05-2014   00:00:00</span></td>
                    </tr>
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-run.png"></td>
                        <td >Points accumulated last run</td>
                        <td align="center">8 810</td>
                        <td align="center">110</td>
                    </tr>
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-run.png"></td>
                        <td >Points accumulated current run</td>
                        <td align="center">20 000</td>
                        <td align="center">15 000</td>
                    </tr>
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-run.png"></td>
                        <td >Points total for this run</td>
                        <td><span class="highlight">28810</span></td>
                        <td><span class="highlight">15110</span></td>
                    </tr>
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-run.png"></td>
                        <td >Potential Payout</td>
                        <td colspan="2"><span class="highlight">€260</span></td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- end point -->

        <div class="block bg-grey margin-bottom-15">
            <h3>Latest Closed Run</h3>
            <div class="table-scrollable">
                <table class="table max550" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th align="center">Points Left</th>
                    <th align="center">Points Right</th>
                    </thead>
                    <tr>
                        <td width="5%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-run.png"></td>
                        <td >Points accumulated last run</td>
                        <td align="center">23 210</td>
                        <td align="center">28 910</td>
                    </tr>
                    <tr>
                        <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-balance.png"></td>
                        <td >Balance for this calculation</td>
                        <td align="center">Payleg</td>
                        <td align="center">Powerleg</td>
                    </tr>
                    <tr>
                        <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-steps.png"></td>
                        <td >Steps made in last</td>
                        <td align="center">58</td>
                        <td align="center">36</td>
                    </tr>
                    <tr class="bg-black">
                        <td width="55%" colspan="2">Steps used for calculation</td>
                        <td align="center">36</td>
                        <td align="center">36</td>
                    </tr>
                    <tr>
                        <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-caluclate.png"></td>
                        <td >Points used for calculation</td>
                        <td align="center">36 X 400 = 14 400</td>
                        <td align="center">36 X 800 = 28 800</td>
                    </tr>
                    <tr class="bg-black">
                        <td width="55%" colspan="2">Transferable Points</td>
                        <td align="center">8 810</td>
                        <td align="center">110</td>
                    </tr>
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-balance.png"></td>
                        <td >Balanced for next calculation</td>
                        <td align="center">Powerleg</td>
                        <td align="center">Payleg</td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td width="20" class="noborder"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-pay.png"></td>
                        <td  class="noborder">Payout Conversion level</td>
                        <td class="noborder"><span class="highlight">10%</span></td>
                        <td class="noborder"><span class="highlight">&euro; 2880</span></td>
                    </tr>
                    <tr>
                        <td width="20"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-rank.png"></td>
                        <td >Commission CAP</td>
                        <td>&nbsp;</td>
                        <td><span class="highlight">&euro; 2000</span></td>
                    </tr>
                    <tr class="bg-green">
                        <td colspan="2">Commissions Earned week 13,2014</td>
                        <td>&nbsp;</td>
                        <td align="center">€ 2880</td>
                    </tr>
                </table>
            </div>

            <div class="block text-center"><a href="javascript:void()" class="btn btn-sm-block blue"  data-toggle="modal" data-target="#bonus">How to reach next step?</a></div>
        </div>
        <!-- end latest closed  -->
    </div>
</div>
<?php
/**
 * Bonus Payout Modal content
 */
?>
<div class="modal fade" id="bonus">
    <div class="modal-dialog wide" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close bonus-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="uppercase no-margin">Bonus Payout Table Overview</h4>
            </div>
            <div class="modal-body">
                <iframe src="<?php echo $this->createUrl('account/bonusPayoutTable'); ?>" style="zoom:0.60" width="99.6%" height="450" frameborder="0"></iframe>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>