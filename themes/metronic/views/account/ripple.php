<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Ripple Bonus";
$this->breadcrumbs=array(
    'Account',
    'Ripple Bonus',
);
?>
<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="rank-block">
            <h3>Ripple Bonus</h3>
            <div class="rank-active"><img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/badges/icon-business-associate.png"><i></i></div>
            <div class="rank-body">
                <h4 class="margin-bottom-10 grey">Current Rank</h4>
                <h4 class="green bold uppercase">Business Associate</h4>
            </div>

            <div class="clearfix"></div>
        </div>
        <!-- user dt -->

        <div class="block bg-grey margin-bottom-15">
            <h3>My Current Payout Levels</h3>
            <table width="100%" class="table no-space" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <th width="40%">&nbsp;</th>
                <th align="center"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-basic.png"></th>
                <th align="center"><img width="45" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-badges/icon-badges1.png"></th>
                <th align="center"><img width="45" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-badges/icon-badges3.png"></th>
                </thead>
                <tr>
                    <td>Level 1</td>
                    <td align="center">1.5%</td>
                    <td align="center">3.5%</td>
                    <td align="center">5%</td>
                </tr>
                <tr>
                    <td>Level 2</td>
                    <td align="center">1.5%</td>
                    <td align="center">3.5%</td>
                    <td align="center">5%</td>
                </tr>
                <tr>
                    <td>Level 3</td>
                    <td align="center">1.5%</td>
                    <td align="center">3.5%</td>
                    <td align="center">5%</td>
                </tr>
                <tr>
                    <td>Level 4</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">4%</td>
                    <td align="center">4%</td>
                </tr>
            </table>
            <div class="text-center"><a href="javascript:void()" class="btn btn-sm-block green" data-toggle="modal" data-target="#bonus">Bonus Payout Table Overview</a></div>
            <div class="clearfix"></div>
        </div>
        <!-- end levels -->

        <div class="block bg-grey margin-bottom-15">
            <h3>Historic Earnings</h3>
            <table width="100%" class="table no-space" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <th width="50%">Run</th>
                <th align="right">Commission</th>
                </thead>
                <tr>
                    <td>January, 2014</td>
                    <td align="right"><strong class="green">€ 5,20</strong></td>
                </tr>
                <tr>
                    <td>February, 2014</td>
                    <td align="right"><strong class="green">€ 10</strong></td>
                </tr>
                <tr>
                    <td>March, 2014</td>
                    <td align="right"><strong class="green">€ 35</strong></td>
                </tr>
                <tr>
                    <td>April, 2014</td>
                    <td align="right"><strong class="green">€ 10</strong></td>
                </tr>
                <tr class="bg-black">
                    <td>Total 2014</td>
                    <td align="right">€125</td>
                </tr>
                <tr class="bg-green">
                    <td>All Time Total</td>
                    <td align="right">€12520</td>
                </tr>
            </table>
            <div class="clearfix"></div>
        </div>
        <!-- historic earnings -->
    </div>
    <!-- left column -->

    <div class="col-lg-8 col-md-7">
        <div class="block bg-grey margin-bottom-15">
            <h3>Your Ripple Earnings</h3>
            <p>Our privacy policy allows us only to show your first comission line</p>
            <div class="table-scrollable">
                <table class="table max650" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th align="center">Current</th>
                        <th align="center">Monthly Resi. Income</th>
                        <th align="right">Total Accrued</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Commission Level 1</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td>Commission Level 2</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td>Commission Level 3</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td>Bonus Level 1</td>
                        <td align="center"><strong class="green bold">€123,45</strong></td>
                        <td align="center"><strong class="green bold">€764,56</strong></td>
                        <td align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr class="bg-black">
                        <td>Total Earned as Team Manager</td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="right"><strong class="bold">€17600,88</strong></td>
                    </tr>
                    <tr>
                        <td bgcolor="#E2E2E2">Bonus Level 2</td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€123,45</strong></td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€764,56</strong></td>
                        <td bgcolor="#E2E2E2" align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td bgcolor="#E2E2E2">Bonus Level 3</td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€123,45</strong></td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€764,56</strong></td>
                        <td bgcolor="#E2E2E2" align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td bgcolor="#E2E2E2">Bonus Level 4</td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€123,45</strong></td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€764,56</strong></td>
                        <td bgcolor="#E2E2E2" align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr>
                        <td bgcolor="#E2E2E2">Bonus Level 5</td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€123,45</strong></td>
                        <td bgcolor="#E2E2E2" align="center"><strong class="green bold">€764,56</strong></td>
                        <td bgcolor="#E2E2E2" align="right"><strong class="green bold">€4674,22</strong></td>
                    </tr>
                    <tr class="bg-green">
                        <td>Potential Earnings</td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="center"><strong class="bold">€17600,88</strong></td>
                        <td align="right"><strong class="bold">€17600,88</strong></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right"><a href="" class="btn blue no-margin">How to Earn Bonus?</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- ripple earning -->

        <div class="block margin-bottom-20 bg-grey">
            <h3>Commission Level 1</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table max650" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th colspan="2" width="30%">User Details</th>
                                <th width="55%">Product</th>
                                <th align="center" width="6%">Date</th>
                                <th align="center" width="9%">Order ID</th>
                                <th align="center" width="9%">This Month</th>
                                <th align="right" width="9%">Accrued</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-basic.png"> Basic Pack €149</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">ID:123456</td>
                                <td align="center"><strong class="green bold">€ 1,45</strong></td>
                                <td align="right" valign="middle"><strong class="green bold">€ 5</strong></td>
                            </tr>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-bronze.png"> Bronze Pack €299</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">ID:123456</td>
                                <td align="center"><strong class="green bold">€ 1,25</strong></td>
                                <td align="right" valign="middle"><strong class="green bold">€ 10</strong></td>
                            </tr>
                            <tr>
                                <td width="40"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-silver.png"> Silver Pack €599</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">ID:123456</td>
                                <td align="center"><strong class="green bold">€ 0,35</strong></td>
                                <td align="right" valign="middle"><strong class="green bold">€ 35</strong></td>
                            </tr>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-gold.png"> Gold Pack €1199</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">ID:123456</td>
                                <td align="center"><strong class="green bold">€ 0,75</strong></td>
                                <td align="right" valign="middle"><strong class="green bold">€ 75</strong></td>
                            </tr>
                            <tr class="bg-green">
                                <td colspan="4">Total</td>
                                <td colspan="3" align="right">$125</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end commission level 1 -->
    </div>
</div>
