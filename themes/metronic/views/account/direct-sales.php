<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Direct Sales Bonus";
$this->breadcrumbs=array(
    'Account',
    'Direct Sales Bonus',
);
?>
<div class="row">
    <div class="col-md-12">
        <div class="block margin-bottom-20 bg-grey">
            <h3><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-direct-sales-bonus.png" width="36"></i> Direct Sales Bonus</h3>
            <div class="table-scrollable f14">
                <table class="table max800" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th width="30%">Month</th>
                        <th width="12%" align="center">Starting Date</th>
                        <th width="15%" align="center">Closing Date</th>
                        <th width="15%" align="center">Generated Commission</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                    </thead>
                    <tr>
                        <td>June YYYY</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 620</strong></td>
                        <td align="right"><a href="javascript:void()" class="btn btn-sm green show-dt">Show Details</a></td>
                    </tr>
                    <tr>
                        <td>July YYYY</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 500</strong></td>
                        <td align="right"><a href="javascript:void()" class="btn btn-sm green show-dt">Show Details</a></td>
                    </tr>
                    <tr>
                        <td>August YYYY</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 37,41</strong></td>
                        <td align="right"><a href="javascript:void()" class="btn btn-sm green show-dt">Show Details</a></td>
                    </tr>
                    <tr><td align="center" colspan="5"><a href="javascript:void()" class="btn btn-sm green">Show More History</a></td></tr>
                    <tr class="bg-black">
                        <td colspan="3">Total 2014</td>
                        <td colspan="2" align="right">€4500</td>
                    </tr>
                    <tr class="bg-green">
                        <td colspan="3">All Time Total</td>
                        <td colspan="2" align="right">&euro;9255</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end dsb -->

<div class="row dsb-panel" style="display:none;">
    <div class="col-md-12">
        <div class="block margin-bottom-20 bg-grey">
            <div class="row">
                <div class="col-md-6">
                    <h3><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-direct-sales-bonus.png" width="36"></i>
                        DSB - June 2014
                    </h3>
                </div>
                <div class="col-md-6 text-right">Closing Date: DD/MM/YY  &nbsp; 00:00:00 CET</div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable f14">
                        <table class="table max1000" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                            <tr>
                                <th width="35%" colspan="3">User Details</th>
                                <th width="25%" >Sales Type</th>
                                <th width="8%" align="center">Date</th>
                                <th width="10%" align="center">Status</th>
                                <th width="5%" align="right">DSB</th>
                            </tr>
                            </thead>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td>
                                    John Doe <br>
                                    johndoe1985@gmail.com
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-basic.png"> Basic Incentive Pack €149</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">Ok</td>
                                <td valign="middle" align="right"><h4 class="green bold">&euro; 5</h4></td>
                            </tr>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td>
                                    John Doe <br>
                                    johndoe1985@gmail.com
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-bronze.png"> Bronze Incentive Pack €299</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">Ok</td>
                                <td valign="middle" align="right"><h4 class="green bold">&euro; 10</h4></td>
                            </tr>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td>
                                    John Doe <br>
                                    johndoe1985@gmail.com
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-silver.png"> Silver Incentive Pack €599</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">Ok</td>
                                <td valign="middle" align="right"><h4 class="green bold">&euro; 35</h4></td>
                            </tr>
                            <tr>
                                <td width="55"><a class="online" href="profile.html"><img class="user-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/john.png"></a></td>
                                <td>
                                    <h4 class="no-margin bold green"><a href="profile.html">johndoe</a></h4>
                                    ID:123456
                                </td>
                                <td>
                                    John Doe <br>
                                    johndoe1985@gmail.com
                                </td>
                                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/small-pack-gold-1.png"> Gold Incentive Pack €1199</td>
                                <td align="center">dd/mm/yy<br>@hh-mm-ss</td>
                                <td align="center">Ok</td>
                                <td valign="middle" align="right"><h4 class="green bold">&euro; 75</h4></td>
                            </tr>
                            <tr class="bg-green">
                                <td colspan="4">Total</td>
                                <td colspan="3" align="right">&euro; 125</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end dsb month -->
