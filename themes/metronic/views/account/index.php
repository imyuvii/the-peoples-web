<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Overview";
$this->breadcrumbs=array(
    'Account',
    'Overview',
);
?>

<div class="row">
    <div class="col-md-12">
        <div class="block margin-bottom-20 bg-grey clearfix">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="text-center">Your Status</h2>
                    <div class="portlet light">
                        <div class="row margin-bottom-15">
                            <div class="col-md-6 col-xs-6 text-center no-space">
                                <h5 class="uppercase grey margin-top-20">Current Package</h5>
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/pkg-gold.png" class="margin-bottom-15">
                                <div class="clearfix"></div>
                                <button type="submit" class="btn btn-default btn-primary">Upgrade <i class="fa fa-question-circle"></i></button>
                            </div>
                            <!-- package end -->

                            <div class="col-md-6 col-xs-6 text-center no-space">
                                <h5 class="uppercase grey margin-top-20">Current Rank</h5>
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/rank-am.png" class="margin-bottom-15">
                                <div class="clearfix"></div>
                                <span class="next-rank">Next Rank</span> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-rank1.png">
                            </div>
                            <!-- rank end -->
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <table class="table max650" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-fast-track-bonus.png" width="36"></td>
                            <td>FTB</td>
                            <td align="center">
                                <div class="ftb-timer pie-title-center" data-percent="20"> <span class="pie-value"></span> </div>
                            </td>
                            <td align="center">
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user green"></i>
                                <i class="fa fa-user black"></i>
                                <i class="fa fa-user black"></i>
                                <i class="fa fa-user black"></i>
                            </td>
                            <td align="right"><a href="" class="btn btn-sm green"  data-toggle="modal" data-target="#ftb">More Info</a></td>
                        </tr>
                        <tr>
                            <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-royalty-bonus-ripple.png" width="36"></td>
                            <td>RB # levels</td>
                            <td align="center">3</td>
                            <td align="center">

                            </td>
                            <td align="right">
                                <a href="javascript:void()" class="btn btn-sm green" data-toggle="modal" data-target="#rb">More Info</a>
                            </td>
                        </tr>
                        <tr>
                            <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-ripple-matching-bonus.png" width="36"></td>
                            <td>RMB # levels</td>
                            <td align="center">0</td>
                            <td align="center">

                            </td>
                            <td align="right">
                                <a href="javascript:void()" class="btn btn-sm green" data-toggle="modal" data-target="#rmb">More Info</a>
                            </td>
                        </tr>
                        <tr>
                            <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-ripple-royalty-bonus.png" width="36"></td>
                            <td>RRB # levels</td>
                            <td align="center">0</td>
                            <td align="center">

                            </td>
                            <td align="right">
                                <a href="javascript:void()" class="btn btn-sm green" data-toggle="modal" data-target="#rrb">More Info</a>
                            </td>
                        </tr>
                        <tr>
                            <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-binary.png" width="36"></td>
                            <td>BSTB levels</td>
                            <td align="center">0</td>
                            <td align="center">

                            </td>
                            <td align="right">
                                <a href="javascript:void()" class="btn btn-sm green" data-toggle="modal" data-target="#bstb">More Info</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block margin-bottom-20 bg-grey">
            <div class="table-scrollable f14">
                <table class="table max650" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th colspan="2"><h3 class="no-margin">Your Affiliate Earnings</h3></th>
                        <th width="12%" align="center">Closing Date</th>
                        <th width="15%" align="center">Amount (residual)</th>
                        <th width="13%">&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tr>
                        <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-direct-sales-bonus.png" width="36"></td>
                        <td>DSB - Direct Sales Bonus</td>
                        <td align="center"></td>
                        <td align="center"><strong class="green">€ 620</strong></td>
                        <td>&nbsp;</td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show Details</a></td>
                    </tr>
                    <tr>
                        <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-fast-track-bonus.png" width="36"></td>
                        <td>FTB - Fast-Track Bonus</td>
                        <td align="center"><div class="ftb-timer pie-title-center" data-percent="20"> <span class="pie-value"></span> </div></td>
                        <td align="center"><strong class="green">€ 500</strong></td>
                        <td>&nbsp;</td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show Details</a></td>
                    </tr>
                    <tr>
                        <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-royalty-bonus-ripple.png" width="36"></td>
                        <td>RB - Ripple Bonus</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 37,41</strong></td>
                        <td align="right"><strong class="green">€ 325,00</strong> <i class="fa fa-info-circle"></i></td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show Details</a></td>
                    </tr>
                    <tr>
                        <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-ripple-matching-bonus.png" width="36"></td>
                        <td>RMB - Ripple Matching</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 12,76</strong></td>
                        <td align="right"><strong class="green">€ 57,01</strong> <i class="fa fa-info-circle"></i></td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show Details</a></td>
                    </tr>
                    <tr>
                        <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-ripple-royalty-bonus.png" width="36"></td>
                        <td>RRB - Ripple Royalty Bonus</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 12,76</strong></td>
                        <td align="right"><strong class="green">€ 57,01</strong> <i class="fa fa-info-circle"></i></td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show Details</a></td>
                    </tr>
                    <tr>
                        <td width="40"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/icon-binary.png" width="36"></td>
                        <td>BSTB - Binary Sales Team Bonus</td>
                        <td align="center">dd/mm/yy</td>
                        <td align="center"><strong class="green">€ 24,80</strong></td>
                        <td>&nbsp;</td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show Details</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end affiliate earning -->

<div class="row">
    <div class="col-md-12">
        <div class="block bg-grey">
            <div class="f14">
                <table class="table max650" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th colspan="5"><h3 class="no-margin">Total Sum</h3></th>
                    </tr>
                    </thead>
                    <tr>
                        <td width="20"><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-earning-total.png"></i></td>
                        <td>Current Month earnings TOTAL</td>
                        <td align="right">
                            <a class="btn btn-sm green" href="javascript:void()">Show</a>
                        </td>
                    </tr>
                    <tr>
                        <td><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-play.png"></i></td>
                        <td>Last months earning ‘after Close’</td>
                        <td align="right"><a class="btn btn-sm green" href="javascript:void()">Show</a></td>
                    </tr>

                    <tr>
                        <td><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-earning-total.png"></i></td>
                        <td>Current Year Earnings</td>

                        <td align="right" width="20%"> <select class="week-list full text-left">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>  </td>
                    </tr>
                    <tr>
                        <td><i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-earning-total.png"></i></td>
                        <td>Previous Years Earnings</td>
                        <td align="right"> <select class="week-list full text-left">
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                            </select></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end total sum -->
