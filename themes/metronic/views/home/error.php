<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="col-md-12 page-500 text-center">
	<div class="number font-red" style="font-size: 72px;"> <?php echo $code; ?> </div>
	<div class="details">
		<h3><?php echo CHtml::encode($message); ?></h3>
		<?php if($code == 500): ?>
			<p> We are fixing it! Please come back in a while.<br> </p>
		<?php endif; ?>
		<p><a href="" onclick="window.history.go(-1); return false;" class="btn red btn-outline"> Return Back </a><br> </p>
	</div>
</div>