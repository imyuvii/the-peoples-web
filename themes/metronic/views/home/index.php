<?php
/* @var $this SiteController */

$this->pageTitle="Dashboard";
$this->breadcrumbs=array(
	'Dashboard',
);
?>

<!-- END PAGE HEADER-->

<div class="portlet light">
	<div class="row figure-data">
		<div class="col-lg-2 col-md-4 col-sm-6 fig-box"><big class="number">340,230</big><span>People in Ripple Organisation</span></div>
		<div class="col-lg-2 col-md-4 col-sm-6 fig-box"><big class="number">$653,000</big><span>Total Earnings</span></div>
		<div class="col-lg-2 col-md-4 col-sm-6 fig-box"><big class="number">47,500</big><span>New Registerations</span></div>
		<div class="col-lg-2 col-md-4 col-sm-6 fig-box"><big class="number">340,230</big><span>Donec ultrices faucibus</span></div>
		<div class="col-lg-2 col-md-4 col-sm-6 fig-box"><big class="number">47,500</big><span>Phasellus sodales</span></div>
		<div class="col-lg-2 col-md-4 col-sm-6 fig-box"><big class="number">107,200</big><span>People in Binary Organisation</span></div>
	</div>
</div>
<!-- end figure -->

<div class="row">
	<div class="col-lg-4 col-md-6">
		<div class="portlet light">
			<div class="row">
				<div class="col-md-6 col-xs-6 text-center no-space">
					<h5 class="uppercase grey">Current Package</h5>
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/pkg-gold.png" class="margin-bottom-15">
					<div class="clearfix"></div>
					<button type="submit" class="btn btn-default btn-primary">Upgrade <i class="fa fa-question-circle"></i></button>
				</div>
				<!-- package end -->

				<div class="col-md-6 col-xs-6 text-center no-space">
					<h5 class="uppercase grey">Current Rank</h5>
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/rank-am.png" class="margin-bottom-15">
					<div class="clearfix"></div>
					<span class="next-rank">Next Rank</span> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-rank1.png">
				</div>
				<!-- rank end -->
			</div>
		</div>
	</div>
	<!-- end rank -->

	<div class="col-lg-4 col-md-6">
		<div class="portlet light">
			<div class="row">
				<div class="col-md-12 text-center">
					<h5 class="uppercase grey">Personal Affiliate Link</h5>
					<div class="affiliate-link">
						<a href="javascript:void()" data-toggle="tooltip" title="Email affiliate link to ..."><i class="fullscreen"></i></a>
						<a href="javascript:void()" data-toggle="tooltip" title="Copy affiliate link"><i class="link"></i></a>
						<a href="javascript:void()" data-toggle="tooltip" title="Open affiliate link in dashboard"><i class="screenlink"></i></a>
					</div>
					<a href="javascript:void()" target="_blank" class="grey">http://youraffiliatelinkurl.com</a>
				</div>
				<!-- package end -->
			</div>
		</div>
	</div>
	<!-- end personal affiliate -->

	<div class="col-md-4">
		<div class="portlet box purple">
			<div class="portlet-title">
				<h4 class="uppercase">News & Updates</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body">
				<ul class="news-slider">
					<li>
						<div class="news-head">
							<i><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-ic-biz.png"></i>
							<span class="news-title"><strong>Sint occaecati cupiditat</strong><small>29 mins ago</small></span>
						</div>
						<p>Lorem ipsum dolor sit amet consectetur adipiscing elite, sed do eiusmod tempor.. more</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end news -->
</div>

<div class="row">
	<div class="col-md-4">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">Social Feed</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="60"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout/img/icon-fb.png"></td>
						<td><span><big class="blue">42</big> shares and <big class="blue">20</big> likes <br>via Facebook</span></td>
					</tr>
				</table>
				<br><br>
				<div class="social-links">
					<a href="javascript:void()"><i class="fa fa-facebook"></i></a>
					<a href="javascript:void()"><i class="fa fa-twitter"></i></a>
					<a href="javascript:void()"><i class="fa fa-linkedin-square"></i></a>
				</div>
			</div>
		</div>
	</div>
	<!-- end social -->

	<div class="col-lg-4 col-md-12">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">Visit Websites and Backoffice</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body">
				<div class="daily-visit text-center">
					<a href="https://prelaunch.vaiox.com" target="_blank" data-toggle="tooltip" title="Go to VAIOX" class="web"><i class="vaiox"></i></a>
					<a href="http://www.fortunityalliance.com/login.php" target="_blank" data-toggle="tooltip" title="Go to FAN Backoffice" class="web"><i class="fan"></i></a>
					<a href="http://kickstart.fortunityalliance.com" target="_blank" data-toggle="tooltip" title="Go to Kickstart Website" class="web"><i class="ks"></i></a>
				</div>
			</div>
		</div>
	</div>
	<!-- end visitor -->

	<div class="col-md-4">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">New Members</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body clearfix">
				<ul class="user-list">
					<li>
						<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></a>
						<div class="member-cont">
							<h5><a href="profile.html">Anna Opichia</a></h5>
							<span class="green">lead designer</span>
						</div>
						<a href="javascript:void()" class="view"><i class="fa fa-eye grey"></i></a>
					</li>

					<li>
						<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-3.jpg"></a>
						<div class="member-cont">
							<h5><a href="profile.html">Bobby Socks</a></h5>
							<span class="green">CEO</span>
						</div>
						<a href="javascript:void()" class="view"><i class="fa fa-eye grey"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end member -->
</div>
<!-- end three col -->

<div class="row">
	<div class="col-lg-6">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">Stats</h4>
				<div class="tools">
					<a class="reload" href="javascript:void()" data-original-title="" title=""></a>
					<a href="javascript:;" class="fullscreen" data-original-title="" title=""></a>
				</div>
			</div>

			<div class="portlet-body" style="position:relative;">
				<h5 class="text-center bold vertical-text">Months</h5>
				<div id="dashboard-chart" class="chart-holder"><svg></svg></div>
				<h5 class="text-center bold">No. of People</h5>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">E-Ticketbooker</h4>
				<div class="tools">
					<a class="reload" href="javascript:void()" data-original-title="" title=""></a>
					<a href="javascript:;" class="fullscreen" data-original-title="" title=""></a>
				</div>
			</div>

			<div class="portlet-body" style="position:relative;">
				<div class="scroller" style="height: 270px;" data-always-visible="1" data-rail-visible="0">
					<div class="input-group input-large date-picker input-daterange margin-bottom-20" id="datepicker" >
						<input type="text" class="form-control" name="start" />
						<span class="input-group-addon">to </span>
						<input type="text" class="form-control" name="end" />
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-8">
							<div class="book-list">
								Monday 29/08/2016<br>
								presentation Iriscall @ Diepenbeek 8PM
							</div>
						</div>
						<div class="col-md-4"><a href="javascript:void()" class="btn btn-green-block margin-top-15">Register Here</a></div>
					</div>

					<div class="row">
						<div class="col-md-8">
							<div class="book-list">
								Monday 29/08/2016<br>
								presentation Iriscall @ Diepenbeek 8PM
							</div>
						</div>
						<div class="col-md-4"><a href="javascript:void()" class="btn btn-green-block margin-top-15">Register Here</a></div>
					</div>

					<div class="row">
						<div class="col-md-8">
							<div class="book-list">
								Monday 29/08/2016<br>
								presentation Iriscall @ Diepenbeek 8PM
							</div>
						</div>
						<div class="col-md-4"><a href="javascript:void()" class="btn btn-green-block margin-top-15">Register Here</a></div>
					</div>

					<div class="row">
						<div class="col-md-8">
							<div class="book-list">
								Monday 29/08/2016<br>
								presentation Iriscall @ Diepenbeek 8PM
							</div>
						</div>
						<div class="col-md-4"><a href="javascript:void()" class="btn btn-green-block margin-top-15">Register Here</a></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- end graph -->


<div class="row">
	<div class="col-lg-4 col-md-6">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">Activity Feed</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body clearfix">
				<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
					<ul class="user-list">
						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></a>
							<div class="activty-cont">
								<h5><a href="javascript:void()">Anna Opichia</a> <span>become gold member </span></h5>
								<small class="grey">29 mins ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"></a>
							<div class="activty-cont">
								<h5><a href="profile.html">Bobby Socks</a> <span>registered for business member group</span></h5>
								<small class="grey">30 mins ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-3.jpg"></a>
							<div class="activty-cont">
								<h5><a href="profile.html">Bobby Socks</a> <span>registered for business member group</span></h5>
								<small class="grey">3 hours ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></a>
							<div class="activty-cont">
								<h5><a href="profile.html">Roger Flopple</a> <span>become gold member </span></h5>
								<small class="grey">4 hours ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"></a>
							<div class="activty-cont"><h5><a href="profile.html">Deel McApple</a> <span>registered for business member group</span></h5>
								<small class="grey">8 hours ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-3.jpg"></a>
							<div class="activty-cont">
								<h5><a href="profile.html">Bobby Socks</a> <span>registered for business member group</span></h5>
								<small class="grey">3 hours ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-2.jpg"></a>
							<div class="activty-cont">
								<h5><a href="profile.html">Roger Flopple</a> <span>become gold member </span></h5>
								<small class="grey">4 hours ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>

						<li>
							<a href="profile.html" class="user-img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/pages/img/avatar-mini-4.jpg"></a>
							<div class="activty-cont"><h5><a href="profile.html">Deel McApple</a> <span>registered for business member group</span></h5>
								<small class="grey">8 hours ago</small>
								<div class="actions">
									<a href="javascript:void()"><i class="fa fa-eye"></i></a>
									<a href="javascript:void()"><i class="fa fa-comment"></i></a>
									<a href="javascript:void()"><i class="fa fa-phone"></i></a>
									<a href="javascript:void()"><i class="fa fa-envelope"></i></a>
									<a href="javascript:void()"><i class="fa fa-users"></i></a>                                            </div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- end activity feed -->

	<div class="col-lg-4 col-md-6">
		<div class="portlet box light">
			<div class="portlet-title">
				<h4 class="uppercase">Support Tickets</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body clearfix">
				<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
					<div class="sortable-todo">
						<table class="ticket-head" width="100%">
							<thead>
							<th width="25%">#</th>
							<th width="50%">Subject</th>
							<th width="10%" class="text-right">Status</th>
							</thead>
						</table>



						<div class="ticket-todo">
							<input type="checkbox" name="ticket2" id="ticket2" class="css-checkbox" />
							<label for="ticket2" class="css-label">
								<table width="100%">
									<tbody>
									<td>2930</td>
									<td width="60%">Responsive Issue</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>

						<div class="ticket-todo">
							<input type="checkbox" name="ticket3" id="ticket3" class="css-checkbox" />
							<label for="ticket3" class="css-label">
								<table width="100%">
									<tbody>
									<td>2931</td>
									<td width="60%">Image Missing</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>

						<div class="ticket-todo">
							<input type="checkbox" name="ticket4" id="ticket4" class="css-checkbox" />
							<label for="ticket4" class="css-label">
								<table width="100%">
									<tbody>
									<td>2932</td>
									<td width="60%">Alignment Issue</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>

						<div class="ticket-todo">
							<input type="checkbox" name="ticket5" id="ticket5" class="css-checkbox" />
							<label for="ticket5" class="css-label">
								<table width="100%">
									<tbody>
									<td>2933</td>
									<td width="60%">Profile Pic Missing</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>

						<div class="ticket-todo">
							<input type="checkbox" name="ticket6" id="ticket6" class="css-checkbox" />
							<label for="ticket6" class="css-label">
								<table width="100%">
									<tbody>
									<td>2934</td>
									<td width="60%">Image Missing</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>

						<div class="ticket-todo">
							<input type="checkbox" name="ticket7" id="ticket7" class="css-checkbox" />
							<label for="ticket7" class="css-label">
								<table width="100%">
									<tbody>
									<td>2935</td>
									<td width="60%">Alignment issue</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>

						<div class="ticket-todo">
							<input type="checkbox" name="ticket8" id="ticket8" class="css-checkbox" />
							<label for="ticket8" class="css-label">
								<table width="100%">
									<tbody>
									<td>2936</td>
									<td width="60%">Responsive Issue</td>
									<td class="text-right">Open</td>
									</tbody>
								</table>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end support -->

	<div class="col-lg-4 col-md-12">
		<div class="portlet box orange" style="overflow:hidden;">
			<div class="portlet-title">
				<h4 class="uppercase">Weekplanner</h4>
				<div class="tools">
					<a title="" data-original-title="" href="javascript:void()" class="reload"></a>
					<a title="" data-original-title="" class="fullscreen" href="javascript:;"></a>
				</div>
			</div>

			<div class="portlet-body clearfix">
				<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
					<div class="week-row">
						<div class="row">
							<div class="col-md-6">Monday 29/08/2016</div>
							<div class="col-md-6">
								<select class="week-list full">
									<option value="0">Lorem Ipsum 1</option>
									<option value="1">Lorem Ipsum 2</option>
								</select>
							</div>
						</div>
					</div>

					<div class="week-row">
						<div class="row">
							<div class="col-md-6">Monday 29/08/2016</div>
							<div class="col-md-6">
								<select class="week-list full">
									<option value="0">Lorem Ipsum 1</option>
									<option value="1">Lorem Ipsum 2</option>
								</select>
							</div>
						</div>
					</div>

					<div class="week-row">
						<div class="row">
							<div class="col-md-6">Monday 29/08/2016</div>
							<div class="col-md-6">
								<select class="week-list full">
									<option value="0">Lorem Ipsum 1</option>
									<option value="1">Lorem Ipsum 2</option>
								</select>
							</div>
						</div>
					</div>

					<div class="week-row">
						<div class="row">
							<div class="col-md-6">Monday 29/08/2016</div>
							<div class="col-md-6">
								<select class="week-list full">
									<option value="0">Lorem Ipsum 1</option>
									<option value="1">Lorem Ipsum 2</option>
								</select>
							</div>
						</div>
					</div>

					<div class="week-row">
						<div class="row">
							<div class="col-md-6">Monday 29/08/2016</div>
							<div class="col-md-6">
								<select class="week-list full">
									<option value="0">Lorem Ipsum 1</option>
									<option value="1">Lorem Ipsum 2</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end todo -->
</div>