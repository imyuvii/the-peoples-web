<?php
/**
 * Created by PhpStorm.
 * User: Sagar
 * Date: 3/17/2017
 * Time: 7:10 PM
 */
$this->pageTitle="View";
$this->breadcrumbs=array(
    'Email',
    'View',
);
?>

    <div class="col-md-9 col-sm-8">
        <div class="compose-bt margin-bottom-15">
            <a href="email-compose.html" class="btn btn-sm btn-primary" ><i class="fa fa-reply"></i> Reply</a>
            <a href="email-compose.html" class="btn btn-sm btn-primary" ><i class="fa fa-reply-all"></i> Reply to all</a>
            <button class="btn btn-sm bg-grey-salsa"><i class="fa fa-arrow-right"></i> Forward</button>
            <button class="btn  btn-sm bg-grey-salsa" type="button" title=""><i class="fa fa-print"></i> </button>
            <button class="btn btn-sm tooltips bg-grey-salsa" type="button"><i class="fa fa-trash-o"></i></button>
        </div>
        <div class="clearfix"></div>
        <section class="portlet box light">
            <div class="portlet-body">
                <h4 class="uppercase"> Bucket Admin Bootstrap 3 Responsive Flat Dashboard</h4>
                <div class="mail-sender">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="assets/admin/pages/img/email-img.jpg" alt="">
                            <strong>ThemeBucket</strong>
                            <span>[themebucket@gmail.com]</span>
                            to
                            <strong>me</strong>
                        </div>
                        <div class="col-md-4">
                            <p class="date"> 10:15AM 02 FEB 2014</p>
                        </div>
                    </div>
                </div>
                <div class="view-mail">
                    <p>Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. </p>
                    <p>Porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices <a href="javascript:void()">faucibus rutrum</a>. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. </p>
                    <p>Sodales vulputate urna, vel <a href="javascript:void()">accumsan augue egestas ac</a>. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. </p>
                </div>
                <div class="attachment-mail">
                    <p>
                        <span><i class="fa fa-paperclip"></i> 2 attachments — </span>
                        <a href="javascript:void()">Download all attachments</a>
                        |
                        <a href="javascript:void()">View all images</a>
                    </p>
                    <ul>
                        <li>
                            <a class="atch-thumb"href="javascript:void()">
                                <img src="assets/admin/pages/img/product_img.jpg">
                            </a>

                            <a class="name"href="javascript:void()">
                                IMG_001.jpg
                                <span>20KB</span>
                            </a>

                            <div class="links">
                                <a href="javascript:void()">View</a> -
                                <a href="javascript:void()">Download</a>
                            </div>
                        </li>

                        <li>
                            <a class="atch-thumb"href="javascript:void()">
                                <img src="assets/admin/pages/img/weather_image.jpg">
                            </a>

                            <a class="name"href="javascript:void()">
                                IMG_001.jpg
                                <span>20KB</span>
                            </a>

                            <div class="links">
                                <a href="javascript:void()">View</a> -
                                <a href="javascript:void()">Download</a>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>
        </section>
    </div>
    <!-- right mail compose -->

<!-- Modal -->
<div id="myModal" class="modal fade col-md-offset-4 col-md-4">
    <div class="portlet box light">
        <div class="portlet-title">
            <h4 class="uppercase">Create new folder</h4>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3"><label>Name</label></div>
                <div class="col-md-9"><input name="" type="text" class="form-control"></div>
            </div>

            <div class="row">
                <hr class="divider"/>
                <div class="col-md-12">
                    <h4>Select where to place the new folder</h4>
                    <ul class="list-group-item-info crt-fl-list">
                        <li><a href="">Inbox</a></li>
                        <li><a href="">> Subfolder</a></li>
                        <li><a href="">> Subfolder</a></li>
                        <li><a href="">Sent Mail</a></li>
                        <li><a href="">Important</a></li>
                        <li><a href="">Drafts</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Create</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>
<!-- end modal -->
