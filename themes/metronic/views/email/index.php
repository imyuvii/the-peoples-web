<?php
/**
 * Created by PhpStorm.
 * User: imyuvii
 * Date: 01/03/17
 * Time: 6:00 PM
 */

/* @var $this SiteController */

$this->pageTitle="Inbox";
$this->breadcrumbs=array(
    'Email',
    'Inbox',
);
?>


    <div class="col-md-9 col-sm-8">
        <section class="portlet box light">
            <header class="portlet-title">
                <h4 class="uppercase"> Inbox (34)</h4>
                <div class="tools">
                    <a title="" data-original-title="" href="javascript:void()" class="reload"></a>                        </div>
            </header>
            <div class="panel-body minimal">
                <div class="mail-option">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="chk-all">
                                <div class="inbox-small-cells" style="padding:0;">
                                    <input type="checkbox" name="selectall" id="selectall" class="css-checkbox" />
                                    <label for="selectall" class="css-label"></label>
                                </div>

                                <div class="btn-group">
                                    <a data-toggle="dropdown"href="javascript:void()" class="btn mini all">
                                        All
                                        <i class="fa fa-angle-down "></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void()"> None</a></li>
                                        <li><a href="javascript:void()"> Read</a></li>
                                        <li><a href="javascript:void()"> Unread</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="btn-group">
                                <a data-original-title="Refresh" data-placement="top" data-toggle="dropdown"href="javascript:void()" class="btn mini tooltips">
                                    <i class=" fa fa-refresh"></i>
                                </a>
                            </div>
                            <div class="btn-group hidden-phone">
                                <a data-toggle="dropdown"href="javascript:void()" class="btn mini blue">
                                    More
                                    <i class="fa fa-angle-down "></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void()"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a data-toggle="dropdown"href="javascript:void()" class="btn mini blue">
                                    Move to
                                    <i class="fa fa-angle-down "></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <ul class="label-lists">
                                            <li><a href="javascript:void()"><i class="fa fa-inbox"></i> Inbox</a></li>
                                            <li><a href="javascript:void()"><i class="fa fa-folder-o"></i> > Subfolder</a></li>
                                            <li><a href="javascript:void()"><i class="fa fa-folder-o"></i> > Subfolder</a></li>
                                            <li><a href="javascript:void()"><i class="fa fa-envelope-o"></i> Sent Mail</a></li>
                                            <li><a href="javascript:void()"><i class="fa fa-certificate"></i> Important</a></li>
                                            <li><a href="javascript:void()"><i class="fa fa-ban"></i> Drafts</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void()" data-toggle="modal" data-target="#myModal"><i class="fa fa-folder-o"></i> Create New Folder</a></li>
                                    <li><a href="javascript:void()"><i class="fa fa-trash-o"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <ul class="unstyled inbox-pagination">
                                <li><span>1-50 of 124</span></li>
                                <li>
                                    <a class="np-btn"href="javascript:void()"><i class="fa fa-angle-left  pagination-left"></i></a>
                                </li>
                                <li>
                                    <a class="np-btn"href="javascript:void()"><i class="fa fa-angle-right pagination-right"></i></a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-3">
                            <input type="text" placeholder=" Search" class="form-control search pull-right">
                        </div>
                    </div>
                    <!-- end inbox header -->

                </div>
                <div class="table-inbox-wrap ">
                    <div class="table-scrollable">
                        <table class="table max650 table-inbox table-hover">
                            <tbody>
                            <tr class="unread">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox1" id="inbox1" class="css-checkbox" />
                                    <label for="inbox1" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message  dont-show"><a href="email-view.html">ABC Company</a></td>
                                <td class="view-message "><a href="email-view.html">Lorem ipsum dolor imit set.</a></td>
                                <td class="view-message  inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                <td class="view-message  text-right">12.10 AM</td>
                            </tr>
                            <tr class="unread">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox2" id="inbox2" class="css-checkbox" />
                                    <label for="inbox2" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message dont-show"><a href="email-view.html">Mr Bean</a></td>
                                <td class="view-message"><a href="email-view.html">Hi Bro, Lorem ipsum dolor imit</a></td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">Jan 11</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox3" id="inbox3" class="css-checkbox" />
                                    <label for="inbox3" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message dont-show"><a href="email-view.html">Jonathan Smith</a></td>
                                <td class="view-message"><a href="email-view.html">Lorem ipsum dolor sit amet</a></td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">March 15</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox4" id="inbox4" class="css-checkbox" />
                                    <label for="inbox4" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message dont-show"><a href="email-view.html">Facebook</a></td>
                                <td class="view-message"><a href="email-view.html">Dolor sit amet, consectetuer adipiscing</a></td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">June 01</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox5" id="inbox5" class="css-checkbox" />
                                    <label for="inbox5" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                <td class="view-message dont-show"><a href="email-view.html">Tasi man <span class="label label-danger pull-right">urgent</span></a></td>
                                <td class="view-message"><a href="email-view.html">Lorem ipsum dolor sit amet</a></td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">May 2</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox6" id="inbox6" class="css-checkbox" />
                                    <label for="inbox6" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                <td class="view-message dont-show"><a href="email-view.html">Facebook</a></td>
                                <td class="view-message"><a href="email-view.html">Dolor sit amet, consectetuer adipiscing</a></td>
                                <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                <td class="view-message text-right">March 14</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox7" id="inbox7" class="css-checkbox" />
                                    <label for="inbox7" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                <td class="view-message dont-show">Bafent</td>
                                <td class="view-message">Lorem ipsum dolor sit amet</td>
                                <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                <td class="view-message text-right">December 19</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox8" id="inbox8" class="css-checkbox" />
                                    <label for="inbox8" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message dont-show">Facebook <span class="label label-success pull-right">megazine</span></td>
                                <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">March 04</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox9" id="inbox9" class="css-checkbox" />
                                    <label for="inbox9" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message dont-show">Dorimon</td>
                                <td class="view-message view-message">Lorem ipsum dolor sit amet</td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">June 13</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox10" id="inbox10" class="css-checkbox" />
                                    <label for="inbox10" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                <td class="view-message dont-show">Facebook <span class="label label-info pull-right">family</span></td>
                                <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">March 24</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox11" id="inbox11" class="css-checkbox" />
                                    <label for="inbox11" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                <td class="view-message dont-show">Mogli Marion</td>
                                <td class="view-message">Lorem ipsum dolor sit amet</td>
                                <td class="view-message inbox-small-cells"></td>
                                <td class="view-message text-right">February 09</td>
                            </tr>
                            <tr class="">
                                <td class="inbox-small-cells">
                                    <input type="checkbox" name="inbox12" id="inbox12" class="css-checkbox" />
                                    <label for="inbox12" class="css-label"></label>
                                </td>
                                <td class="inbox-small-cells"><i class="fa fa-star inbox-started"></i></td>
                                <td class="dont-show">Facebook</td>
                                <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
                                <td class="view-message inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                                <td class="view-message text-right">May 14</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- right mail compose -->
<!-- end earning -->

<!-- Modal -->
<div id="myModal" class="modal fade col-md-offset-4 col-md-4">
    <div class="portlet box light">
        <div class="portlet-title">
            <h4 class="uppercase">Create new folder</h4>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3"><label>Name</label></div>
                <div class="col-md-9"><input name="" type="text" class="form-control"></div>
            </div>

            <div class="row">
                <hr class="divider"/>
                <div class="col-md-12">
                    <h4>Select where to place the new folder</h4>
                    <ul class="list-group-item-info crt-fl-list">
                        <li><a href="">Inbox</a></li>
                        <li><a href="">> Subfolder</a></li>
                        <li><a href="">> Subfolder</a></li>
                        <li><a href="">Sent Mail</a></li>
                        <li><a href="">Important</a></li>
                        <li><a href="">Drafts</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Create</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>
<!-- end modal -->
