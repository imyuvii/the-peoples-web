<?php
/**
 * Created by PhpStorm.
 * User: Sagar
 * Date: 3/17/2017
 * Time: 6:09 PM
 */

$this->pageTitle="Compose Mail";
$this->breadcrumbs=array(
    'Email',
    'Compose Mail',
);
?>

    <div class="col-md-9 col-sm-8">
        <section class="portlet box light">
            <header class="portlet-title">
                <h4 class="uppercase"> Compose Mail</h4>
            </header>
            <div class="portlet-body">
                <div class="compose-btn">
                    <button class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Send</button>
                    <button class="btn btn-sm"><i class="fa fa-times"></i> Discard</button>
                    <button class="btn btn-sm">Draft</button>
                </div>
                <div class="search-mail pull-right"><input type="text" placeholder=" Search" class="form-control search"></div>
                <div class="compose-mail">
                    <form role="form-horizontal" method="post">
                        <div class="form-group">
                            <label for="to" class="">To:</label>
                            <input type="text" tabindex="1" id="to" class="form-control">
                            <a href="javascript:void()" class="add-contact"><i class="fa fa-plus-circle"></i></a>

                            <div class="compose-options">
                                <a onClick="$(this).hide(); $('#cc').parent().removeClass('hidden'); $('#cc').focus();" href="javascript:;">Cc</a>
                                <a onClick="$(this).hide(); $('#bcc').parent().removeClass('hidden'); $('#bcc').focus();" href="javascript:;">Bcc</a>                                    </div>
                        </div>

                        <div class="form-group hidden">
                            <label for="cc" class="">Cc:</label>
                            <input type="text" tabindex="2" id="cc" class="form-control">
                            <a href="javascript:void()" class="add-contact"><i class="fa fa-plus-circle"></i></a>
                        </div>

                        <div class="form-group hidden">
                            <label for="bcc" class="">Bcc:</label>
                            <input type="text" tabindex="2" id="bcc" class="form-control">
                            <a href="javascript:void()" class="add-contact"><i class="fa fa-plus-circle"></i></a>
                        </div>

                        <div class="form-group">
                            <label for="subject" class="">Subject:</label>
                            <input type="text" tabindex="1" id="subject" class="form-control">
                        </div>

                        <div class="compose-editor">
                            <textarea class="wysihtml5 form-control" rows="9"></textarea>
                            <input type="file" class="default">
                        </div>
                        <div class="compose-btn">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Send</button>
                            <button class="btn btn-sm"><i class="fa fa-times"></i> Discard</button>
                            <button class="btn btn-sm">Draft</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- right mail compose -->