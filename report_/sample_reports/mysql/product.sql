-- Products
-- VARIABLE: {
--      name: "range",
--      display: "Report Range",
--      type: "daterange",
--      default: { start: "yesterday", end: "yesterday" }
-- }
-- VARIABLE: {
--    name: "where_field1",
--    display: "Select Field1",
--    type: "select",
--    multiple: false,
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=5"}
-- }
--  VARIABLE: {
--    name: "where_condition1",
--    display: "Where condition 1",
--  modifier_options: ["=","!=",">","<"]
--  }
-- VARIABLE: {
--    name: "where_field2",
--    display: "Select Field2",
--    type: "select",
--    multiple: false,
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=5"}
-- }
--  VARIABLE: {
--    name: "where_condition2",
--    display: "Where condition 2",
--  modifier_options: ["=","!=",">","<"]
--  }
-- VARIABLE: {
--    name: "where_field3",
--    display: "Select Field3",
--    type: "select",
--    multiple: false,
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=5"}
-- }
--  VARIABLE: {
--    name: "where_condition3",
--    display: "Where condition 3",
--  modifier_options: ["=","!=",">","<"]
--  }
-- VARIABLE: {
--    name: "field_name",
--    display: "Select Field",
--    type: "select",
--    multiple: true,
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=5"}
-- }
--  VARIABLE: {
--    name: "query_builder",
--    display: "SQL Query",
--    type:"textarea"
--  }
-- CHART: {
-- 	"columns": ["created_at","product_id","product_name"],
-- 	"type": "AnnotatedTimeLine",
-- }


{% if query_builder == '' %}
SELECT
{% for field in field_name %}
  {% if field != 'multiselect-all' %}
    {{ field }}
    {% if not loop.last %}, {% endif %}
  {% endif %}
{% endfor %}
FROM
    product_info
WHERE
    created_at BETWEEN "{{ range.start }}" AND "{{ range.end }}"
{% if where_condition1 != '' %}
      AND
      {{ where_field1 }}
      {{ where_condition1_modifier }}
      '{{where_condition1}}'
    {% endif %}
{% if where_condition2 != '' %}
      AND
      {{ where_field2 }}
      {{ where_condition2_modifier }}
      '{{where_condition2}}'
    {% endif %}
{% if where_condition3 != '' %}
      AND
      {{ where_field3 }}
      {{ where_condition3_modifier }}
      '{{where_condition3}}'
    {% endif %}

{% else %}
  {{ query_builder }}
{% endif %}