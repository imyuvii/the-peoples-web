-- Other
-- VARIABLE: {
--      name: "range",
--      display: "Report Range",
--      type: "daterange",
--      default: { start: "yesterday", end: "yesterday" }
-- }
--VARIABLE: {
--   name: "select_table1",
--   display: "Select Tables 1",
--   type: "select",
--   multiple: false,
--   database_options: {table:"cyl_tables",column:"table_name"}
--}
-- VARIABLE: {
--    name: "join_field1",
--    display: "Select Join Field1",
--    type: "select",
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=1"}
-- }
-- VARIABLE: {
--    name: "select_where_field1",
--    display: "Where Select Field1",
--    type: "select",
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=1"}
-- }
--  VARIABLE: {
--    name: "select_where_condition1",
--    display: "Where condition 1",
--  modifier_options: ["=","!=",">","<"]
--  }
--VARIABLE: {
--   name: "select_table2",
--   display: "Select Tables 2",
--   type: "select",
--   multiple: false,
--   database_options: {table:"cyl_tables",column:"table_name"}
--}
-- VARIABLE: {
--    name: "join_field2",
--    display: "Select Join Field2",
--    type: "select",
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=1"}
-- }
-- VARIABLE: {
--    name: "select_where_field2",
--    display: "Where Select Fields2",
--    type: "select",
--    multiple: false,
--    database_options: {table:"cyl_fields",column:"field_name", where:"table_id=1"}
-- }
--  VARIABLE: {
--    name: "select_where_condition2",
--    display: "Where condition 2",
--  modifier_options: ["=","!=",">","<"]
--  }
--  VARIABLE: {
--    name: "query_builder",
--    display: "SQL Query",
--    type:"textarea"
--  }


{% if query_builder == '' %}

{% if select_table1 != '' %}
  SELECT
  *
  FROM
      {{ select_table1 }} AS {{ select_table1 }}

      {% if select_table2 != '' and join_field1 != '' %}
        INNER JOIN
        {{ select_table2 }} AS {{ select_table2 }}
        ON {{ select_table1 }}.{{join_field1}} = {{ select_table2 }}.{{join_field2}}
      {% endif %}
  WHERE
      {{select_table1}}.created_at BETWEEN "{{ range.start }}" AND "{{ range.end }}"
  {% if select_where_condition1 != '' %}
        AND
        {{ select_table1 }}.{{ select_where_field1 }}
        {{ select_where_condition1_modifier }}
        '{{select_where_condition1}}'
      {% endif %}

  {% if select_where_condition2 != '' %}
        AND
        {{ select_table2 }}.{{ select_where_field2 }}
        {{ select_where_condition2_modifier }}
        '{{select_where_condition2}}'
      {% endif %}

  {% if where_condition4 != '' %}
        AND
        {{ select_table1 }}.{{ select_where_field4 }}
        {{ select_where_condition4_modifier }}
        '{{where_condition4}}'
      {% endif %}
{% endif %}

{% else %}
  {{ query_builder }}
{% endif %}